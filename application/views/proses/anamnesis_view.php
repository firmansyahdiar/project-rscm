<div id="tbAnamnesi">
    <a href="#" class="easyui-linkbutton" plain="true" onclick="tambahAnamnesi()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah</a> 
    <a href="#" class="easyui-linkbutton" plain="true" onclick="ubahAnamnesi()"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Ubah</a> 
    <a href="#" class="easyui-linkbutton" plain="true" onclick="hapusAnamnesi()"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Hapus</a> 
    <!--<a href="#" class="easyui-linkbutton" plain="true" onclick="cetakIndividu()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Cetak</a>-->
    <div style="float: right; padding-top: 2px">
        <input id="cari" class="easyui-searchbox" style="width: 300px" data-options="searcher:doSearch,prompt:'Masukan kata pencarian'">
    </div>
</div>
<table id="dgAnamnesi" fitcolumns="true"></table>
<div id="dglAnamnesi" class="easyui-dialog" data-options="modal:true, closed:true" style="width: 95%; padding: 5px; max-height: 88%; top: 10%" buttons="#dglAnamnesi-buttons">
    <div class="ftitle">Kunjungan Anamnesia</div>
    <form id="fmAnamnesi" class="form-horizontal fm" method="post" novalidate="novalidate">
        <div class="container-fluid">
            <div class="row">
                <div class="col col-xs-12">
                    <div class="fitem">
                        <label>Nama Pasien :</label> 
                        <input id="id_pasien_anamnesis" name="id_pasien" class="easyui-combobox" required="true" type="text" maxlength="254" style="width: 30%;" />
                    </div>
                    <div class="fitem">
                        <label>Kunjungan Ke :</label> 
                        <input id="kunjungan_anamnesis" name="kunjungan" class="easyui-numberspinner" required="true" type="text" style="width: 30%;" />
                    </div>
                    <div class="fitem">
                        <label>Tanggal Periksa :</label> 
                        <input id="tgl_periksa_anamnesis" name="tgl_periksa" class="easyui-textbox" required="true" style="width: 20%;" />
                    </div>
                </div>
                <div id="tabAnamnesi" class="easyui-tabs" fit="true" border="false" style="height: 1px; padding: 1px;">
                    <div id="Anamnesi" title="Karsinoma Nasofaring">
                <!-- TAB KARSINOMA NASOFARING -->
                        <div class="col col-xs-7">
                        <!-- AA. BENJOLAN LEHER -->
                            <div class="fitem">
                                <label>Benjolan Leher:</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input width="400" id="Aa1_anamnesis" type="radio" name="Aa1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input width="400" id="Aa1_anamnesis" type="radio" name="Aa1" value="2"> Tidak 
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aa2_anamnesis" type="radio" name="Aa2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aa2_anamnesis" type="radio" name="Aa2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aa2_anamnesis" type="radio" name="Aa2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aa3_anamnesis" type="radio" name="Aa3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aa3_anamnesis" type="radio" name="Aa3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aa3_anamnesis" type="radio" name="Aa3" value="3"> 12 Bulan
                            </div><hr>
                        <!-- AB. HIDUNG TERSUMBAT -->
                            <div class="fitem">
                                <label>Hidung Tersumbat:</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ab1_anamnesis" type="radio" name="Ab1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ab1_anamnesis" type="radio" name="Ab1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ab2_anamnesis" type="radio" name="Ab2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ab2_anamnesis" type="radio" name="Ab2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ab2_anamnesis" type="radio" name="Ab2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ab3_anamnesis" type="radio" name="Ab3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ab3_anamnesis" type="radio" name="Ab3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ab3_anamnesis" type="radio" name="Ab3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- AC. LENDIR BERCAMPUR DARAH -->
                            <div class="fitem">
                                <label>Ingus / Lendir Bercampur Darah:</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ac1_anamnesis" type="radio" name="Ac1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ac1_anamnesis" type="radio" name="Ac1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ac2_anamnesis" type="radio" name="Ac2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ac2_anamnesis" type="radio" name="Ac2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ac2_anamnesis" type="radio" name="Ac2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ac3_anamnesis" type="radio" name="Ac3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ac3_anamnesis" type="radio" name="Ac3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ac3_anamnesis" type="radio" name="Ac3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- AD. TELINGA TERASA PENUH -->
                            <div class="fitem">
                                <label>Telinga Terasa Penuh :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ad1_anamnesis" type="radio" name="Ad1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ad1_anamnesis" type="radio" name="Ad1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ad2_anamnesis" type="radio" name="Ad2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ad2_anamnesis" type="radio" name="Ad2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ad2_anamnesis" type="radio" name="Ad2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ad3_anamnesis" type="radio" name="Ad3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ad3_anamnesis" type="radio" name="Ad3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ad3_anamnesis" type="radio" name="Ad3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- AE. POST NASAL DRIP -->
                            <div class="fitem">
                                <label>Post Nasal Drip :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ae1_anamnesis" type="radio" name="Ae1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ae1_anamnesis" type="radio" name="Ae1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ae2_anamnesis" type="radio" name="Ae2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ae2_anamnesis" type="radio" name="Ae2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ae2_anamnesis" type="radio" name="Ae2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ae3_anamnesis" type="radio" name="Ae3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ae3_anamnesis" type="radio" name="Ae3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ae3_anamnesis" type="radio" name="Ae3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- AF. PENGLIHATAN DOUBLE -->
                            <div class="fitem">
                                <label>Pengelihatan Double :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Af1_anamnesis" type="radio" name="Af1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Af1_anamnesis" type="radio" name="Af1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Af2_anamnesis" type="radio" name="Af2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Af2_anamnesis" type="radio" name="Af2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Af2_anamnesis" type="radio" name="Af2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Af3_anamnesis" type="radio" name="Af3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Af3_anamnesis" type="radio" name="Af3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Af3_anamnesis" type="radio" name="Af3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- AG. GANGGUAN DENGAR UNIT -->
                            <div class="fitem">
                                <label>Gangguan Dengat Unit :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ag1_anamnesis" type="radio" name="Ag1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ag1_anamnesis" type="radio" name="Ag1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ag2_anamnesis" type="radio" name="Ag2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ag2_anamnesis" type="radio" name="Ag2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ag2_anamnesis" type="radio" name="Ag2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ag3_anamnesis" type="radio" name="Ag3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ag3_anamnesis" type="radio" name="Ag3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ag3_anamnesis" type="radio" name="Ag3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- AH. GANGUAN DENGAR BILAT -->
                            <div class="fitem">
                                <label>Gangguan Dengat Bilat :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ah1_anamnesis" type="radio" name="Ah1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ah1_anamnesis" type="radio" name="Ah1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ah2_anamnesis" type="radio" name="Ah2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ah2_anamnesis" type="radio" name="Ah2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ah2_anamnesis" type="radio" name="Ah2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ah3_anamnesis" type="radio" name="Ah3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ah3_anamnesis" type="radio" name="Ah3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ah3_anamnesis" type="radio" name="Ah3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- AI. TELINGA BERDENGUN -->
                            <div class="fitem">
                                <label>Telinga Berdengung :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ai1_anamnesis" type="radio" name="Ai1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ai1_anamnesis" type="radio" name="Ai1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ai2_anamnesis" type="radio" name="Ai2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ai2_anamnesis" type="radio" name="Ai2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ai2_anamnesis" type="radio" name="Ai2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ai3_anamnesis" type="radio" name="Ai3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ai3_anamnesis" type="radio" name="Ai3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ai3_anamnesis" type="radio" name="Ai3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- AJ. NYERI TELINGA -->
                            <div class="fitem">
                                <label>Nyeri Telinga :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aj1_anamnesis" type="radio" name="Aj1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aj1_anamnesis" type="radio" name="Aj1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aj2_anamnesis" type="radio" name="Aj2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aj2_anamnesis" type="radio" name="Aj2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aj2_anamnesis" type="radio" name="Aj2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aj3_anamnesis" type="radio" name="Aj3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aj3_anamnesis" type="radio" name="Aj3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Aj3_anamnesis" type="radio" name="Aj3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- AK. CAIRAN TELINGA -->
                            <div class="fitem">
                                <label>Cairan Telinga :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ak1_anamnesis" type="radio" name="Ak1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ak1_anamnesis" type="radio" name="Ak1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ak2_anamnesis" type="radio" name="Ak2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ak2_anamnesis" type="radio" name="Ak2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ak2_anamnesis" type="radio" name="Ak2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ak3_anamnesis" type="radio" name="Ak3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ak3_anamnesis" type="radio" name="Ak3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ak3_anamnesis" type="radio" name="Ak3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- AL. Sakit Kepala Unilateral -->
                            <div class="fitem">
                                <label>Sakita kepala  :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Al1_anamnesis" type="radio" name="Al1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Al1_anamnesis" type="radio" name="Al1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Al2_anamnesis" type="radio" name="Al2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Al2_anamnesis" type="radio" name="Al2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Al2_anamnesis" type="radio" name="Al2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Al3_anamnesis" type="radio" name="Al3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Al3_anamnesis" type="radio" name="Al3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Al3_anamnesis" type="radio" name="Al3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- AM. PARSE N. KRANIAL -->
                            <div class="fitem">
                                <label>Parse N. Kranial :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Am1_anamnesis" type="radio" name="Am1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Am1_anamnesis" type="radio" name="Am1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Am2_anamnesis" type="radio" name="Am2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Am2_anamnesis" type="radio" name="Am2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Am2_anamnesis" type="radio" name="Am2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Am3_anamnesis" type="radio" name="Am3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Am3_anamnesis" type="radio" name="Am3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Am3_anamnesis" type="radio" name="Am3" value="3"> > 12 Bulan
                            </div><hr>
                        </div>
                    </div>
                    <div id="ht" title="Tumor Sinonasal">
                        <div class="col col-xs-7">
                        <!-- BA. BENGKAK HIDUNG LATERAL -->
                            <div class="fitem">
                                <label>Bengkak Hidung Lateral :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ba1_anamnesis" type="radio" name="Ba1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ba1_anamnesis" type="radio" name="Ba1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ba2_anamnesis" type="radio" name="Ba2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ba2_anamnesis" type="radio" name="Ba2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ba2_anamnesis" type="radio" name="Ba2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ba3_anamnesis" type="radio" name="Ba3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ba3_anamnesis" type="radio" name="Ba3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ba3_anamnesis" type="radio" name="Ba3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- BB. RASA BAAL PIPI / GIGI -->
                            <div class="fitem">
                                <label>Rasa Baal Pipi / Gigi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bb1_anamnesis" type="radio" name="Bb1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bb1_anamnesis" type="radio" name="Bb1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bb2_anamnesis" type="radio" name="Bb2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bb2_anamnesis" type="radio" name="Bb2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bb2_anamnesis" type="radio" name="Bb2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bb3_anamnesis" type="radio" name="Bb3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bb3_anamnesis" type="radio" name="Bb3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bb3_anamnesis" type="radio" name="Bb3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- BC. BENJOLAN DISAMPING MATA -->
                            <div class="fitem">
                                <label>Benjolan Disamping Mata :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bc1_anamnesis" type="radio" name="Bc1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bc1_anamnesis" type="radio" name="Bc1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bc2_anamnesis" type="radio" name="Bc2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bc2_anamnesis" type="radio" name="Bc2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bc2_anamnesis" type="radio" name="Bc2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bc3_anamnesis" type="radio" name="Bc3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bc3_anamnesis" type="radio" name="Bc3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bc3_anamnesis" type="radio" name="Bc3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- BD. GANGGUAN PENCIUMAN -->
                            <div class="fitem">
                                <label>Gangguan Penciuman :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bd1_anamnesis" type="radio" name="Bd1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bd1_anamnesis" type="radio" name="Bd1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bd2_anamnesis" type="radio" name="Bd2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bd2_anamnesis" type="radio" name="Bd2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bd2_anamnesis" type="radio" name="Bd2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bd3_anamnesis" type="radio" name="Bd3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bd3_anamnesis" type="radio" name="Bd3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bd3_anamnesis" type="radio" name="Bd3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- BE. BENJOLAN DI PALATUM -->
                            <div class="fitem">
                                <label>Benjolan di Palatum :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Be1_anamnesis" type="radio" name="Be1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Be1_anamnesis" type="radio" name="Be1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Be2_anamnesis" type="radio" name="Be2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Be2_anamnesis" type="radio" name="Be2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Be2_anamnesis" type="radio" name="Be2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Be3_anamnesis" type="radio" name="Be3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Be3_anamnesis" type="radio" name="Be3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Be3_anamnesis" type="radio" name="Be3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- BF. NYERI PIPI / GIGI -->
                            <div class="fitem">
                                <label>Nyeri Pipi / Gigi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bf1_anamnesis" type="radio" name="Bf1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bf1_anamnesis" type="radio" name="Bf1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bf2_anamnesis" type="radio" name="Bf2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bf2_anamnesis" type="radio" name="Bf2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bf2_anamnesis" type="radio" name="Bf2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bf3_anamnesis" type="radio" name="Bf3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bf3_anamnesis" type="radio" name="Bf3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bf3_anamnesis" type="radio" name="Bf3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- BG. MATA MENONJOL BUTA -->
                            <div class="fitem">
                                <label>Mata Menonjol Buta :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bg1_anamnesis" type="radio" name="Bg1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bg1_anamnesis" type="radio" name="Bg1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bg2_anamnesis" type="radio" name="Bg2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bg2_anamnesis" type="radio" name="Bg2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bg2_anamnesis" type="radio" name="Bg2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bg3_anamnesis" type="radio" name="Bg3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bg3_anamnesis" type="radio" name="Bg3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bg3_anamnesis" type="radio" name="Bg3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- BH. MUKA MENCONG -->
                            <div class="fitem">
                                <label>Muka Mencong :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bh1_anamnesis" type="radio" name="Bh1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bh1_anamnesis" type="radio" name="Bh1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bh2_anamnesis" type="radio" name="Bh2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bh2_anamnesis" type="radio" name="Bh2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bh2_anamnesis" type="radio" name="Bh2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bh3_anamnesis" type="radio" name="Bh3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bh3_anamnesis" type="radio" name="Bh3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bh3_anamnesis" type="radio" name="Bh3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- BI. SUARA SENGAU -->
                            <div class="fitem">
                                <label>Suara Sengau :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bi1_anamnesis" type="radio" name="Bi1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bi1_anamnesis" type="radio" name="Bi1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bi2_anamnesis" type="radio" name="Bi2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bi2_anamnesis" type="radio" name="Bi2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bi2_anamnesis" type="radio" name="Bi2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bi3_anamnesis" type="radio" name="Bi3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bi3_anamnesis" type="radio" name="Bi3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bi3_anamnesis" type="radio" name="Bi3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- BJ. GIGI NYERI / GOYAH -->
                            <div class="fitem">
                                <label>Gigi Nyeri / Goyah :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bj1_anamnesis" type="radio" name="Bj1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bj1_anamnesis" type="radio" name="Bj1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bj2_anamnesis" type="radio" name="Bj2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bj2_anamnesis" type="radio" name="Bj2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bj2_anamnesis" type="radio" name="Bj2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bj3_anamnesis" type="radio" name="Bj3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bj3_anamnesis" type="radio" name="Bj3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Bj3_anamnesis" type="radio" name="Bj3" value="3"> > 12 Bulan
                            </div><hr>
                        </div>
                    </div>
                    <div id="ht" title="Tumor Oral Cavity">
                        <div class="col col-xs-7">
                        <!-- CA. SARIAWAN SUKAR SEMBUH -->
                            <div class="fitem">
                                <label>Sariawan Sukar Sembuh :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ca1_anamnesis" type="radio" name="Ca1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ca1_anamnesis" type="radio" name="Ca1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ca2_anamnesis" type="radio" name="Ca2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ca2_anamnesis" type="radio" name="Ca2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ca2_anamnesis" type="radio" name="Ca2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ca3_anamnesis" type="radio" name="Ca3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ca3_anamnesis" type="radio" name="Ca3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ca3_anamnesis" type="radio" name="Ca3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- CB. PENDARAHAN LOKAL -->
                            <div class="fitem">
                                <label>Pendarahan Lokal :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cb1_anamnesis" type="radio" name="Cb1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cb1_anamnesis" type="radio" name="Cb1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cb2_anamnesis" type="radio" name="Cb2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cb2_anamnesis" type="radio" name="Cb2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cb2_anamnesis" type="radio" name="Cb2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cb3_anamnesis" type="radio" name="Cb3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cb3_anamnesis" type="radio" name="Cb3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cb3_anamnesis" type="radio" name="Cb3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- CC. KESULITAN MENELAN -->
                            <div class="fitem">
                                <label>Kesulitan Menelan :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cc1_anamnesis" type="radio" name="Cc1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cc1_anamnesis" type="radio" name="Cc1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cc2_anamnesis" type="radio" name="Cc2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cc2_anamnesis" type="radio" name="Cc2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cc2_anamnesis" type="radio" name="Cc2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cc3_anamnesis" type="radio" name="Cc3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cc3_anamnesis" type="radio" name="Cc3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cc3_anamnesis" type="radio" name="Cc3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- CD. NYERI PADA LESI -->
                            <div class="fitem">
                                <label>Nyeri Pada Lesi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cd1_anamnesis" type="radio" name="Cd1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cd1_anamnesis" type="radio" name="Cd1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cd2_anamnesis" type="radio" name="Cd2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cd2_anamnesis" type="radio" name="Cd2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cd2_anamnesis" type="radio" name="Cd2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cd3_anamnesis" type="radio" name="Cd3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cd3_anamnesis" type="radio" name="Cd3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cd3_anamnesis" type="radio" name="Cd3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- CE. SULIT BERBICARA -->
                            <div class="fitem">
                                <label>Sulit Berbicara :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ce1_anamnesis" type="radio" name="Ce1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ce1_anamnesis" type="radio" name="Ce1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ce2_anamnesis" type="radio" name="Ce2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ce2_anamnesis" type="radio" name="Ce2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ce2_anamnesis" type="radio" name="Ce2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ce3_anamnesis" type="radio" name="Ce3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ce3_anamnesis" type="radio" name="Ce3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ce3_anamnesis" type="radio" name="Ce3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- CF. LUKA BERGAUNG -->
                            <div class="fitem">
                                <label>Luka Bergaung :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cf1_anamnesis" type="radio" name="Cf1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cf1_anamnesis" type="radio" name="Cf1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cf2_anamnesis" type="radio" name="Cf2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cf2_anamnesis" type="radio" name="Cf2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cf2_anamnesis" type="radio" name="Cf2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cf3_anamnesis" type="radio" name="Cf3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cf3_anamnesis" type="radio" name="Cf3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Cf3_anamnesis" type="radio" name="Cf3" value="3"> > 12 Bulan
                            </div><hr>
                        </div>
                    </div>
                    <div id="ht" title="Tumor Orofaring">
                        <div class="col col-xs-7">
                        <!-- DA. GERAK LIDAH TERBATAS -->
                            <div class="fitem">
                                <label>Gerak Lidah Terbatas :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Da1_anamnesis" type="radio" name="Da1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Da1_anamnesis" type="radio" name="Da1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Da2_anamnesis" type="radio" name="Da2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Da2_anamnesis" type="radio" name="Da2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Da2_anamnesis" type="radio" name="Da2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Da3_anamnesis" type="radio" name="Da3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Da3_anamnesis" type="radio" name="Da3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Da3_anamnesis" type="radio" name="Da3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- DB. BERAT BADAN TURUN -->
                            <div class="fitem">
                                <label>Berat Badan Turun :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Db1_anamnesis" type="radio" name="Db1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Db1_anamnesis" type="radio" name="Db1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Db2_anamnesis" type="radio" name="Db2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Db2_anamnesis" type="radio" name="Db2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Db2_anamnesis" type="radio" name="Db2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Db3_anamnesis" type="radio" name="Db3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Db3_anamnesis" type="radio" name="Db3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Db3_anamnesis" type="radio" name="Db3" value="3"> > 12 Bulan
                            </div><hr>
                         <!--DC. NYERI MENELAN --> 
                            <div class="fitem">
                                <label>Nyeri Menelan :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dc1_anamnesis" type="radio" name="Dc1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dc1_anamnesis" type="radio" name="Dc1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dc2_anamnesis" type="radio" name="Dc2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dc2_anamnesis" type="radio" name="Dc2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dc2_anamnesis" type="radio" name="Dc2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dc3_anamnesis" type="radio" name="Dc3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dc3_anamnesis" type="radio" name="Dc3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dc3_anamnesis" type="radio" name="Dc3" value="3"> > 12 Bulan
                            </div><hr>
                         <!--DD. HOT POTATO VOICE--> 
                            <div class="fitem">
                                <label>Hot Potato Voive :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dd1_anamnesis" type="radio" name="Dd1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dd1_anamnesis" type="radio" name="Dd1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dd2_anamnesis" type="radio" name="Dd2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dd2_anamnesis" type="radio" name="Dd2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dd2_anamnesis" type="radio" name="Dd2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dd3_anamnesis" type="radio" name="Dd3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dd3_anamnesis" type="radio" name="Dd3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Dd3_anamnesis" type="radio" name="Dd3" value="3"> > 12 Bulan
                            </div><hr>
                         <!--DE. MULUT BERBAU--> 
                            <div class="fitem">
                                <label>Mulut Berbau :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="De1_anamnesis" type="radio" name="De1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="De1_anamnesis" type="radio" name="De1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="De2_anamnesis" type="radio" name="De2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="De2_anamnesis" type="radio" name="De2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="De2_anamnesis" type="radio" name="De2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="De3_anamnesis" type="radio" name="De3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="De3_anamnesis" type="radio" name="De3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="De3_anamnesis" type="radio" name="De3" value="3"> > 12 Bulan
                            </div><hr>
                         <!--DF. SUKAR BUKA MULUT--> 
                            <div class="fitem">
                                <label>Sukar Buka Mulut :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Df1_anamnesis" type="radio" name="Df1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Df1_anamnesis" type="radio" name="Df1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Df2_anamnesis" type="radio" name="Df2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Df2_anamnesis" type="radio" name="Df2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Df2_anamnesis" type="radio" name="Df2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Df3_anamnesis" type="radio" name="Df3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Df3_anamnesis" type="radio" name="Df3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Df3_anamnesis" type="radio" name="Df3" value="3"> > 12 Bulan
                            </div><hr>
                        </div>
                    </div>
                    <div id="ht" title="Angiofibroma Nasofaring">
                        <div class="col col-xs-9">
                        <!-- EA. HIDUNG TERSUMBAT -->
                            <div class="fitem">
                                <label>Gerak Lidah Terbatas :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ea1_anamnesis" type="radio" name="Ea1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ea1_anamnesis" type="radio" name="Ea1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ea2_anamnesis" type="radio" name="Ea2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ea2_anamnesis" type="radio" name="Ea2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ea2_anamnesis" type="radio" name="Ea2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ea3_anamnesis" type="radio" name="Ea3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ea3_anamnesis" type="radio" name="Ea3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ea3_anamnesis" type="radio" name="Ea3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- EB. MIMISAN -->
                            <div class="fitem">
                                <label>Mimisan :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eb1_anamnesis" type="radio" name="Eb1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eb1_anamnesis" type="radio" name="Eb1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eb2_anamnesis" type="radio" name="Eb2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eb2_anamnesis" type="radio" name="Eb2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eb2_anamnesis" type="radio" name="Eb2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eb3_anamnesis" type="radio" name="Eb3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eb3_anamnesis" type="radio" name="Eb3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eb3_anamnesis" type="radio" name="Eb3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- EC. SAKIT KEPALA -->
                            <div class="fitem">
                                <label>Sakit Kepala :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ec1_anamnesis" type="radio" name="Ec1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ec1_anamnesis" type="radio" name="Ec1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ec2_anamnesis" type="radio" name="Ec2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ec2_anamnesis" type="radio" name="Ec2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ec2_anamnesis" type="radio" name="Ec2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ec3_anamnesis" type="radio" name="Ec3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ec3_anamnesis" type="radio" name="Ec3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ec3_anamnesis" type="radio" name="Ec3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- ED. PEMBENGKAKAN HIDUNG -->
                            <div class="fitem">
                                <label>Pembengkakan Hidung :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ed1_anamnesis" type="radio" name="Ed1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ed1_anamnesis" type="radio" name="Ed1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ed2_anamnesis" type="radio" name="Ed2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ed2_anamnesis" type="radio" name="Ed2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ed2_anamnesis" type="radio" name="Ed2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ed3_anamnesis" type="radio" name="Ed3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ed3_anamnesis" type="radio" name="Ed3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ed3_anamnesis" type="radio" name="Ed3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- EE. SESAK NAFAS -->
                            <div class="fitem">
                                <label>Sesak Nafas :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ee1_anamnesis" type="radio" name="Ee1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ee1_anamnesis" type="radio" name="Ee1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lokasi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ee2_anamnesis" type="radio" name="Ee2" value="1"> Kanan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ee2_anamnesis" type="radio" name="Ee2" value="2"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ee2_anamnesis" type="radio" name="Ee2" value="3"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Lama : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ee3_anamnesis" type="radio" name="Ee3" value="1"> 0 - 6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ee3_anamnesis" type="radio" name="Ee3" value="2"> 6 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ee3_anamnesis" type="radio" name="Ee3" value="3"> > 12 Bulan
                            </div><hr>
                        <!-- EF. PERTAMA KALI KE DOKTER -->
                            <div class="fitem">
                                <label>Pertama kali kedokter :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ef1_anamnesis" type="radio" name="Ef1" value="1"> < 6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ef1_anamnesis" type="radio" name="Ef1" value="2"> > 6 Bulan 
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ef2_anamnesis" type="radio" name="Ef2" value="1"> Dokter Umum 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ef2_anamnesis" type="radio" name="Ef2" value="2"> Dokter THT Lainnya <input id="Ef2Lainnya" name="Ef2Lainnya" class="easyui-textbox" type="text" style="width: 30%;" />
                            </div><hr>
                        <!-- EG. RIWAYAT PENYAKIT TERDAHULU -->
                            <div class="fitem">
                                <label>Riwayat penyakit dahulu : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eg1_anamnesis" type="checkbox" name="Eg1"> GERD 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eg2_anamnesis" type="checkbox" name="Eg2"> Barrett 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eg3_anamnesis" type="checkbox" name="Eg3"> Achalasia 
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eg4_anamnesis" type="checkbox" name="Eg4"> Konsumsi Zat Korosif 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Eg5_anamnesis" type="checkbox" name="Eg5"> Lainnya <input id="EgLainnya_anamnesis" name="EgLainnya" class="easyui-textbox" type="text" style="width: 18%;" />
                            </div><hr>
                        </div>
                    </div>
                    <div id="ht" title="Tumor Laring">
                        <div class="col col-xs-9"><br>
                        <!-- FA. SUARA SERAK -->
                            <div class="fitem">
                                <label>Suara Serak :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fa1_anamnesis" type="radio" name="Fa1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fa1_anamnesis" type="radio" name="Fa1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lama Keluhan : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fa2_anamnesis" type="radio" name="Fa2" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fa2_anamnesis" type="radio" name="Fa2" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fa2_anamnesis" type="radio" name="Fa2" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- FB. NAFAS BUNYI -->
                            <div class="fitem">
                                <label>Nafas Bunyi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fb1_anamnesis" type="radio" name="Fb1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fb1_anamnesis" type="radio" name="Fb1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lama Keluhan : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fb2_anamnesis" type="radio" name="Fb2" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fb2_anamnesis" type="radio" name="Fb2" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fb2_anamnesis" type="radio" name="Fb2" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- FC. SESAK NAFAS -->
                            <div class="fitem">
                                <label>Sesak Nafas :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fc1_anamnesis" type="radio" name="Fc1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fc1_anamnesis" type="radio" name="Fc1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lama Keluhan : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fc2_anamnesis" type="radio" name="Fc2" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fc2_anamnesis" type="radio" name="Fc2" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fc2_anamnesis" type="radio" name="Fc2" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- FD. NYERI MENELAN -->
                            <div class="fitem">
                                <label>Nyeri Menelan :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fd1_anamnesis" type="radio" name="Fd1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fd1_anamnesis" type="radio" name="Fd1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lama Keluhan : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fd2_anamnesis" type="radio" name="Fd2" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fd2_anamnesis" type="radio" name="Fd2" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fd2_anamnesis" type="radio" name="Fd2" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- FE. SULIT MENELAN -->
                            <div class="fitem">
                                <label>Sulit Menelan :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fe1_anamnesis" type="radio" name="Fe1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fe1_anamnesis" type="radio" name="Fe1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lama Keluhan : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fe2_anamnesis" type="radio" name="Fe2" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fe2_anamnesis" type="radio" name="Fe2" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fe2_anamnesis" type="radio" name="Fe2" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- FF. TERSEDAK -->
                            <div class="fitem">
                                <label>Tersedak :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ff1_anamnesis" type="radio" name="Ff1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ff1_anamnesis" type="radio" name="Ff1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lama Keluhan : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ff2_anamnesis" type="radio" name="Ff2" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ff2_anamnesis" type="radio" name="Ff2" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ff2_anamnesis" type="radio" name="Ff2" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- FG. BENJOLAN PADA LEHER -->
                            <div class="fitem">
                                <label>Benjolan Pada Leher :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fg1_anamnesis" type="radio" name="Fg1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fg1_anamnesis" type="radio" name="Fg1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Lama Keluhan : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fg2_anamnesis" type="radio" name="Fg2" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fg2_anamnesis" type="radio" name="Fg2" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Fg2_anamnesis" type="radio" name="Fg2" value="3"> >12 Bulan 
                            </div><hr>
                        </div>
                    </div>
                    <div id="ht" title="Tumor Parotis">
                        <div class="col col-xs-9"><br>
                        <!-- GA. BENJOLAN -->
                            <div class="fitem">
                                <label>Benjolan Pada Ujung Bawah Telinga :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ga1_anamnesis" type="radio" name="Ga1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ga1_anamnesis" type="radio" name="Ga1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ga2_anamnesis" type="radio" name="Ga2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ga2_anamnesis" type="radio" name="Ga2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama Keluhan :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ga3_anamnesis" type="radio" name="Ga3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ga3_anamnesis" type="radio" name="Ga3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ga3_anamnesis" type="radio" name="Ga3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- GB. WAJAH MENCONG -->
                            <div class="fitem">
                                <label>Wajah Mencong :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gb1_anamnesis" type="radio" name="Gb1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gb1_anamnesis" type="radio" name="Gb1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gb2_anamnesis" type="radio" name="Gb2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gb2_anamnesis" type="radio" name="Gb2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama Keluhan :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gb3_anamnesis" type="radio" name="Gb3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gb3_anamnesis" type="radio" name="Gb3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gb3_anamnesis" type="radio" name="Gb3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- GC. DEMAM -->
                            <div class="fitem">
                                <label>Demam :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gc1_anamnesis" type="radio" name="Gc1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gc1_anamnesis" type="radio" name="Gc1" value="2"> Tidak
                            </div><hr>
                        <!-- GD. LUDAH BERCAMPUR NANAH -->
                            <div class="fitem">
                                <label>Ludah Bercampur Nanah :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gd1_anamnesis" type="radio" name="Gd1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gd1_anamnesis" type="radio" name="Gd1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lama Keluhan :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gd2_anamnesis" type="radio" name="Gd2" value="1"> 0-6 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gd2_anamnesis" type="radio" name="Gd2" value="2"> 6-12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Gd2_anamnesis" type="radio" name="Gd2" value="3"> >12 Bulan
                            </div><hr>
                        </div>
                    </div>
                    <div id="ht" title="Tumor Telinga">
                        <div class="col col-xs-9"><br>
                        <!-- HA. BENJOLAN -->
                            <div class="fitem">
                                <label>Benjolan di Telinga :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ha1_anamnesis" type="radio" name="Ha1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ha1_anamnesis" type="radio" name="Ha1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ha2_anamnesis" type="radio" name="Ha2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ha2_anamnesis" type="radio" name="Ha2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ha3_anamnesis" type="radio" name="Ha3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ha3_anamnesis" type="radio" name="Ha3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ha3_anamnesis" type="radio" name="Ha3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- HB. KELUAR CAIRAN DI TELINGA -->
                            <div class="fitem">
                                <label>Keluar Cairan di Telinga :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hb1_anamnesis" type="radio" name="Hb1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hb1_anamnesis" type="radio" name="Hb1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hb2_anamnesis" type="radio" name="Hb2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hb2_anamnesis" type="radio" name="Hb2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hb3_anamnesis" type="radio" name="Hb3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hb3_anamnesis" type="radio" name="Hb3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hb3_anamnesis" type="radio" name="Hb3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- HC. PENURUNAN PENDENGARAN -->
                            <div class="fitem">
                                <label>Penurunan Pendengaran :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hc1_anamnesis" type="radio" name="Hc1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hc1_anamnesis" type="radio" name="Hc1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hc2_anamnesis" type="radio" name="Hc2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hc2_anamnesis" type="radio" name="Hc2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hc3_anamnesis" type="radio" name="Hc3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hc3_anamnesis" type="radio" name="Hc3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hc3_anamnesis" type="radio" name="Hc3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- HD. TELINGA TERASA PENUH -->
                            <div class="fitem">
                                <label>Telinga Terasa Penuh :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hd1_anamnesis" type="radio" name="Hd1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hd1_anamnesis" type="radio" name="Hd1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hd2_anamnesis" type="radio" name="Hd2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hd2_anamnesis" type="radio" name="Hd2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hd3_anamnesis" type="radio" name="Hd3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hd3_anamnesis" type="radio" name="Hd3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hd3_anamnesis" type="radio" name="Hd3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- HE. TELINGA BERBUNYI -->
                            <div class="fitem">
                                <label>Telinga Berbunyi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="He1_anamnesis" type="radio" name="He1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="He1_anamnesis" type="radio" name="He1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="He2_anamnesis" type="radio" name="He2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="He2_anamnesis" type="radio" name="He2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="He3_anamnesis" type="radio" name="He3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="He3_anamnesis" type="radio" name="He3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="He3_anamnesis" type="radio" name="He3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- HF. PUSING BERPUTAR -->
                            <div class="fitem">
                                <label>Pusing Berputar :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hf1_anamnesis" type="radio" name="Hf1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hf1_anamnesis" type="radio" name="Hf1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hf2_anamnesis" type="radio" name="Hf2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hf2_anamnesis" type="radio" name="Hf2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hf3_anamnesis" type="radio" name="Hf3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hf3_anamnesis" type="radio" name="Hf3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hf3_anamnesis" type="radio" name="Hf3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- HG. GANGGURAN KESEIMBANGAN -->
                            <div class="fitem">
                                <label>Gangguan Keseimbangan :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hg1_anamnesis" type="radio" name="Hg1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hg1_anamnesis" type="radio" name="Hg1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hg2_anamnesis" type="radio" name="Hg2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hg2_anamnesis" type="radio" name="Hg2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hg3_anamnesis" type="radio" name="Hg3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hg3_anamnesis" type="radio" name="Hg3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hg3_anamnesis" type="radio" name="Hg3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- HH. MUKA MENCONG -->
                            <div class="fitem">
                                <label>Muka Mencong :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hh1_anamnesis" type="radio" name="Hh1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hh1_anamnesis" type="radio" name="Hh1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hh2_anamnesis" type="radio" name="Hh2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hh2_anamnesis" type="radio" name="Hh2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hh3_anamnesis" type="radio" name="Hh3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hh3_anamnesis" type="radio" name="Hh3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hh3_anamnesis" type="radio" name="Hh3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- HI. NYERI TELINGA -->
                            <div class="fitem">
                                <label>Nyeri Telinga :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hi1_anamnesis" type="radio" name="Hi1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hi1_anamnesis" type="radio" name="Hi1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hi2_anamnesis" type="radio" name="Hi2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hi2_anamnesis" type="radio" name="Hi2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hi3_anamnesis" type="radio" name="Hi3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hi3_anamnesis" type="radio" name="Hi3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hi3_anamnesis" type="radio" name="Hi3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- HJ. KELUAR DARAH DARI TELINGA -->
                            <div class="fitem">
                                <label>Keluar Darah Dari Telingas :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hj1_anamnesis" type="radio" name="Hj1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hj1_anamnesis" type="radio" name="Hj1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hj2_anamnesis" type="radio" name="Hj2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hj2_anamnesis" type="radio" name="Hj2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hj3_anamnesis" type="radio" name="Hj3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hj3_anamnesis" type="radio" name="Hj3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Hj3_anamnesis" type="radio" name="Hj3" value="3"> >12 Bulan 
                            </div><hr>
                        </div>
                    </div>
                    <div id="ht" title="Tumor Esofagus">
                        <div class="col col-xs-9"><br>
                        <!-- IA. BENJOLAN -->
                            <div class="fitem">
                                <label>Benjolan Leher :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ia1_anamnesis" type="radio" name="Ia1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ia1_anamnesis" type="radio" name="Ia1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ia2_anamnesis" type="radio" name="Ia2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ia2_anamnesis" type="radio" name="Ia2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ia3_anamnesis" type="radio" name="Ia3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ia3_anamnesis" type="radio" name="Ia3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ia3_anamnesis" type="radio" name="Ia3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- IB. DISFAGIA -->
                            <div class="fitem">
                                <label>Disfagia :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ib1_anamnesis" type="radio" name="Ib1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ib1_anamnesis" type="radio" name="Ib1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ib2_anamnesis" type="radio" name="Ib2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ib2_anamnesis" type="radio" name="Ib2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ib3_anamnesis" type="radio" name="Ib3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ib3_anamnesis" type="radio" name="Ib3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ib3_anamnesis" type="radio" name="Ib3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- IC. PENURUNAN BERAT BADAN -->
                            <div class="fitem">
                                <label>Penurunan Berat Badan :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ic1_anamnesis" type="radio" name="Ic1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ic1_anamnesis" type="radio" name="Ic1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ic2_anamnesis" type="radio" name="Ic2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ic2_anamnesis" type="radio" name="Ic2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ic3_anamnesis" type="radio" name="Ic3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ic3_anamnesis" type="radio" name="Ic3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ic3_anamnesis" type="radio" name="Ic3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- ID. MNTAH DARAH -->
                            <div class="fitem">
                                <label>Muntah Darah :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Id1_anamnesis" type="radio" name="Id1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Id1_anamnesis" type="radio" name="Id1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Id2_anamnesis" type="radio" name="Id2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Id2_anamnesis" type="radio" name="Id2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Id3_anamnesis" type="radio" name="Id3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Id3_anamnesis" type="radio" name="Id3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Id3_anamnesis" type="radio" name="Id3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- IE. NYERI DADA / PERUT -->
                            <div class="fitem">
                                <label>Nyeri Dada / Perut :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ie1_anamnesis" type="radio" name="Ie1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ie1_anamnesis" type="radio" name="Ie1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ie2_anamnesis" type="radio" name="Ie2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ie2_anamnesis" type="radio" name="Ie2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ie3_anamnesis" type="radio" name="Ie3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ie3_anamnesis" type="radio" name="Ie3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ie3_anamnesis" type="radio" name="Ie3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- IF. GANGGUAN PENCERNAAN -->
                            <div class="fitem">
                                <label>Gangguan Pencernaan :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="If1_anamnesis" type="radio" name="If1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="If1_anamnesis" type="radio" name="If1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="If2_anamnesis" type="radio" name="If2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="If2_anamnesis" type="radio" name="If2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="If3_anamnesis" type="radio" name="If3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="If3_anamnesis" type="radio" name="If3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="If3_anamnesis" type="radio" name="If3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- IG. BATUK KRONIS -->
                            <div class="fitem">
                                <label>Batuk Kronis :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ig1_anamnesis" type="radio" name="Ig1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ig1_anamnesis" type="radio" name="Ig1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ig2_anamnesis" type="radio" name="Ig2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ig2_anamnesis" type="radio" name="Ig2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ig3_anamnesis" type="radio" name="Ig3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ig3_anamnesis" type="radio" name="Ig3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ig3_anamnesis" type="radio" name="Ig3" value="3"> >12 Bulan 
                            </div><hr>
                        <!-- IH. SUARA SERAK -->
                            <div class="fitem">
                                <label>Suara Serak :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ih1_anamnesis" type="radio" name="Ih1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ih1_anamnesis" type="radio" name="Ih1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Lokasi :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ih2_anamnesis" type="radio" name="Ih2" value="1"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ih2_anamnesis" type="radio" name="Ih2" value="2"> Kanan 
                            </div>
                            <div class="fitem">
                                <label>Lama :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ih3_anamnesis" type="radio" name="Ih3" value="1"> 0-6 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ih3_anamnesis" type="radio" name="Ih3" value="2"> 6-12 Bulan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ih3_anamnesis" type="radio" name="Ih3" value="3"> >12 Bulan 
                            </div><hr>
                        </div>
                    </div>
                    <div id="ht" title="Riwayat Keluarga">
                        <div class="col col-xs-9"><br>
                        <!-- JA. RIWAYAT KELUARGA -->
                            <div class="fitem">
                                <label>Kanker Keluarga :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja1_anamnesis" type="radio" name="Ja1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja1_anamnesis" type="radio" name="Ja1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label> Jika Ya, sebutkan jenis kanker : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja2_anamnesis" type="radio" name="Ja2" value="1"> Sama 
                            </div>
                            <div class="fitem">
                                <label> Jika Ya, sebutkan jenis kanker : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja2_anamnesis" type="radio" name="Ja2" value="2"> Ca Cervic
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja2_anamnesis" type="radio" name="Ja2" value="3"> Ca Mammae 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja2_anamnesis" type="radio" name="Ja2" value="4"> Ca Prostat 
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja2_anamnesis" type="radio" name="Ja2" value="5"> Ca Paru
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja2_anamnesis" type="radio" name="Ja2" value="6"> Ca Kolon 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja2_anamnesis" type="radio" name="Ja2" value="7"> Ca Lainnya <input id="Ja2Lainnya_anamnesis" name="Ja2Lainnya" class="easyui-textbox" type="text" style="width: 30%;" /> 
                            </div><hr>
                            <div class="fitem">
                                <label> Jika Ya, sebutkan hubungan keluarga : </label> 
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja3_anamnesis" type="radio" name="Ja3" value="1"> Kakek 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja3_anamnesis" type="radio" name="Ja3" value="2"> Nenek 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja3_anamnesis" type="radio" name="Ja3" value="3"> Ayah 
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja3_anamnesis" type="radio" name="Ja3" value="4"> Ibu 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja3_anamnesis" type="radio" name="Ja3" value="5"> Paman / Ibu 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja3_anamnesis" type="radio" name="Ja3" value="6"> Kakak / Adik 
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja3_anamnesis" type="radio" name="Ja3" value="7"> Anak
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ja3_anamnesis" type="radio" name="Ja3" value="8"> Lainnya, Sebutkan <input id="Ja3Lainnya_anamnesis" name="Ja3Lainnya" class="easyui-textbox" type="text" style="width: 30%;" />
                            </div><hr>
                        </div>
                    </div>
                    <div id="ht" title="Faktor Risiko">
                        <div class="col col-xs-7"><br>
                        <!-- KA. KONSUMSI ALKOHOL -->
                            <div class="fitem">
                                <label>Konsumsi Alkohol :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ka1_anamnesis" type="radio" name="Ka1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ka1_anamnesis" type="radio" name="Ka1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Frekuensi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ka2_anamnesis" type="radio" name="Ka2" value="1"> 1-3X / Minggu 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ka2_anamnesis" type="radio" name="Ka2" value="2"> 3-7X / Minggu  
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ka2_anamnesis" type="radio" name="Ka2" value="3"> 7X / Minggu 
                            </div>
                            <div class="fitem">
                                <label>Lamanya : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ka3_anamnesis" type="radio" name="Ka3" value="1"> 0 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ka3_anamnesis" type="radio" name="Ka3" value="2"> 12 - 60 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ka3_anamnesis" type="radio" name="Ka3" value="3"> > 60 Bulan
                            </div>
                            <div class="fitem">
                                <label>Jumlah : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ka4_anamnesis" type="radio" name="Ka4" value="1"> <1 Gelas/hari 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Ka4_anamnesis" type="radio" name="Ka4" value="2"> Tidak>1 Gelas/hari 
                            </div><hr>
                        <!-- KB. RIWAYAT MEROKOK -->
                            <div class="fitem">
                                <label>Riwayat Merokok :</label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Kb1_anamnesis" type="radio" name="Kb1" value="1"> Ya
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Kb1_anamnesis" type="radio" name="Kb1" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label>Frekuensi : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Kb2_anamnesis" type="radio" name="Kb2" value="1"> 1-3X / Minggu 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Kb2_anamnesis" type="radio" name="Kb2" value="2"> 3-7X / Minggu  
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Kb2_anamnesis" type="radio" name="Kb2" value="3"> 7X / Minggu 
                            </div>
                            <div class="fitem">
                                <label>Lamanya : </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Kb3_anamnesis" type="radio" name="Kb3" value="1"> 0 - 12 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Kb3_anamnesis" type="radio" name="Kb3" value="2"> 12 - 60 Bulan
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Kb3_anamnesis" type="radio" name="Kb3" value="3"> > 60 Bulan
                            </div>
                            <div class="fitem">
                                <label>Jumlah : </label>                                 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Kb4_anamnesis" type="radio" name="Kb4" value="1"> 0-12 Batang/hari 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Kb4_anamnesis" type="radio" name="Kb4" value="2"> 10-24 Batang/hari 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="Kb4_anamnesis" type="radio" name="Kb4" value="3"> >24 Batang/hari 
                            </div><hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="dglAnamnesi-buttons"> 
    <a href="#" class="easyui-linkbutton" onclick="simpanAnamnesi()"><span class="glyphicon glyphicon-save" aria-hidden="true"></span> Simpan</a> 
    <a href="#" class="easyui-linkbutton" onclick="batalAnamnesi()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</a>
</div>

<script type="text/javascript" src="{base_url}asset/js/helper/tanggal.js"></script>
<script type="text/javascript" src="{base_url}asset/js/helper/pasien.js"></script>

<script type="text/javascript">
    format_tanggal("#tgl_periksa_anamnesis");
    datebox_tanggal("#tgl_periksa_anamnesis");
    combobox_pasien('#id_pasien_anamnesis', '{base_url}');
    var method;
    var id;
    
    
    function get_optradio(p){
       var temp=[];
        temp.push($("input[type='radio'][name='"+p+"']:checked").val());
       if($('#'+p+'L1').length){
            console.log($('#'+p+'L1').textbox('getValue'));
            temp.push($('#'+p+'L1').textbox('getValue'));
        }
        return temp.join(':');
    }
   
    function set_optradio(val,id){
        var k = val.split(':');
        $('#'+id+k[0]).prop('checked',true);
        $('#'+id+'L1').textbox({value:k[1]});        
   }
   
    function disable(id){
        $('#'+id).textbox({
            disabled:true,
            value:''
        });
    }

    function enable(id,param){
        if(param=true){
        $('#'+id).radio({
            disabled:false
        });
        }else{
        $('#'+id).radio({
            disabled:true
        });    
        }
    }

    function get_checkbox(p){
        var temp =[];
        $("input[name='"+p+"[]']:checked").each(function (){
            temp.push($(this).val());
        });
        if($('#'+p+'L1').length){
            temp.push('@'+$('#'+p+'L1').val());
        }
        if($('#'+p+'L2').length){
            temp.push('@'+$('#'+p+'L2').val());
        }
        return temp.join(':');
    }
    
    function set_checkbox(val,id){
        var k = val.split(':');
        var ord = 1;
        for(var i=0;i<k.length;i++){
            if(isNaN(k[i])){
                $('#'+id+'L'+ord).textbox({value:k[i].replace('@','')});
                ord++;
            }else{
                $('#'+id+k[i]).prop('checked',true);
            }
        }
    }
    
    
    $('#dgAnamnesi').datagrid({
        url: '{base_url}index.php/proses/anamnesis/get_page',
        title: 'Kunjungan Anamnesia',
        fit: true, border:false, striped:true, singleSelect: true, rownumbers: true, pagination: true, toolbar: '#tbAnamnesi',
        columns: [[
                {field: 'id_anamnesis', title: 'id', hidden: true},
                {field: 'nama_pasien', width: 100, title: 'Nama Pasien'},
                {field: 'kunjungan', width: 100, title: 'Kunjungan '},
                {field: 'tgl_periksa', width: 120, title: 'Tanggal Periksa', formatter: function (value, row, index) {
                            return decode_tanggal(value);
                        }}
                //{field: 'status', width: 50, title: 'Status', formatter: function (value, row, index) {return value == 1 ? 'Terminasi' : 'Aktif';}}
            ]]
    });
    
    function tambahAnamnesi() {
        $('#fmAnamnesi').form('clear');
        $('#dglAnamnesi').dialog('open').dialog('setTitle', 'Tambah');
        method='tambah';
        id='';
    }
    
    function ubahAnamnesi() {
        var row = $('#dgAnamnesi').datagrid('getSelected');
		console.log(row);
        if (row) {
//            $('#fmAnamnesi').form('clear');
            $('#fmAnamnesi').form('load', row);
            
//            TAB KARSINOMA NASOFARING (A)

            set_optradio(row.Aa1,'Aa1_anamnesis');
            set_optradio(row.Aa2,'Aa2_anamnesis');
            set_optradio(row.Aa3,'Aa3_anamnesis');            
            set_optradio(row.Ab1,'Ab1_anamnesis');
            set_optradio(row.Ab2,'Ab2_anamnesis');
            set_optradio(row.Ab3,'Ab3_anamnesis');            
            set_optradio(row.Ac1,'Ac1_anamnesis');
            set_optradio(row.Ac2,'Ac2_anamnesis');
            set_optradio(row.Ac3,'Ac3_anamnesis');            
            set_optradio(row.Ad1,'Ad1_anamnesis');
            set_optradio(row.Ad2,'Ad2_anamnesis');
            set_optradio(row.Ad3,'Ad3_anamnesis');            
            set_optradio(row.Ae1,'Ae1_anamnesis');
            set_optradio(row.Ae2,'Ae2_anamnesis');
            set_optradio(row.Ae3,'Ae3_anamnesis');            
            set_optradio(row.Af1,'Af1_anamnesis');
            set_optradio(row.Af2,'Af2_anamnesis');
            set_optradio(row.Af3,'Af3_anamnesis');            
            set_optradio(row.Ag1,'Ag1_anamnesis');
            set_optradio(row.Ag2,'Ag2_anamnesis');
            set_optradio(row.Ag3,'Ag3_anamnesis');            
            set_optradio(row.Ah1,'Ah1_anamnesis');
            set_optradio(row.Ah2,'Ah2_anamnesis');
            set_optradio(row.Ah3,'Ah3_anamnesis');            
            set_optradio(row.Ai1,'Ai1_anamnesis');
            set_optradio(row.Ai2,'Ai2_anamnesis');
            set_optradio(row.Ai3,'Ai3_anamnesis');            
            set_optradio(row.Aj1,'Aj1_anamnesis');
            set_optradio(row.Aj2,'Aj2_anamnesis');
            set_optradio(row.Aj3,'Aj3_anamnesis');            
            set_optradio(row.Ak1,'Ak1_anamnesis');
            set_optradio(row.Ak2,'Ak2_anamnesis');
            set_optradio(row.Ak3,'Ak3_anamnesis');            
            set_optradio(row.Al1,'Al1_anamnesis');
            set_optradio(row.Al2,'Al2_anamnesis');
            set_optradio(row.Al3,'Al3_anamnesis');            
            set_optradio(row.Am1,'Am1_anamnesis');
            set_optradio(row.Am2,'Am2_anamnesis');
            set_optradio(row.Am3,'Am3_anamnesis');
            
//            TAB TUMOR VACUM NASI (B)

            set_optradio(row.Ba1,'Ba1_anamnesis');
            set_optradio(row.Ba2,'Ba2_anamnesis');
            set_optradio(row.Ba3,'Ba3_anamnesis');            
            set_optradio(row.Bb1,'Bb1_anamnesis');
            set_optradio(row.Bb2,'Bb2_anamnesis');
            set_optradio(row.Bb3,'Bb3_anamnesis');            
            set_optradio(row.Bc1,'Bc1_anamnesis');
            set_optradio(row.Bc2,'Bc2_anamnesis');
            set_optradio(row.Bc3,'Bc3_anamnesis');            
            set_optradio(row.Bd1,'Bd1_anamnesis');
            set_optradio(row.Bd2,'Bd2_anamnesis');
            set_optradio(row.Bd3,'Bd3_anamnesis');            
            set_optradio(row.Be1,'Be1_anamnesis');
            set_optradio(row.Be2,'Be2_anamnesis');
            set_optradio(row.Be3,'Be3_anamnesis');            
            set_optradio(row.Bf1,'Bf1_anamnesis');
            set_optradio(row.Bf2,'Bf2_anamnesis');
            set_optradio(row.Bf3,'Bf3_anamnesis');            
            set_optradio(row.Bg1,'Bg1_anamnesis');
            set_optradio(row.Bg2,'Bg2_anamnesis');
            set_optradio(row.Bg3,'Bg3_anamnesis');            
            set_optradio(row.Bh1,'Bh1_anamnesis');
            set_optradio(row.Bh2,'Bh2_anamnesis');
            set_optradio(row.Bh3,'Bh3_anamnesis');            
            set_optradio(row.Bi1,'Bi1_anamnesis');
            set_optradio(row.Bi2,'Bi2_anamnesis');
            set_optradio(row.Bi3,'Bi3_anamnesis');            
            set_optradio(row.Bj1,'Bj1_anamnesis');
            set_optradio(row.Bj2,'Bj2_anamnesis');
            set_optradio(row.Bj3,'Bj3_anamnesis');
            
//            TAB TUMOR ORAL CAVITY (C)

            set_optradio(row.Ca1,'Ca1_anamnesis');
            set_optradio(row.Ca2,'Ca2_anamnesis');
            set_optradio(row.Ca3,'Ca3_anamnesis');
            set_optradio(row.Cb1,'Cb1_anamnesis');
            set_optradio(row.Cb2,'Cb2_anamnesis');
            set_optradio(row.Cb3,'Cb3_anamnesis');
            set_optradio(row.Cc1,'Cc1_anamnesis');
            set_optradio(row.Cc2,'Cc2_anamnesis');
            set_optradio(row.Cc3,'Cc3_anamnesis');
            set_optradio(row.Cd1,'Cd1_anamnesis');
            set_optradio(row.Cd2,'Cd2_anamnesis');
            set_optradio(row.Cd3,'Cd3_anamnesis');
            set_optradio(row.Ce1,'Ce1_anamnesis');
            set_optradio(row.Ce2,'Ce2_anamnesis');
            set_optradio(row.Ce3,'Ce3_anamnesis');
            set_optradio(row.Cf1,'Cf1_anamnesis');
            set_optradio(row.Cf2,'Cf2_anamnesis');
            set_optradio(row.Cf3,'Cf3_anamnesis');
            
//            TAB RUMOR OROFARING (D)

            set_optradio(row.Da1,'Da1_anamnesis');
            set_optradio(row.Da2,'Da2_anamnesis');
            set_optradio(row.Da3,'Da3_anamnesis');
            set_optradio(row.Db1,'Db1_anamnesis');
            set_optradio(row.Db2,'Db2_anamnesis');
            set_optradio(row.Db3,'Db3_anamnesis');
            set_optradio(row.Dc1,'Dc1_anamnesis');
            set_optradio(row.Dc2,'Dc2_anamnesis');
            set_optradio(row.Dc3,'Dc3_anamnesis');
            set_optradio(row.Dd1,'Dd1_anamnesis');
            set_optradio(row.Dd2,'Dd2_anamnesis');
            set_optradio(row.Dd3,'Dd3_anamnesis');
            set_optradio(row.De1,'De1_anamnesis');
            set_optradio(row.De2,'De2_anamnesis');
            set_optradio(row.De3,'De3_anamnesis');
            set_optradio(row.Df1,'Df1_anamnesis');
            set_optradio(row.Df2,'Df2_anamnesis');
            set_optradio(row.Df3,'Df3_anamnesis');
            
//            TAB ANGIOFIBROMA NASOFARING (E)

            set_optradio(row.Ea1,'Ea1_anamnesis');
            set_optradio(row.Ea2,'Ea2_anamnesis');
            set_optradio(row.Ea3,'Ea3_anamnesis');
            set_optradio(row.Eb1,'Eb1_anamnesis');
            set_optradio(row.Eb2,'Eb2_anamnesis');
            set_optradio(row.Eb3,'Eb3_anamnesis');
            set_optradio(row.Ec1,'Ec1_anamnesis');
            set_optradio(row.Ec2,'Ec2_anamnesis');
            set_optradio(row.Ec3,'Ec3_anamnesis');
            set_optradio(row.Ed1,'Ed1_anamnesis');
            set_optradio(row.Ed2,'Ed2_anamnesis');
            set_optradio(row.Ed3,'Ed3_anamnesis');
            set_optradio(row.Ee1,'Ee1_anamnesis');
            set_optradio(row.Ee2,'Ee2_anamnesis');
            set_optradio(row.Ee3,'Ee3_anamnesis');
            set_optradio(row.Ef1,'Ef1_anamnesis');
            set_optradio(row.Ef2,'Ef2_anamnesis');
            
            document.getElementById("Eg1_anamnesis").checked = (row.Eg1 == 1) ? true : false;
            document.getElementById("Eg2_anamnesis").checked = (row.Eg2 == 1) ? true : false;
            document.getElementById("Eg3_anamnesis").checked = (row.Eg3 == 1) ? true : false;
            document.getElementById("Eg4_anamnesis").checked = (row.Eg4 == 1) ? true : false;
            document.getElementById("Eg5_anamnesis").checked = (row.Eg5 == 1) ? true : false;
//            document.getElementById("Eg6_anamnesis").checked = (row.Eg6 == 1) ? true : false;
            
//            TAB TUMOR LARING (F)
            
            set_optradio(row.Fa1,'Fa1_anamnesis');
            set_optradio(row.Fa2,'Fa2_anamnesis');
            set_optradio(row.Fb1,'Fb1_anamnesis');
            set_optradio(row.Fb2,'Fb2_anamnesis');
            set_optradio(row.Fc1,'Fc1_anamnesis');
            set_optradio(row.Fc2,'Fc2_anamnesis');
            set_optradio(row.Fd1,'Fd1_anamnesis');
            set_optradio(row.Fd2,'Fd2_anamnesis');
            set_optradio(row.Fe1,'Fe1_anamnesis');
            set_optradio(row.Fe2,'Fe2_anamnesis');
            set_optradio(row.Ff1,'Ff1_anamnesis');
            set_optradio(row.Ff2,'Ff2_anamnesis');
            set_optradio(row.Fg1,'Fg1_anamnesis');
            set_optradio(row.Fg2,'Fg2_anamnesis');
            
//            TAB TUMOR PAROTIS (G)
            
            set_optradio(row.Ga1,'Ga1_anamnesis');
            set_optradio(row.Ga2,'Ga2_anamnesis');
            set_optradio(row.Ga3,'Ga3_anamnesis');
            set_optradio(row.Gb1,'Gb1_anamnesis');
            set_optradio(row.Gb2,'Gb2_anamnesis');
            set_optradio(row.Gb3,'Gb3_anamnesis');
            set_optradio(row.Gc1,'Gc1_anamnesis');
            set_optradio(row.Gd1,'Gd1_anamnesis');
            set_optradio(row.Gd2,'Gd2_anamnesis');
            
//            TAB TUMOR TELINGA (H)
            
            set_optradio(row.Ha1,'Ha1_anamnesis');
            set_optradio(row.Ha2,'Ha2_anamnesis');
            set_optradio(row.Ha3,'Ha3_anamnesis');
            set_optradio(row.Hb1,'Hb1_anamnesis');
            set_optradio(row.Hb2,'Hb2_anamnesis');
            set_optradio(row.Hb3,'Hb3_anamnesis');
            set_optradio(row.Hc1,'Hc1_anamnesis');
            set_optradio(row.Hc2,'Hc2_anamnesis');
            set_optradio(row.Hc3,'Hc3_anamnesis');
            set_optradio(row.Hd1,'Hd1_anamnesis');
            set_optradio(row.Hd2,'Hd2_anamnesis');
            set_optradio(row.Hd3,'Hd3_anamnesis');
            set_optradio(row.He1,'He1_anamnesis');
            set_optradio(row.He2,'He2_anamnesis');
            set_optradio(row.He3,'He3_anamnesis');
            set_optradio(row.Hf1,'Hf1_anamnesis');
            set_optradio(row.Hf2,'Hf2_anamnesis');
            set_optradio(row.Hf3,'Hf3_anamnesis');
            set_optradio(row.Hg1,'Hg1_anamnesis');
            set_optradio(row.Hg2,'Hg2_anamnesis');
            set_optradio(row.Hg3,'Hg3_anamnesis');
            set_optradio(row.Hh1,'Hh1_anamnesis');
            set_optradio(row.Hh2,'Hh2_anamnesis');
            set_optradio(row.Hh3,'Hh3_anamnesis');
            set_optradio(row.Hi1,'Hi1_anamnesis');
            set_optradio(row.Hi2,'Hi2_anamnesis');
            set_optradio(row.Hi3,'Hi3_anamnesis');
            set_optradio(row.Hj1,'Hj1_anamnesis');
            set_optradio(row.Hj2,'Hj2_anamnesis');
            set_optradio(row.Hj3,'Hj3_anamnesis');
            
//            TAB TUMOR ESOFAGUS (I)
            
            set_optradio(row.Ia1,'Ia1_anamnesis');
            set_optradio(row.Ia2,'Ia2_anamnesis');
            set_optradio(row.Ia3,'Ia3_anamnesis');
            set_optradio(row.Ib1,'Ib1_anamnesis');
            set_optradio(row.Ib2,'Ib2_anamnesis');
            set_optradio(row.Ib3,'Ib3_anamnesis');
            set_optradio(row.Ic1,'Ic1_anamnesis');
            set_optradio(row.Ic2,'Ic2_anamnesis');
            set_optradio(row.Ic3,'Ic3_anamnesis');
            set_optradio(row.Id1,'Id1_anamnesis');
            set_optradio(row.Id2,'Id2_anamnesis');
            set_optradio(row.Id3,'Id3_anamnesis');
            set_optradio(row.Ie1,'Ie1_anamnesis');
            set_optradio(row.Ie2,'Ie2_anamnesis');
            set_optradio(row.Ie3,'Ie3_anamnesis');
            set_optradio(row.If1,'If1_anamnesis');
            set_optradio(row.If2,'If2_anamnesis');
            set_optradio(row.If3,'If3_anamnesis');
            set_optradio(row.Ig1,'Ig1_anamnesis');
            set_optradio(row.Ig2,'Ig2_anamnesis');
            set_optradio(row.Ig3,'Ig3_anamnesis');
            set_optradio(row.Ih1,'Ih1_anamnesis');
            set_optradio(row.Ih2,'Ih2_anamnesis');
            set_optradio(row.Ih3,'Ih3_anamnesis');
            
//            TAB RIWAYAT KELUARGA (J)
            
            set_optradio(row.Ja1,'Ja1_anamnesis');
            set_optradio(row.Ja2,'Ja2_anamnesis');
            set_optradio(row.Ja3,'Ja3_anamnesis');
            
//            TAB FAKTOR RISIKO (K)
            
            set_optradio(row.Ka1,'Ka1_anamnesis');
            set_optradio(row.Ka2,'Ka2_anamnesis');
            set_optradio(row.Ka3,'Ka3_anamnesis');
            set_optradio(row.Ka4,'Ka4_anamnesis');            
            set_optradio(row.Kb1,'Kb1_anamnesis');
            set_optradio(row.Kb2,'Kb2_anamnesis');
            set_optradio(row.Kb3,'Kb3_anamnesis');   
            set_optradio(row.Kb3,'Kb4_anamnesis');   
            
            $('#dglAnamnesi').dialog('open').dialog('setTitle', 'Ubah');
            method = 'ubah';
            id = row.id_anamnesis;
        }
    }
    
    function batalAnamnesi() {
        $('#dglAnamnesi').dialog('close');
    }
    
    function doSearch(value) {
        $('#dgAnamnesi').datagrid('load', {
            cari: value
        });
    }
    
    function simpanAnamnesi(){
         if ($('#fmAnamnesi').form('validate')) {
            $.messager.progress({title: 'Simpan Data'});
            $.ajax({
                url: '{base_url}index.php/proses/anamnesis/simpan',
                data: {
                    'method': method,
                    'id_anamnesis': id,
                    'id_pasien': $('#id_pasien_anamnesis').combobox('getValue'),
                    'kunjungan':$('#kunjungan_anamnesis').textbox('getText'),
                    'tgl_periksa':$('#tgl_periksa_anamnesis').datebox('getValue'),
                    
//                    KARSINOMA NASOFARING (A)

                    'Aa1':get_optradio('Aa1'),
                    'Aa2':get_optradio('Aa2'),
                    'Aa3':get_optradio('Aa3'),                    
                    'Ab1':get_optradio('Ab1'),
                    'Ab2':get_optradio('Ab2'),
                    'Ab3':get_optradio('Ab3'),                    
                    'Ac1':get_optradio('Ac1'),
                    'Ac2':get_optradio('Ac2'),
                    'Ac3':get_optradio('Ac3'),                    
                    'Ad1':get_optradio('Ad1'),
                    'Ad2':get_optradio('Ad2'),
                    'Ad3':get_optradio('Ad3'),                    
                    'Ae1':get_optradio('Ae1'),
                    'Ae2':get_optradio('Ae2'),
                    'Ae3':get_optradio('Ae3'),                    
                    'Af1':get_optradio('Af1'),
                    'Af2':get_optradio('Af2'),
                    'Af3':get_optradio('Af3'),                    
                    'Ag1':get_optradio('Ag1'),
                    'Ag2':get_optradio('Ag2'),
                    'Ag3':get_optradio('Ag3'),                    
                    'Ah1':get_optradio('Ah1'),
                    'Ah2':get_optradio('Ah2'),
                    'Ah3':get_optradio('Ah3'),                    
                    'Ai1':get_optradio('Ai1'),
                    'Ai2':get_optradio('Ai2'),
                    'Ai3':get_optradio('Ai3'),                    
                    'Aj1':get_optradio('Aj1'),
                    'Aj2':get_optradio('Aj2'),
                    'Aj3':get_optradio('Aj3'),                    
                    'Ak1':get_optradio('Ak1'),
                    'Ak2':get_optradio('Ak2'),
                    'Ak3':get_optradio('Ak3'),                    
                    'Al1':get_optradio('Al1'),
                    'Al2':get_optradio('Al2'),
                    'Al3':get_optradio('Al3'),                    
                    'Am1':get_optradio('Am1'),
                    'Am2':get_optradio('Am2'),
                    'Am3':get_optradio('Am3'),
                    
//                    TAB TUMOR VACUM NASI (B)

                    'Ba1':get_optradio('Ba1'),
                    'Ba2':get_optradio('Ba2'),
                    'Ba3':get_optradio('Ba3'),
                    'Bb1':get_optradio('Bb1'),
                    'Bb2':get_optradio('Bb2'),
                    'Bb3':get_optradio('Bb3'),
                    'Bc1':get_optradio('Bc1'),
                    'Bc2':get_optradio('Bc2'),
                    'Bc3':get_optradio('Bc3'),
                    'Bd1':get_optradio('Bd1'),
                    'Bd2':get_optradio('Bd2'),
                    'Bd3':get_optradio('Bd3'),
                    'Be1':get_optradio('Be1'),
                    'Be2':get_optradio('Be2'),
                    'Be3':get_optradio('Be3'),
                    'Bf1':get_optradio('Bf1'),
                    'Bf2':get_optradio('Bf2'),
                    'Bf3':get_optradio('Bf3'),
                    'Bg1':get_optradio('Bg1'),
                    'Bg2':get_optradio('Bg2'),
                    'Bg3':get_optradio('Bg3'),
                    'Bh1':get_optradio('Bh1'),
                    'Bh2':get_optradio('Bh2'),
                    'Bh3':get_optradio('Bh3'),
                    'Bi1':get_optradio('Bi1'),
                    'Bi2':get_optradio('Bi2'),
                    'Bi3':get_optradio('Bi3'),
                    'Bj1':get_optradio('Bj1'),
                    'Bj2':get_optradio('Bj2'),
                    'Bj3':get_optradio('Bj3'),
                    
//                    TAB TUMOR ORAL CAVITY (C)

                    'Ca1':get_optradio('Ca1'),
                    'Ca2':get_optradio('Ca2'),
                    'Ca3':get_optradio('Ca3'),
                    'Cb1':get_optradio('Cb1'),
                    'Cb2':get_optradio('Cb2'),
                    'Cb3':get_optradio('Cb3'),
                    'Cc1':get_optradio('Cc1'),
                    'Cc2':get_optradio('Cc2'),
                    'Cc3':get_optradio('Cc3'),
                    'Cd1':get_optradio('Cd1'),
                    'Cd2':get_optradio('Cd2'),
                    'Cd3':get_optradio('Cd3'),
                    'Ce1':get_optradio('Ce1'),
                    'Ce2':get_optradio('Ce2'),
                    'Ce3':get_optradio('Ce3'),
                    'Cf1':get_optradio('Cf1'),
                    'Cf2':get_optradio('Cf2'),
                    'Cf3':get_optradio('Cf3'),
                    
//                    TAB TUMOR OROFARNG (D)

                    'Da1':get_optradio('Da1'),
                    'Da2':get_optradio('Da2'),
                    'Da3':get_optradio('Da3'),
                    'Db1':get_optradio('Db1'),
                    'Db2':get_optradio('Db2'),
                    'Db3':get_optradio('Db3'),
                    'Dc1':get_optradio('Dc1'),
                    'Dc2':get_optradio('Dc2'),
                    'Dc3':get_optradio('Dc3'),
                    'Dd1':get_optradio('Dd1'),
                    'Dd2':get_optradio('Dd2'),
                    'Dd3':get_optradio('Dd3'),
                    'De1':get_optradio('De1'),
                    'De2':get_optradio('De2'),
                    'De3':get_optradio('De3'),
                    'Df1':get_optradio('Df1'),
                    'Df2':get_optradio('Df2'),
                    'Df3':get_optradio('Df3'),
                    
//                    TAB ANGIOFIBROMA NASOFARING (E)

                    'Ea1':get_optradio('Ea1'),
                    'Ea2':get_optradio('Ea2'),
                    'Ea3':get_optradio('Ea3'),
                    'Eb1':get_optradio('Eb1'),
                    'Eb2':get_optradio('Eb2'),
                    'Eb3':get_optradio('Eb3'),
                    'Ec1':get_optradio('Ec1'),
                    'Ec2':get_optradio('Ec2'),
                    'Ec3':get_optradio('Ec3'),
                    'Ed1':get_optradio('Ed1'),
                    'Ed2':get_optradio('Ed2'),
                    'Ed3':get_optradio('Ed3'),
                    'Ee1':get_optradio('Ee1'),
                    'Ee2':get_optradio('Ee2'),
                    'Ee3':get_optradio('Ee3'),
                    'Ef1':get_optradio('Ef1'),
                    'Ef2':get_optradio('Ef2'),
                    'Ef2Lainnya':$('#Ef2Lainnya').textbox('getValue'),                    
                    'Eg1': document.getElementById('Eg1_anamnesis').checked,
                    'Eg2': document.getElementById('Eg2_anamnesis').checked,
                    'Eg3': document.getElementById('Eg3_anamnesis').checked,
                    'Eg4': document.getElementById('Eg4_anamnesis').checked,
                    'Eg5': document.getElementById('Eg5_anamnesis').checked,
//                    'Eg6': document.getElementById('Eg6_anamnesis').checked,
                    'EgLainnya':$('#EgLainnya_anamnesis').textbox('getValue'),
                    
//                    TAB TUMOR LARING (F)

                    'Fa1':get_optradio('Fa1'),
                    'Fa2':get_optradio('Fa2'),
                    'Fb1':get_optradio('Fb1'),
                    'Fb2':get_optradio('Fb2'),
                    'Fc1':get_optradio('Fc1'),
                    'Fc2':get_optradio('Fc2'),
                    'Fd1':get_optradio('Fd1'),
                    'Fd2':get_optradio('Fd2'),
                    'Fe1':get_optradio('Fe1'),
                    'Fe2':get_optradio('Fe2'),
                    'Ff1':get_optradio('Ff1'),
                    'Ff2':get_optradio('Ff2'),
                    'Fg1':get_optradio('Fg1'),
                    'Fg2':get_optradio('Fg2'),
                    
//                    TAB TUMOR PAROTIS (G)

                    'Ga1':get_optradio('Ga1'),
                    'Ga2':get_optradio('Ga2'),
                    'Ga3':get_optradio('Ga3'),
                    'Gb1':get_optradio('Gb1'),
                    'Gb2':get_optradio('Gb2'),
                    'Gb3':get_optradio('Gb3'),
                    'Gc1':get_optradio('Gc1'),
                    'Gd1':get_optradio('Gd1'),
                    'Gd2':get_optradio('Gd2'),
                    
//                    TAB TUMOR TELINGA (H)

                    'Ha1':get_optradio('Ha1'),
                    'Ha2':get_optradio('Ha2'),
                    'Ha3':get_optradio('Ha3'),
                    'Hb1':get_optradio('Hb1'),
                    'Hb2':get_optradio('Hb2'),
                    'Hb3':get_optradio('Hb3'),
                    'Hc1':get_optradio('Hc1'),
                    'Hc2':get_optradio('Hc2'),
                    'Hc3':get_optradio('Hc3'),
                    'Hd1':get_optradio('Hd1'),
                    'Hd2':get_optradio('Hd2'),
                    'Hd3':get_optradio('Hd3'),
                    'He1':get_optradio('He1'),
                    'He2':get_optradio('He2'),
                    'He3':get_optradio('He3'),
                    'Hf1':get_optradio('Hf1'),
                    'Hf2':get_optradio('Hf2'),
                    'Hf3':get_optradio('Hf3'),
                    'Hg1':get_optradio('Hg1'),
                    'Hg2':get_optradio('Hg2'),
                    'Hg3':get_optradio('Hg3'),
                    'Hh1':get_optradio('Hh1'),
                    'Hh2':get_optradio('Hh2'),
                    'Hh3':get_optradio('Hh3'),
                    'Hi1':get_optradio('Hi1'),
                    'Hi2':get_optradio('Hi2'),
                    'Hi3':get_optradio('Hi3'),
                    'Hj1':get_optradio('Hj1'),
                    'Hj2':get_optradio('Hj2'),
                    'Hj3':get_optradio('Hj3'),
                    
//                    TAB TUMOR ESOFAGUS (I)

                    'Ia1':get_optradio('Ia1'),
                    'Ia2':get_optradio('Ia2'),
                    'Ia3':get_optradio('Ia3'),
                    'Ib1':get_optradio('Ib1'),
                    'Ib2':get_optradio('Ib2'),
                    'Ib3':get_optradio('Ib3'),
                    'Ic1':get_optradio('Ic1'),
                    'Ic2':get_optradio('Ic2'),
                    'Ic3':get_optradio('Ic3'),
                    'Id1':get_optradio('Id1'),
                    'Id2':get_optradio('Id2'),
                    'Id3':get_optradio('Id3'),
                    'Ie1':get_optradio('Ie1'),
                    'Ie2':get_optradio('Ie2'),
                    'Ie3':get_optradio('Ie3'),
                    'If1':get_optradio('If1'),
                    'If2':get_optradio('If2'),
                    'If3':get_optradio('If3'),
                    'Ig1':get_optradio('Ig1'),
                    'Ig2':get_optradio('Ig2'),
                    'Ig3':get_optradio('Ig3'),
                    'Ih1':get_optradio('Ih1'),
                    'Ih2':get_optradio('Ih2'),
                    'Ih3':get_optradio('Ih3'),
                    
//                    TAB RIWAYAT KELUARGA (J)
                    
                    'Ja1':get_optradio('Ja1'),
                    'Ja2':get_optradio('Ja2'),
                    'Ja2Lainnya':$('#Ja2Lainnya_anamnesis').textbox('getValue'),
                    'Ja3':get_optradio('Ja3'),
                    'Ja3Lainnya':$('#Ja3Lainnya_anamnesis').textbox('getValue'),
                    
//                    TAB FAKTOR RISIKO (K)
                    
                    'Ka1':get_optradio('Ka1'),
                    'Ka2':get_optradio('Ka2'),
                    'Ka3':get_optradio('Ka3'),
                    'Ka4':get_optradio('Ka4'),                    
                    'Kb1':get_optradio('Kb1'),
                    'Kb2':get_optradio('Kb2'),
                    'Kb3':get_optradio('Kb3'),
                    'Kb4':get_optradio('Kb4'),
                    
                    'user_id':'<?php echo $user_id;?>',
                    'modified': <?php echo date('Ymd');?>
                },
                type: 'post',
                success: function (data) {                    
                    var result = eval('(' + data + ')');
                    if (result.success) {
                        $.messager.show({title: 'Simpan', msg: 'Data Tersimpan.', showType: 'show'});
                        $('#dglAnamnesi').dialog('close');
                        $('#dgAnamnesi').datagrid('reload');
                    } else {
                        $.messager.alert('Error', result.msg, 'error');
                    }
                }
            });
            $.messager.progress('close');
        }
    }
    
    function hapusAnamnesi() {
        var row = $('#dgAnamnesi').datagrid('getSelected');
        if (row) {
            $.messager.confirm('Konfirmasi', 'Anda yakin akan menghapus data ini?', function (r) {
                if (r) {
                    $.post('{base_url}index.php/proses/anamnesis/hapus', {id_anamnesis: row.id_anamnesis}, function (result) {
                        if (result.success) {
                            $.messager.show({title: 'Hapus', msg: 'Data Terhapus.', showType: 'slide'});
                            $('#dgAnamnesi').datagrid('reload');
                        } else {
                            $.messager.alert('Error', result.msg, 'error');
                        }
                    }, 'json');
                }
            });
        }
    }
</script>