<div id="tbPemeriksaan_fisik">
    <a href="#" class="easyui-linkbutton" plain="true" onclick="tambahPemeriksaan_fisik()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah</a> 
    <a href="#" class="easyui-linkbutton" plain="true" onclick="ubahPemeriksaan_fisik()"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Ubah</a> 
    <a href="#" class="easyui-linkbutton" plain="true" onclick="hapusPemeriksaan_fisik()"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Hapus</a> 
    <!--<a href="#" class="easyui-linkbutton" plain="true" onclick="cetakIndividu()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Cetak</a>-->
    <div style="float: right; padding-top: 2px">
        <input id="cari" class="easyui-searchbox" style="width: 300px" data-options="searcher:doSearch,prompt:'Masukan kata pencarian'">
    </div>
</div>
<table id="dgPemeriksaan_fisik" fitcolumns="true"></table>
<div id="dglPemeriksaan_fisik" class="easyui-dialog" data-options="modal:true, closed:true" style="width: 90%; padding: 5px; max-height: 88%; top: 5%" buttons="#dglPemeriksaan_fisik-buttons">
    <div class="ftitle">Kunjungan Pemeriksaan Fisik</div>
    <form id="fmPemeriksaan_fisik" class="form-horizontal fm" method="post" novalidate="novalidate" enctype="multipart/form-data">
        <div class="container-fluid">
            <div class="row">
                <div class="col col-xs-8">
                    <div class="fitem">
                        <label>Nama Pasien :</label> 
                        <input id="id_pasien_pemeriksaan_fisik" name="id_pasien" class="easyui-combobox" required="true" type="text" maxlength="254" style="width: 55%;" />
                    </div>
                    <div class="fitem">
                        <label>Kunjungan Ke :</label> 
                        <input id="kunjungan_pemeriksaan_fisik" name="kunjungan" class="easyui-numberspinner" required="true" type="text" style="width: 55%;" />
                    </div>
                    <div class="fitem">
                        <label>Tanggal Periksa :</label> 
                        <input id="tgl_periksa_pemeriksaan_fisik" name="tgl_periksa" class="easyui-textbox" required="true" style="width: 55%;" />
                    </div>
                    <div class="fitem">
                        <label>TD :</label> 
                        <input id="td_pemeriksaan_fisik" name="td" class="easyui-textbox" type="text" style="width: 10%;" /> &nbsp mmHg
                        <label>TB :</label> 
                        <input id="tb_pemeriksaan_fisik" name="tb" class="easyui-textbox" type="text" style="width: 10%;" /> &nbsp Cm;
                    </div>
                    <div class="fitem">
                        <label>Nadi :</label> 
                        <input id="nadi_pemeriksaan_fisik" name="nadi" class="easyui-textbox" type="text" style="width: 10%;" /> &nbsp menit
                        <label>BB :</label> 
                        <input id="bb_pemeriksaan_fisik" name="bb" class="easyui-textbox" type="text" style="width: 10%;" /> &nbsp Kg
                    </div>
                </div>
                <div id="tabPemeriksaan_fisik" class="easyui-tabs" fit="true" border="false" style="height: 100%; padding: 1px;">
                    <div id="Pemeriksaan_fisik" title="Pemeriksaan Fisik">
                <!-- TENGGOROK -->
                <div class="col col-xs-11"><br>
                            <div class="fitem">
                                <label> Karnofsky scale : </label> 
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks1" type="radio" name="ks1" value="1"> 0 &nbsp; = &nbsp; Meninggal
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks1" type="radio" name="ks1" value="2"> 10 &nbsp; = &nbsp; Penderita sampai pada keadaan terminal (moribund), proses kematian sedang berlangsung.
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks1" type="radio" name="ks1" value="3"> 20 &nbsp; = &nbsp; Sangat terlihat sakit. Harus mendapatkan perawatan khusus di Rumah Sakit dan perawatan penunjang yang memadai.
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks1" type="radio" name="ks1" value="4"> 30 &nbsp; = &nbsp; Sangat tidak berdaya. Membutuhkan perawatan medis khusus di Rumah Sakit walaupun tidak dalam keadaan yang mengancam jiwa.
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks1" type="radio" name="ks1" value="5"> 40 &nbsp; = &nbsp; Membutuhkan perhatian medis secara khusus dan pendampingan.
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks1" type="radio" name="ks1" value="6"> 50 &nbsp; = &nbsp; Tidak dapat melakukan aktifitas sehari-hari. Sangat membutuhkan bantuan orang lain dan pengobatan medis.
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks1" type="radio" name="ks1" value="7"> 60 &nbsp; = &nbsp; Tidak dapat melakukan aktifitas sehari-hari. Membutuhkan bantuan orang lain pada keadaan yang sangat sulit, sebagian besar keperluan diri dapat dipenuhi oleh diri sendiri.
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks1" type="radio" name="ks1" value="8"> 70 &nbsp; = &nbsp; Tidak dapat melakukan aktifitas sehari-hari. Mampu menjaga keadaan diri sendiri (care for self).
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks1" type="radio" name="ks1" value="9"> 80 &nbsp; = &nbsp; Mampu melakukan aktifitas dengan usaha (effort). Didapatkan beberapa tanda gejala dari penyakit.
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks1" type="radio" name="ks1" value="10"> 90 &nbsp; = &nbsp; Mampu melakukan aktifitas sehari-hari, Didapatkan tanda minimal dari gejala dan penyakit.
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks1" type="radio" name="ks1" value="11"> 100 &nbsp; = &nbsp; Aktifitas normal, tidak ada keluhan, tidak didapatkan adanya penyakit.
                            </div><hr>
                        <!--NASEONDOSKOPI-->
                            <div class="fitem">
                                <label> Nasoendoskopi : </label>
                            </div>
                            <div class="fitem">
                                <label></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hidung Kanan</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Hidung Kiri</b>
                            </div>
                            <div class="fitem">
                                <label>Kavum nasi :</label>
                                <input id="ks2" name="ks2" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks3" name="ks3" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Konka inferior :</label>
                                <input id="ks4" name="ks4" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks5" name="ks5" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Konka media :</label>
                                <input id="ks6" name="ks6" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks7" name="ks7" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Meatus media, koana :</label>
                                <input id="ks8" name="ks8" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks9" name="ks9" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Torus tubarius :</label>
                                <input id="ks10" name="ks10" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks11" name="ks11" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Fossa Rossenmuller :</label>
                                <input id="ks12" name="ks12" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks13" name="ks13" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Atap Nasofaring :</label>
                                <input id="ks14" name="ks14" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks15" name="ks15" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Hipofaring :</label>
                                <input id="ks16" name="ks16" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks17" name="ks17" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Supra Glotis :</label>
                            </div>
                            <div class="fitem">
                                <label>&nbsp;&nbsp; - Plica Ariepiglotika :</label>
                                <input id="ks18" name="ks18" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks19" name="ks19" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>&nbsp;&nbsp; - Aritenoid :</label>
                                <input id="ks20" name="ks20" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks21" name="ks21" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>&nbsp;&nbsp; - Plica Ventrikularis :</label>
                                <input id="ks22" name="ks22" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks23" name="ks23" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Glotis :</label>
                                <input id="ks24" name="ks24" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks25" name="ks25" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Sub Glotis :</label>
                                <input id="ks26" name="ks26" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks27" name="ks27" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Rima Glotis :</label>
                                <input id="ks28" name="ks28" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks29" name="ks29" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div><hr>
                        <!--TENGGOROK-->
                            <div class="fitem">
                                <label> Tenggorok : </label>
                            </div>
                            <div class="fitem">
                                <label></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Kanan</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Kiri</b>
                            </div>
                            <div class="fitem">
                                <label>Tonsil :</label>
                                <input id="ks30" name="ks30" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks31" name="ks31" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Palatum molle :</label>
                                <input id="ks32" name="ks32" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks33" name="ks33" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Palatum durum :</label>
                                <input id="ks34" name="ks34" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks35" name="ks35" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Lidah :</label>
                                <input id="ks36" name="ks36" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks37" name="ks37" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Dasar Mulut :</label>
                                <input id="ks38" name="ks38" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks39" name="ks39" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Pangkal lidah :</label>
                                <input id="ks40" name="ks40" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks41" name="ks41" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <div class="fitem">
                                <label>Gigi geligi :</label>
                                <input id="ks42" name="ks42" class="easyui-textbox" type="text" style="width: 12%;" />
                                <input id="ks43" name="ks43" class="easyui-textbox" type="text" style="width: 12%;" />
                            </div>
                            <hr>
                        <!--TELINGA-->
                            <div class="fitem">
                                <label> Telinga </label>
                            </div>
                            <div class="fitem">
                                <label> Membran Timpani : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks55" type="radio" name="ks55" value="1"> Intak  
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks55" type="radio" name="ks55" value="2"> Perforasi 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks55" type="radio" name="ks55" value="3"> Tidak Dapat di Nilai
                            </div>
                            <div class="fitem">
                                <label> Jenis Perforasi : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks56" type="radio" name="ks56" value="1"> Sentral  
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks56" type="radio" name="ks56" value="2"> Marginal 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks56" type="radio" name="ks56" value="3"> Total 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks56" type="radio" name="ks56" value="4"> Tidak Dapat di Nilai
                            </div>
                            <div class="fitem">
                                <label> Sekret : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks57" type="radio" name="ks57" value="1"> Mukoid 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks57" type="radio" name="ks57" value="2"> Seromukoid 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks57" type="radio" name="ks57" value="3"> Mukopurulen 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks57" type="radio" name="ks57" value="4"> Serous 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks57" type="radio" name="ks57" value="5"> Purulen 
                            </div>
                            <div class="fitem">
                                <label> Parese N VII : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks58" type="radio" name="ks58" value="1"> Sentral 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks58" type="radio" name="ks58" value="2"> Perifer 
                            </div>
                            <div class="fitem">
                                <label> </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; Jika Perifer : &nbsp;&nbsp;&nbsp;&nbsp; : 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks59" type="radio" name="ks59" value="1"> HB I 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks59" type="radio" name="ks59" value="2"> HB II
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks59" type="radio" name="ks59" value="3"> HB III
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks59" type="radio" name="ks59" value="4"> HB IV
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks59" type="radio" name="ks59" value="5"> HB V
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks59" type="radio" name="ks59" value="6"> HB VI
                            </div><hr>
                        <!--PAROTIS-->
                            <div class="fitem">
                                <label> Parotis : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks44" type="radio" name="ks44" value="1"> Unilateral
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks44" type="radio" name="ks44" value="2"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks44" type="radio" name="ks44" value="3"> Kanan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks44" type="radio" name="ks44" value="4"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> Bentuk Tumor Parotis : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks45" type="radio" name="ks45" value="1"> Normal 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks45" type="radio" name="ks45" value="2"> Ulkus 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks45" type="radio" name="ks45" value="3"> Eksofitik 
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; Ukuran Tumor : <input id="ks46" name="ks46" class="easyui-textbox" type="text" style="width: 10%;" />
                            </div>
                            <div class="fitem">
                                <label> Parese Fasial : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks47" type="radio" name="ks47" value="1"> Ya 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks47" type="radio" name="ks47" value="2"> Tidak 
                            </div>
                            <div class="fitem">
                                <label> Jika Ya : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks48" type="radio" name="ks48" value="1"> Kiri
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks48" type="radio" name="ks48" value="2"> Kanan  
                            </div><hr>
                        <!--TIROID-->
                            <div class="fitem">
                                <label> Tiroid : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks49" type="radio" name="ks49" value="1"> Unilateral
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks49" type="radio" name="ks49" value="2"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks49" type="radio" name="ks49" value="3"> Kanan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks49" type="radio" name="ks49" value="4"> Bilateral
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; Ukuran Tumor : <input id="ks50" name="ks50" class="easyui-textbox" type="text" style="width: 10%;" /> 
                            </div><hr>
                        <!--KELENJAR GETAH BENING-->
                            <div class="fitem">
                                <label> Kelenjar Getah Bening : </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks51" type="radio" name="ks51" value="1"> Unilateral 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks51" type="radio" name="ks51" value="2"> Kiri 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks51" type="radio" name="ks51" value="3"> Kanan 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks51" type="radio" name="ks51" value="4"> Bilateral
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks51" type="radio" name="ks51" value="5"> Soliter
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks51" type="radio" name="ks51" value="6"> Multipel 
                            </div>
                            <div class="fitem">
                                <label> </label> 
                                &nbsp;&nbsp;&nbsp;&nbsp; Ukuran (dalam cm) : <input id="ks52" name="ks52" class="easyui-textbox" type="text" style="width: 10%;" />
                            </div>
                            <div class="fitem">
                                <label> </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; Deskripsi KGB : 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks53" type="radio" name="ks53" value="1"> Keras 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks53" type="radio" name="ks53" value="2"> Lunak 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks53" type="radio" name="ks53" value="3"> Terfiksir 
                            </div>
                            <div class="fitem">
                                <label> </label>
                                &nbsp;&nbsp;&nbsp;&nbsp; Lokasi  KGB &nbsp;&nbsp;&nbsp;&nbsp; : 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks54" type="radio" name="ks54" value="1"> Level I 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks54" type="radio" name="ks54" value="2"> Level II 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks54" type="radio" name="ks54" value="3"> Level III  
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks54" type="radio" name="ks54" value="4"> Level IV 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks54" type="radio" name="ks54" value="5"> Level V 
                                &nbsp;&nbsp;&nbsp;&nbsp; <input id="ks54" type="radio" name="ks54" value="6"> Level VI 
                            </div><hr>
                        </div>
                    </div>
                    <div id="Pemeriksaan_fisik" title="Pemeriksaan Penunjang">
                <!-- TENGGOROK -->
                        <div class="col col-xs-9">
                            <div class="fitem">
                                <label>BIOPSI</label>
                            </div>
                            <div class="fitem">
                                <label>Tanggal :</label>
                                <input id="a1" name="a1" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>RS :</label>
                                <input id="a2" name="a2" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>No.PA :</label>
                                <input id="a3" name="a3" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>Hasil :</label>
                                <input id="a4" name="a4" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            
                            <div class="fitem">
                                <label>Karsinoma Nasofaring : (C11.9)</label>
                            </div>
                            <div class="fitem">
                                <label>WHO :</label>
                                <input id="a5" name="a5" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>WF :</label>
                                <input id="a6" name="a6" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                
                            </div>
                            <div class="fitem">
                                <label style="float:left;">Sinus Paranasal : (C31.9)</label>
                                <div style="width:30%; float:left; margin-left: 5px;">
                                    <input id="a7" type="radio" name="a7" value="1"> Limfoma maligna <br>
                                    <input id="a7" type="radio" name="a7" value="2"> Karsinoma tidak berkeratin <br>
                                    <input id="a7" type="radio" name="a7" value="3"> Diferensiasi sedang buruk <br>
                                    <input id="a7" type="radio" name="a7" value="4"> T/NK Cell Lymphoma <br>
                                    <input id="a7" type="radio" name="a7" value="5"> Adenocystic carcinoma
                                </div>
                                <div style="width:30%; float:left; margin-left: 5px;">
                                    <input id="a7" type="radio" name="a7" value="6"> Karsinoma sel skuamosa <br>
                                    <input id="a7" type="radio" name="a7" value="7"> Diferensiasi baik <br>
                                    <input id="a7" type="radio" name="a7" value="8"> Rhabdomyosarcoma <br>
                                    <input id="a7" type="radio" name="a7" value="9"> AdenoCarsinoma <br>
                                    <input id="a7" type="radio" name="a7" value="10"> Lain sebutkan 
                                    <input id="a7a" name="a7a" class="easyui-textbox" type="text" style="width: 30%;" />
                                </div>
                            </div>                            
                            <div class="fitem">
                                <label style="float:left;">Laring :(Karsinoma sel skuamosa - C32.9)</label>
                                <div style="width:30%; float:left; margin-left: 5px;">
                                    <input id="a8" type="radio" name="a8" value="1"> Keratin <br>
                                    <input id="a8" type="radio" name="a8" value="2"> Diferensiasi baik <br>
                                    <input id="a8" type="radio" name="a8" value="3"> Diferensiasi buruk
                                </div>
                                <div style="width:30%; float:left; margin-left: 5px;">
                                    <input id="a8" type="radio" name="a8" value="4"> Tidak keratin <br>
                                    <input id="a8" type="radio" name="a8" value="5"> Diferensiasi sedang <br>
                                    <input id="a8" type="radio" name="a8" value="6"> Jenis lain: neuroendokrin, dll
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                            <hr>
                            
                            <div class="fitem">
                                <label>FNAB</label>                                
                            </div>
                            <div class="fitem">
                                <label>Tanggal :</label>
                                <input id="a9" name="a9" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>RS :</label>
                                <input id="a10" name="a10" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>No.PA :</label>
                                <input id="a11" name="a11" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            
                            <div class="fitem">
                                <label>Lokasi Lesi </label>
                            </div>
                            <div class="fitem">
                                <label>Tiroid :</label>
                                <input id="a12" type="radio" name="a12" value="1"> Kiri &nbsp;
                                <input id="a12" type="radio" name="a12" value="2"> Kanan
                            </div>
                            <div class="fitem">
                                <label>Parotis :</label>
                                <input id="a13" type="radio" name="a13" value="1"> Kiri &nbsp;
                                <input id="a13" type="radio" name="a13" value="2"> Kanan
                            </div>
                            <div class="fitem">
                                <label>Lokasi KGB:</label>
                                <input id="a14" type="radio" name="a14" value="1"> Kiri &nbsp;
                                <input id="a14" type="radio" name="a14" value="2"> Kanan
                            </div>
                            <div class="fitem">
                                <label></label>
                                <input id="a15" type="radio" name="a15" value="1"> Level I &nbsp;&nbsp;&nbsp;
                                <input id="a15" type="radio" name="a15" value="4"> Level IV
                            </div>
                            <div class="fitem">
                                <label></label>
                                <input id="a15" type="radio" name="a15" value="2"> Level II &nbsp;&nbsp;
                                <input id="a15" type="radio" name="a15" value="5"> Level V
                            </div>
                            <div class="fitem">
                                <label></label>
                                <input id="a15" type="radio" name="a15" value="3"> Level III &nbsp;
                                <input id="a15" type="radio" name="a15" value="6"> Level VI
                            </div>
                            <div class="fitem">
                                <label>Hasil :</label>
                                <input id="a16" name="a16" class="easyui-textbox" style="width: 55%;" />
                            </div><hr>
                            
                            <div class="fitem">
                                <label>Imaging </label>
                            </div>
                            <div class="fitem">
                                <label>CT Scan </label>
                            </div>
                            <div class="fitem">
                                <label>Tanggal :</label>
                                <input id="a17" name="a17" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>RS :</label>
                                <input id="a18" name="a18" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>No :</label>
                                <input id="a19" name="a19" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>File :</label>
                                <div class="fileUpload btn btn-xs btn-success">
                                    <span>Pilih File</span>
                                    <input id="a20L" name="a20L" type="file" accept="image/*" class="upload" onchange="previewFile(this)"/>
                                </div>
                                <input id="a20" name="a20" class="easyui-textbox" data-options="disabled:true" style="width: 47%;"/>
                            </div>
                            
                            <div class="fitem">
                                <label>MRI </label>
                            </div>
                            <div class="fitem">
                                <label>Tanggal :</label>
                                <input id="a21" name="a21" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>RS :</label>
                                <input id="a22" name="a22" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>No :</label>
                                <input id="a23" name="a23" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>File :</label>
                                <div class="fileUpload btn btn-xs btn-success">
                                    <span>Pilih File</span>
                                    <input id="a24L" name="a24L" type="file" accept="image/*" class="upload" onchange="previewFile(this)"/>
                                </div>
                                <input id="a24" name="a24" class="easyui-textbox" data-options="disabled:true" style="width: 47%;"/>
                            </div>
                            
                            <div class="fitem">
                                <label>Rontgen thorax </label>
                            </div>
                            <div class="fitem">
                                <label>Tanggal :</label>
                                <input id="a25" name="a25" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>RS :</label>
                                <input id="a26" name="a26" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>No :</label>
                                <input id="a27" name="a27" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>File :</label>
                                <div class="fileUpload btn btn-xs btn-success">
                                    <span>Pilih File</span>
                                    <input id="a28L" name="a28L" type="file" accept="image/*" class="upload" onchange="previewFile(this)"/>
                                </div>
                                <input id="a28" name="a28" class="easyui-textbox" data-options="disabled:true" style="width: 47%;"/>
                            </div>
                            
                            <div class="fitem">
                                <label>USG abdomen </label>
                            </div>
                            <div class="fitem">
                                <label>Tanggal :</label>
                                <input id="a29" name="a29" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>RS :</label>
                                <input id="a30" name="a30" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>No :</label>
                                <input id="a31" name="a31" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>File :</label>
                                <div class="fileUpload btn btn-xs btn-success">
                                    <span>Pilih File</span>
                                    <input id="a32L" name="a32L" type="file" accept="image/*" class="upload" onchange="previewFile(this)"/>
                                </div>
                                <input id="a32" name="a32" class="easyui-textbox" data-options="disabled:true" style="width: 47%;"/>
                            </div>
                            
                            <div class="fitem">
                                <label>Bone scan </label>
                            </div>
                            <div class="fitem">
                                <label>Tanggal :</label>
                                <input id="a33" name="a33" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>RS :</label>
                                <input id="a34" name="a34" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>No :</label>
                                <input id="a35" name="a35" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>File :</label>
                                <div class="fileUpload btn btn-xs btn-success">
                                    <span>Pilih File</span>
                                    <input id="a36L" name="a36L" type="file" accept="image/*" class="upload" onchange="previewFile(this)"/>
                                </div>
                                <input id="a36" name="a36" class="easyui-textbox" data-options="disabled:true" style="width: 47%;"/>
                            </div>
                            
                            <div class="fitem">
                                <label>USG leher :</label>
                            </div>
                            <div class="fitem">
                                <label>Tanggal :</label>
                                <input id="a37" name="a37" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>RS :</label>
                                <input id="a38" name="a38" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>No :</label>
                                <input id="a39" name="a39" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>File :</label>
                                <div class="fileUpload btn btn-xs btn-success">
                                    <span>Pilih File</span>
                                    <input id="a40L" name="a40L" type="file" accept="image/*" class="upload" onchange="previewFile(this)"/>
                                </div>
                                <input id="a40" name="a40" class="easyui-textbox" data-options="disabled:true" style="width: 47%;"/>
                            </div>
                            
                            <div class="fitem">
                                <label>USG tiroid </label>
                            </div>
                            <div class="fitem">
                                <label>Tanggal :</label>
                                <input id="a41" name="a41" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>RS :</label>
                                <input id="a42" name="a42" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>No :</label>
                                <input id="a43" name="a43" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>File :</label>
                                <div class="fileUpload btn btn-xs btn-success">
                                    <span>Pilih File</span>
                                    <input id="a44L" name="a44L" type="file" accept="image/*" class="upload" onchange="previewFile(this)"/>
                                </div>
                                <input id="a44" name="a44" class="easyui-textbox" data-options="disabled:true" style="width: 47%;"/>
                            </div>
                            
                            <div class="fitem">
                                <label>Hasil masing-masing :</label>
                                <input id="a45" name="a45" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>Metastase jauh</label>
                                <input id="a46" type="radio" name="a46" value="1"> Ya &nbsp;
                                <input id="a46" type="radio" name="a46" value="2"> Tidak
                            </div>
                            <div class="fitem">
                                <label style="float:left;">Lokasi</label>
                                <div style="width:15%; float:left; margin-left: 5px;">
                                    <input id="a47" type="radio" name="a47" value="1"> Paru <br>
                                    <input id="a47" type="radio" name="a47" value="2"> Tulang <br>
                                    <input id="a47" type="radio" name="a47" value="3"> Intrakranial <br>
                                </div>
                                <div style="width:15%; float:left; margin-left: 5px;">
                                    <input id="a47" type="radio" name="a47" value="4"> Hati <br>
                                    <input id="a47" type="radio" name="a47" value="5"> Ginjal
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                            <hr>
                            <div class="fitem">
                                <label>Audiometri Nada Murni : </label>
                            </div>
                            <div class="fitem">
                                <label>Tanggal : </label>
                                <input id="a48" name="a48" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>Rumah Sakit : </label>
                                <input id="a49" name="a49" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>Hasil : </label>
                            </div>
                            <div class="fitem">
                                <label>Telinga Kanan </label>
                                <input id="a50" name="a50" class="easyui-textbox" style="width: 20%;" />
                            </div>
                            <div class="fitem">
                                <label>Telinga Kiri </label>
                                <input id="a51" name="a51" class="easyui-textbox" style="width: 20%;" />
                            </div><hr>
                            
                            <div class="fitem">
                                <label>Timpanometri : </label>
                            </div>
                            <div class="fitem">
                                <label>Tanggal : </label>
                                <input id="a52" name="a52" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>Rumah Sakit : </label>
                                <input id="a53" name="a53" class="easyui-textbox" style="width: 55%;" />
                            </div>
                            <div class="fitem">
                                <label>Hasil : </label>
                            </div>
                            <div class="fitem">
                                <label>Telinga Kanan </label>
                                <input id="a54" name="a54" class="easyui-textbox" style="width: 20%;" />
                            </div>
                            <div class="fitem">
                                <label>Telinga Kiri </label>
                                <input id="a55" name="a55" class="easyui-textbox" style="width: 20%;" />
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div id="Pemeriksaan_fisik" title="Diagnosis">
                        <div class="col col-xs-9">
                            <div class="fitem">
                                <label>Tumor Meeting:</label>
                                <input id="b1" type="radio" name="b1" value="1"> Ya &nbsp;
                                <input id="b1" type="radio" name="b1" value="2"> Tidak &nbsp; 
                            </div><hr>
                            <div class="fitem" style="clear:both;">
                                <label style="float:left;">Stadium</label>
                                <div style="width:10%; float:left; margin-left: 5px;">
                                    <input id="b31" type="radio" name="b31" value="1"> T1 <br>
                                    <input id="b31" type="radio" name="b31" value="2"> T2 <br>
                                    <input id="b31" type="radio" name="b31" value="3"> T3 <br>
                                    <input id="b31" type="radio" name="b31" value="4"> T4a <br>
                                    <input id="b31" type="radio" name="b31" value="5"> T4b
                                </div>
                                <div style="width:10%; float:left; margin-left: 5px;">                                
                                    <input id="b31" type="radio" name="b31" value="6"> N1 <br>
                                    <input id="b31" type="radio" name="b31" value="7"> N2 <br>
                                    <input id="b31" type="radio" name="b31" value="8"> N2a <br>
                                    <input id="b31" type="radio" name="b31" value="9"> N2b <br>
                                    <input id="b31" type="radio" name="b31" value="10"> N3
                                </div>
                                <div style="width:10%; float:left; margin-left: 5px;">
                                    <input id="b31" type="radio" name="b31" value="11"> M0 <br>
                                    <input id="b31" type="radio" name="b31" value="12"> M1
                                </div>
                            </div><br><hr style="clear: both;">
                            <div class="fitem" style="clear:both;">
                                <label>Tanggal tumor meeting dilakukan:</label>
                                <input id="b2" name="b2" class="easyui-textbox" style="width: 55%;"/> 
                            </div>
                            <div class="fitem">
                                <label>Hasil tumor meeting:</label>
                            </div>
                            <div class="fitem">
                                <label>Diagnosa:</label>
                                <input id="b3" name="b3" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label style="float:left;">Rencana terapi:</label>
                                <div style="width:35%; float:left; margin-left: 5px;">
                                    <input id="b4" type="radio" name="b4" value="1"> Operasi <br>
                                    <input id="b4" type="radio" name="b4" value="2"> Kemoradiasi
                                </div>
                                <div style="width:35%; float:left; margin-left: 5px;">
                                    <input id="b4" type="radio" name="b4" value="3"> Operasi + Radioterapi/ kemoradiasi <br>
                                    <input id="b4" type="radio" name="b4" value="4"> Paliatif
                                </div>
                            </div><hr>
                            
                            <div class="fitem">
                                <label>Terapi</label>
                            </div>
                            <div class="fitem">
                                <label>Radioterapi:</label>
                                <input id="b5" type="radio" name="b5" value="1"> Jenis cobalt &nbsp;
                                <input id="b5" type="radio" name="b5" value="2"> 2D/3D &nbsp;
                            </div>
                            <div class="fitem">
                                <label></label>
                                <input id="b5" type="radio" name="b5" value="3"> IMRT &nbsp;
                            </div>
                            <div class="fitem">
                                <label>Mulai tanggal:</label>
                                <input id="b6" name="b6" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Selesai tanggal:</label>
                                <input id="b7" name="b7" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Dosis:</label>
                                <input id="b8" name="b8" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Frekuensi:</label>
                                <input id="b9" name="b9" class="easyui-textbox" style="width: 55%;" /> 
                            </div><hr>
                            
                            <div class="fitem">
                                <label>Neo ajuvan kemoterapi</label>
                            </div>
                            <div class="fitem">
                                <label>Mulai tanggal:</label>
                                <input id="b10" name="b10" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Selesai tanggal:</label>
                                <input id="b11" name="b11" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Jenis:</label>
                                <input id="b12" name="b12" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Dosis:</label>
                                <input id="b13" name="b13" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Jumlah pemberian:</label>
                                <input id="b14" type="radio" name="b14" value="1"> 3x &nbsp;
                                <input id="b14" type="radio" name="b14" value="2"> 6x &nbsp;
                            </div>
                            <div class="fitem">
                                <label></label>
                                <input id="b14" type="radio" name="b14" value="3"> >6x &nbsp;
                            </div><hr>
                            
                            <div class="fitem">
                                <label>Kemoradiasi</label>
                            </div>
                            <div class="fitem">
                                <label>Mulai tanggal:</label>
                                <input id="b15" name="b15" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Selesai tanggal:</label>
                                <input id="b16" name="b16" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Jenis radiasi:</label>
                                <input id="b17" type="radio" name="b17" value="1"> Jenis cobalt &nbsp;
                                <input id="b17" type="radio" name="b17" value="2"> 2D/3D &nbsp;
                            </div>
                            <div class="fitem">
                                <label></label>
                                <input id="b17" type="radio" name="b17" value="3"> IMRT &nbsp;
                            </div>
                            <div class="fitem">
                                <label>Jenis kemoterapi:</label>
                                <input id="b18" name="b18" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Dosis:</label>
                                <input id="b19" name="b19" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Jumlah pemberian:</label>
                                <input id="b20" type="radio" name="b20" value="1"> 3x &nbsp;
                                <input id="b20" type="radio" name="b20" value="2"> 6x &nbsp;
                            </div>
                            <div class="fitem">
                                <label></label>
                                <input id="b20" type="radio" name="b20" value="3"> >6x &nbsp;
                            </div><hr>
                            
                            <div class="fitem">
                                <label>Operasi:</label>
                                <input id="b21" type="radio" name="b21" value="1"> Ya &nbsp;
                                <input id="b21" type="radio" name="b21" value="2"> Tidak &nbsp;
                            </div>
                            <div class="fitem">
                                <label style="float:left;">Jenis operasi:</label>
                                <div style="width:35%; float:left; margin-left: 5px;">
                                    <input id="b22a" type="checkbox" name="b22a"> Maksiletomi <br>
                                    <input id="b22b" type="checkbox" name="b22b"> Parotidektomi <br>
                                    <input id="b22c" type="checkbox" name="b22c"> Mandibulektomi  <br>
                                    <input id="b22d" type="checkbox" name="b22d"> Laringektomi  <br>
                                </div>
                                <div style="width:35%; float:left; margin-left: 5px;">
                                    <input id="b22e" type="checkbox" name="b22e"> Hemiglosektomi  <br>
                                    <input id="b22f" type="checkbox" name="b22f"> Tiroidektomi  <br>
                                    <input id="b22g" type="checkbox" name="b22g"> Endoskopi approach 
                                </div>                                
                            </div><hr>
                            
                            <div class="fitem" style="clear: both;">
                                <label>Diseksi leher:</label>
                                <input id="b23" type="radio" name="b23" value="1"> Ya &nbsp;
                                <input id="b23" type="radio" name="b23" value="2"> Tidak &nbsp;
                            </div>
                            <div class="fitem">
                                <label>Jika ya, pilih:</label>
                                <input id="b24" type="radio" name="b24" value="1"> Radikal &nbsp;
                                <input id="b24" type="radio" name="b24" value="2"> Radikal modifikasi &nbsp;
                            </div>
                            <div class="fitem">
                                <label></label>
                                <input id="b24" type="radio" name="b24" value="3"> Selektif &nbsp;
                            </div><hr>
                            
                            <div class="fitem">
                                <label>Pasca terapi</label>
                            </div>
                            <div class="fitem">
                                <label style="float:left;">Follow up:</label>
                                <div style="width:35%; float:left; margin-left: 5px;">
                                    <input id="b25" type="radio" name="b25" value="1"> <3 bulan &nbsp;
                                    <input id="b25" type="radio" name="b25" value="2"> 6 bulan &nbsp;&nbsp;&nbsp;
                                    <input id="b25" type="radio" name="b25" value="3"> >12 bulan &nbsp;
                                </div>
                                <div style="width:35%; float:left; margin-left: 5px;">
                                    <input id="b25" type="radio" name="b25" value="4"> 3-6 bulan &nbsp;
                                    <input id="b25" type="radio" name="b25" value="5"> 12 bulan &nbsp;
                                </div>
                            </div>
                            <div class="fitem" style="clear:both;">
                                <label>Tanggal kunjungan follow up:</label>
                                <input id="b26" name="b26" class="easyui-textbox" style="width: 55%;" /> 
                            </div>
                            <div class="fitem">
                                <label>Anamnesis:</label>
                                <input id="b27" type="radio" name="b27" value="1"> Ada keluhan &nbsp;
                                <input id="b27" type="radio" name="b27" value="2"> Tidak ada keluhan &nbsp;
                            </div>
                            <div class="fitem">
                                <label>Klinis</label>
                            </div>
                            <div class="fitem">
                                <label>Lokal response:</label>
                                <input id="b28" type="radio" name="b28" value="1"> Ya &nbsp;
                                <input id="b28" type="radio" name="b28" value="2"> Tidak &nbsp;
                            </div>
                            <div class="fitem">
                                <label>Complete response:</label>
                                <input id="b29" type="radio" name="b29" value="1"> Ya &nbsp;
                                <input id="b29" type="radio" name="b29" value="2"> Tidak &nbsp;
                            </div>                            
                            <div class="fitem">
                                <label>Progresive:</label>
                                <input id="b30" type="radio" name="b30" value="1"> Ya &nbsp;
                                <input id="b30" type="radio" name="b30" value="2"> Tidak &nbsp;
                            </div><hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="dglPemeriksaan_fisik-buttons"> 
    <a href="#" class="easyui-linkbutton" onclick="simpanPemeriksaan_fisik()"><span class="glyphicon glyphicon-save" aria-hidden="true"></span> Simpan</a> 
    <a href="#" class="easyui-linkbutton" onclick="batalPemeriksaan_fisik()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</a>
</div>

<script type="text/javascript" src="{base_url}asset/js/helper/tanggal.js"></script>
<script type="text/javascript" src="{base_url}asset/js/helper/pasien.js"></script>
<script type="text/javascript" src="{base_url}asset/js/helper/lokasi.js"></script>
<script type="text/javascript" src="{base_url}asset/js/helper/nasoendoskopi.js"></script>
<script type="text/javascript" src="{base_url}asset/js/helper/rinofaringolaringoskopi.js"></script>
<script type="text/javascript" src="{base_url}asset/js/helper/tenggorok.js"></script>
<script type="text/javascript" src="{base_url}asset/js/helper/telinga.js"></script>

<script type="text/javascript">
    format_tanggal("#tgl_periksa_pemeriksaan_fisik");
    datebox_tanggal("#tgl_periksa_pemeriksaan_fisik");
    combobox_pasien('#id_pasien_pemeriksaan_fisik', '{base_url}');
    datebox_tanggal("#a1");
    datebox_tanggal("#a9");
    datebox_tanggal("#a17");
    datebox_tanggal("#a21");
    datebox_tanggal("#a25");
    datebox_tanggal("#a29");
    datebox_tanggal("#a33");
    datebox_tanggal("#a37");
    datebox_tanggal("#a41");
    datebox_tanggal("#a48");
    datebox_tanggal("#a52");
    datebox_tanggal("#b2");
    datebox_tanggal("#b6");
    datebox_tanggal("#b7");
    datebox_tanggal("#b10");
    datebox_tanggal("#b11");
    datebox_tanggal("#b15");
    datebox_tanggal("#b16");
    datebox_tanggal("#b26");
//    combobox_lokasi("#ks17");
    
    combobox_nasoendoskopi3("#ks2");
    combobox_nasoendoskopi3("#ks3");
    combobox_nasoendoskopi3("#ks4");
    combobox_nasoendoskopi3("#ks5");
    combobox_nasoendoskopi3("#ks6");
    combobox_nasoendoskopi3("#ks7");
    combobox_nasoendoskopi3("#ks8");
    combobox_nasoendoskopi3("#ks9");
    combobox_nasoendoskopi3("#ks10");
    combobox_nasoendoskopi3("#ks11");
    combobox_nasoendoskopi3("#ks12");
    combobox_nasoendoskopi3("#ks13");
    combobox_nasoendoskopi3("#ks14");
    combobox_nasoendoskopi3("#ks15");
    combobox_nasoendoskopi3("#ks16");
    combobox_nasoendoskopi3("#ks17");
    combobox_nasoendoskopi4("#ks18");
    combobox_nasoendoskopi4("#ks19");
    combobox_nasoendoskopi4("#ks20");
    combobox_nasoendoskopi4("#ks21");
    combobox_nasoendoskopi4("#ks22");
    combobox_nasoendoskopi4("#ks23");
    combobox_nasoendoskopi4("#ks24");
    combobox_nasoendoskopi4("#ks25");
    combobox_nasoendoskopi3("#ks26");
    combobox_nasoendoskopi3("#ks27");
    
    combobox_rinofaringolaringoskopi("#ks28");
    combobox_rinofaringolaringoskopi("#ks29");
    
    combobox_tonsil("#ks30");
    combobox_tonsil("#ks31");
    combobox_molle("#ks32");
    combobox_molle("#ks33");
    combobox_durum("#ks34");
    combobox_durum("#ks35");
    combobox_lidah("#ks36");
    combobox_lidah("#ks37");
    combobox_mulut("#ks38");
    combobox_mulut("#ks39");
    combobox_lidah("#ks40");
    combobox_lidah("#ks41");
    combobox_geligi("#ks42");
    combobox_geligi("#ks43");
    
    combobox_telinga("#a50");
    combobox_telinga("#a51");
    combobox_telinga("#a54");
    combobox_telinga("#a55");
    
    var a20L='';
    var a24L='';
    var a28L='';
    var a32L='';
    var a36L='';
    var a40L='';
    var a44L='';
    var method;
    var id;
    
    /*function show(val){
        
    }*/
    
    function previewFile(input) {
            $('#'+input.id.replace('L','')).textbox('setValue', input.files[0].name);
        }
    
    $('#dgPemeriksaan_fisik').datagrid({
        url: '{base_url}index.php/proses/pemeriksaan_fisik/get_page',
        title: 'Kunjungan Pemeriksaan Fisik',
        fit: true, border:false, striped:true, singleSelect: true, rownumbers: true, pagination: true, toolbar: '#tbPemeriksaan_fisik',
        columns: [[
                {field: 'id_pemeriksaan_fisik', title: 'id', hidden: true},
                {field: 'nama_pasien', width: 100, title: 'Nama Pasien'},
                {field: 'kunjungan', width: 100, title: 'Kunjungan '},
                {field: 'tgl_periksa', width: 120, title: 'Tanggal Periksa', formatter: function (value, row, index) {
                            return decode_tanggal(value);
                        }}
                //{field: 'status', width: 50, title: 'Status', formatter: function (value, row, index) {return value == 1 ? 'Terminasi' : 'Aktif';}}
            ]]
    });
    
    function tambahPemeriksaan_fisik() {
        $('#fmPemeriksaan_fisik').form('clear');
        $('#dglPemeriksaan_fisik').dialog('open').dialog('setTitle', 'Tambah');
        method='tambah';
        id='';
    }
    
    function ubahPemeriksaan_fisik() {
        var row = $('#dgPemeriksaan_fisik').datagrid('getSelected');
        if (row) {
            $('#fmPemeriksaan_fisik').form('clear');
            $('#fmPemeriksaan_fisik').form('load', row);            
            $('#dglPemeriksaan_fisik').dialog('open').dialog('setTitle', 'Ubah');
            method = 'ubah';
            id = row.id_pemeriksaan_fisik;
            document.getElementById("b22a").checked = (row.b22a == 1) ? true : false;
            document.getElementById("b22b").checked = (row.b22b == 1) ? true : false;
            document.getElementById("b22c").checked = (row.b22c == 1) ? true : false;
            document.getElementById("b22d").checked = (row.b22e == 1) ? true : false;
            document.getElementById("b22e").checked = (row.b22d == 1) ? true : false;
            document.getElementById("b22f").checked = (row.b22f == 1) ? true : false;
            document.getElementById("b22g").checked = (row.b22g == 1) ? true : false;
        }
    }
    
    function batalPemeriksaan_fisik() {
        $('#dglPemeriksaan_fisik').dialog('close');
    }
    
    function doSearch(value) {
        $('#dgPemeriksaan_fisik').datagrid('load', {
            cari: value
        });
    }
    
    function simpanPemeriksaan_fisik(){
         if ($('#fmPemeriksaan_fisik').form('validate')) {
            $.messager.progress({title: 'Simpan Data'});
            if (document.getElementById("a20L").files.length !== 0) {
                var formData = new FormData($('#fmPemeriksaan_fisik')[0]);
                $.ajax({
                    url: "{base_url}index.php/proses/pemeriksaan_fisik/upload/a20L",
                    type: "POST",
                    data: formData,
                    async: false,
                    success: function (data) {
                        var result = eval('(' + data + ')');
                        if (result.success) {
                            a20L = result.filename;
                            $.messager.show({title: 'File', msg: 'File tersimpan.', showType: 'show'});
                        } else {
                            $success = false;
                            $.messager.alert('Error', result.msg, 'error');
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
            if (document.getElementById("a24L").files.length !== 0) {
                var formData = new FormData($('#fmPemeriksaan_fisik')[0]);
                $.ajax({
                    url: "{base_url}index.php/proses/pemeriksaan_fisik/upload/a24L",
                    type: "POST",
                    data: formData,
                    async: false,
                    success: function (data) {
                        var result = eval('(' + data + ')');
                        if (result.success) {
                            a24L = result.filename;
                            $.messager.show({title: 'File', msg: 'File tersimpan.', showType: 'show'});
                        } else {
                            $success = false;
                            $.messager.alert('Error', result.msg, 'error');
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
            if (document.getElementById("a28L").files.length !== 0) {
                var formData = new FormData($('#fmPemeriksaan_fisik')[0]);
                $.ajax({
                    url: "{base_url}index.php/proses/pemeriksaan_fisik/upload/a28L",
                    type: "POST",
                    data: formData,
                    async: false,
                    success: function (data) {
                        var result = eval('(' + data + ')');
                        if (result.success) {
                            a28L = result.filename;
                            $.messager.show({title: 'File', msg: 'File tersimpan.', showType: 'show'});
                        } else {
                            $success = false;
                            $.messager.alert('Error', result.msg, 'error');
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
            if (document.getElementById("a32L").files.length !== 0) {
                var formData = new FormData($('#fmPemeriksaan_fisik')[0]);
                $.ajax({
                    url: "{base_url}index.php/proses/pemeriksaan_fisik/upload/a32L",
                    type: "POST",
                    data: formData,
                    async: false,
                    success: function (data) {
                        var result = eval('(' + data + ')');
                        if (result.success) {
                            a32L = result.filename;
                            $.messager.show({title: 'File', msg: 'File tersimpan.', showType: 'show'});
                        } else {
                            $success = false;
                            $.messager.alert('Error', result.msg, 'error');
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
            if (document.getElementById("a36L").files.length !== 0) {
                var formData = new FormData($('#fmPemeriksaan_fisik')[0]);
                $.ajax({
                    url: "{base_url}index.php/proses/pemeriksaan_fisik/upload/a36L",
                    type: "POST",
                    data: formData,
                    async: false,
                    success: function (data) {
                        var result = eval('(' + data + ')');
                        if (result.success) {
                            a36L = result.filename;
                            $.messager.show({title: 'File', msg: 'File tersimpan.', showType: 'show'});
                        } else {
                            $success = false;
                            $.messager.alert('Error', result.msg, 'error');
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
            if (document.getElementById("a40L").files.length !== 0) {
                var formData = new FormData($('#fmPemeriksaan_fisik')[0]);
                $.ajax({
                    url: "{base_url}index.php/proses/pemeriksaan_fisik/upload/a40L",
                    type: "POST",
                    data: formData,
                    async: false,
                    success: function (data) {
                        var result = eval('(' + data + ')');
                        if (result.success) {
                            a40L = result.filename;
                            $.messager.show({title: 'File', msg: 'File tersimpan.', showType: 'show'});
                        } else {
                            $success = false;
                            $.messager.alert('Error', result.msg, 'error');
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
            if (document.getElementById("a44L").files.length !== 0) {
                var formData = new FormData($('#fmPemeriksaan_fisik')[0]);
                $.ajax({
                    url: "{base_url}index.php/proses/pemeriksaan_fisik/upload/a44L",
                    type: "POST",
                    data: formData,
                    async: false,
                    success: function (data) {
                        var result = eval('(' + data + ')');
                        if (result.success) {
                            a44L = result.filename;
                            $.messager.show({title: 'File', msg: 'File tersimpan.', showType: 'show'});
                        } else {
                            $success = false;
                            $.messager.alert('Error', result.msg, 'error');
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
            $.ajax({
                url: '{base_url}index.php/proses/pemeriksaan_fisik/simpan',
                data: {
                    'method': method,
                    'id_pemeriksaan_fisik': id,
                    'id_pasien':$('#id_pasien_pemeriksaan_fisik').combobox('getValue'),
                    'kunjungan':$('#kunjungan_pemeriksaan_fisik').textbox('getValue'),
                    'tgl_periksa':$('#tgl_periksa_pemeriksaan_fisik').datebox('getValue'),
                    'td':$('#td_pemeriksaan_fisik').textbox('getValue'),
                    'nadi':$('#nadi_pemeriksaan_fisik').textbox('getValue'),
                    'tb':$('#tb_pemeriksaan_fisik').textbox('getValue'),
                    'bb':$('#bb_pemeriksaan_fisik').textbox('getValue'),
                    
                    'ks1': $('input[name=ks1]:checked').val(),
                    'ks2': $('#ks2').textbox('getValue'),
                    'ks3': $('#ks3').textbox('getValue'),
                    'ks4': $('#ks4').textbox('getValue'),
                    'ks5': $('#ks5').textbox('getValue'),
                    'ks6': $('#ks6').textbox('getValue'),
                    'ks7': $('#ks7').textbox('getValue'),
                    'ks8': $('#ks8').textbox('getValue'),
                    'ks9': $('#ks9').textbox('getValue'),
                    'ks10': $('#ks10').textbox('getValue'),
                    'ks11': $('#ks11').textbox('getValue'),
                    'ks12': $('#ks12').textbox('getValue'),
                    'ks13': $('#ks13').textbox('getValue'),
                    'ks14': $('#ks14').textbox('getValue'),
                    'ks15': $('#ks15').textbox('getValue'),
                    'ks16': $('#ks16').textbox('getValue'),
                    'ks17': $('#ks17').textbox('getValue'),
                    'ks18': $('#ks18').textbox('getValue'),
                    'ks19': $('#ks19').textbox('getValue'),
                    'ks20': $('#ks20').textbox('getValue'),
                    'ks21': $('#ks21').textbox('getValue'),
                    'ks22': $('#ks22').textbox('getValue'),
                    'ks23': $('#ks23').textbox('getValue'),
                    'ks24': $('#ks24').textbox('getValue'),
                    'ks25': $('#ks25').textbox('getValue'),
                    'ks26': $('#ks26').textbox('getValue'),
                    'ks27': $('#ks27').textbox('getValue'),
                    'ks28': $('#ks28').textbox('getValue'),
                    'ks29': $('#ks29').textbox('getValue'),
                    'ks30': $('#ks30').textbox('getValue'),
                    'ks31': $('#ks31').textbox('getValue'),
                    'ks32': $('#ks32').textbox('getValue'),
                    'ks33': $('#ks33').textbox('getValue'),
                    'ks34': $('#ks34').textbox('getValue'),
                    'ks35': $('#ks35').textbox('getValue'),
                    'ks36': $('#ks36').textbox('getValue'),
                    'ks37': $('#ks37').textbox('getValue'),
                    'ks38': $('#ks38').textbox('getValue'),
                    'ks39': $('#ks39').textbox('getValue'),
                    'ks40': $('#ks40').textbox('getValue'),
                    'ks41': $('#ks41').textbox('getValue'),
                    'ks42': $('#ks42').textbox('getValue'),
                    'ks43': $('#ks43').textbox('getValue'),
                    
                    'ks44': $('input[name=ks44]:checked').val(),
                    'ks45': $('input[name=ks45]:checked').val(),
                    'ks46': $('#ks46').textbox('getValue'),
                    'ks47': $('input[name=ks47]:checked').val(),
                    'ks48': $('input[name=ks48]:checked').val(),
                    'ks49': $('input[name=ks49]:checked').val(),
                    'ks50': $('#ks50').textbox('getValue'),
                    'ks51': $('input[name=ks48]:checked').val(),
                    'ks52': $('#ks52').textbox('getValue'),
                    'ks53': $('input[name=ks53]:checked').val(),
                    'ks54': $('input[name=ks54]:checked').val(),
                    'ks55': $('input[name=ks55]:checked').val(),
                    'ks56': $('input[name=ks56]:checked').val(),
                    'ks57': $('input[name=ks57]:checked').val(),
                    'ks58': $('input[name=ks58]:checked').val(),
                    'ks59': $('input[name=ks59]:checked').val(),
                    
                    'a1': $('#a1').datebox('getValue'),
                    'a2': $('#a2').textbox('getValue'),
                    'a3': $('#a3').textbox('getValue'),
                    'a4': $('#a4').textbox('getValue'),
                    'a5': $('#a5').textbox('getValue'),
                    'a6': $('#a6').textbox('getValue'),
                    'a7': $('input[name=a7]:checked').val(),
                    'a7a': $('#a7a').textbox('getValue'),
                    'a8': $('input[name=a8]:checked').val(),
                    'a9': $('#a9').textbox('getValue'),
                    'a10': $('#a10').textbox('getValue'),
                    'a11': $('#a11').textbox('getValue'),
                    'a12': $('input[name=a12]:checked').val(),
                    'a13': $('input[name=a13]:checked').val(),
                    'a14': $('input[name=a14]:checked').val(),
                    'a15': $('input[name=a15]:checked').val(),
                    'a16': $('#a16').textbox('getValue'),
                    'a17': $('#a17').datebox('getValue'),
                    'a18': $('#a18').textbox('getValue'),
                    'a19': $('#a19').textbox('getValue'),
                    'a20': a20L,
//                    'a20': $('#a20').textbox('getValue'),
                    'a21': $('#a21').datebox('getValue'),
                    'a22': $('#a22').textbox('getValue'),
                    'a23': $('#a23').textbox('getValue'),
                    'a24': a24L,
                    'a25': $('#a25').datebox('getValue'),
                    'a26': $('#a26').textbox('getValue'),
                    'a27': $('#a27').textbox('getValue'),
                    'a28': a28L,
                    'a29': $('#a29').datebox('getValue'),
                    'a30': $('#a30').textbox('getValue'),
                    'a31': $('#a31').textbox('getValue'),
                    'a32': a32L,
                    'a33': $('#a33').datebox('getValue'),
                    'a34': $('#a34').textbox('getValue'),
                    'a35': $('#a35').textbox('getValue'),
                    'a36': a36L,
                    'a37': $('#a37').datebox('getValue'),
                    'a38': $('#a38').textbox('getValue'),
                    'a39': $('#a39').textbox('getValue'),
                    'a40': a40L,
                    'a41': $('#a41').datebox('getValue'),
                    'a42': $('#a42').textbox('getValue'),
                    'a43': $('#a43').textbox('getValue'),
                    'a44': a44L,
                    'a45': $('#a45').textbox('getValue'),
                    'a46': $('input[name=a46]:checked').val(),
                    'a47': $('input[name=a47]:checked').val(),
                    'a48': $('#a48').textbox('getValue'),
                    'a49': $('#a49').textbox('getValue'),
                    'a50': $('#a50').textbox('getValue'),
                    'a51': $('#a51').textbox('getValue'),
                    'a52': $('#a52').textbox('getValue'),
                    'a53': $('#a53').textbox('getValue'),
                    'a54': $('#a54').textbox('getValue'),
                    'a55': $('#a55').textbox('getValue'),
                    
                    'b1': $('input[name=b1]:checked').val(),
                    'b2': $('#b2').datebox('getValue'),
                    'b3': $('#b3').textbox('getValue'),
                    'b4': $('input[name=b4]:checked').val(),
                    'b5': $('input[name=b5]:checked').val(),
                    'b6': $('#b6').datebox('getValue'),
                    'b7': $('#b7').datebox('getValue'),
                    'b8': $('#b8').textbox('getValue'),
                    'b9': $('#b9').textbox('getValue'),
                    'b10': $('#b10').datebox('getValue'),
                    'b11': $('#b11').datebox('getValue'),
                    'b12': $('#b12').textbox('getValue'),
                    'b13': $('#b13').textbox('getValue'),
                    'b14': $('input[name=b14]:checked').val(),
                    'b15': $('#b15').datebox('getValue'),
                    'b16': $('#b16').datebox('getValue'),
                    'b17': $('input[name=b17]:checked').val(),
                    'b18': $('#b18').textbox('getValue'),
                    'b19': $('#b19').textbox('getValue'),
                    'b20': $('input[name=b20]:checked').val(),
                    'b21': $('input[name=b21]:checked').val(),
//                    'b22': $('input[name=b22]:checked').val(),
                    'b22a': document.getElementById('b22a').checked,
                    'b22b': document.getElementById('b22b').checked,
                    'b22c': document.getElementById('b22c').checked,
                    'b22d': document.getElementById('b22d').checked,
                    'b22e': document.getElementById('b22e').checked,
                    'b22f': document.getElementById('b22f').checked,
                    'b22g': document.getElementById('b22g').checked,
                    
                    'b23': $('input[name=b23]:checked').val(),
                    'b24': $('input[name=b24]:checked').val(),
                    'b25': $('input[name=b25]:checked').val(),
                    'b26': $('#b26').textbox('getValue'),
                    'b27': $('input[name=b27]:checked').val(),
                    'b28': $('input[name=b28]:checked').val(),
                    'b29': $('input[name=b29]:checked').val(),
                    'b30': $('input[name=b30]:checked').val(),
                    'b31': $('input[name=b31]:checked').val(),
                    
                    'user_id':'<?php echo $user_id;?>',
                    'modified': <?php echo date('Ymd');?>
                },
                type: 'post',
                success: function (data) {                     
//                    var result = eval('(' + data + ')');
//                    if (result.success) {
                        $.messager.show({title: 'Simpan', msg: 'Data Tersimpan.', showType: 'show'});
                        $('#dglPemeriksaan_fisik').dialog('close');
                        $('#dgPemeriksaan_fisik').datagrid('reload');
//                    } else {
//                        $.messager.alert('Error', result.msg, 'error');
//                    }
                }
            });
            $.messager.progress('close');
        }
    }
    
    function hapusPemeriksaan_fisik() {
        var row = $('#dgPemeriksaan_fisik').datagrid('getSelected');
        if (row) {
            $.messager.confirm('Konfirmasi', 'Anda yakin akan menghapus data ini?', function (r) {
                if (r) {
                    $.post('{base_url}index.php/proses/pemeriksaan_fisik/hapus', {id_pemeriksaan_fisik: row.id_pemeriksaan_fisik}, function (result) {
                        if (result.success) {
                            $.messager.show({title: 'Hapus', msg: 'Data Terhapus.', showType: 'slide'});
                            $('#dgPemeriksaan_fisik').datagrid('reload');
                        } else {
                            $.messager.alert('Error', result.msg, 'error');
                        }
                    }, 'json');
                }
            });
        }
    }
</script>
