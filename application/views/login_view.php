<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title>{title}</title>
        <meta charset="UTF-8"/> 
        <meta name="robots" content="noindex,nofollow"/>

        <link rel="shortcut icon" href="{base_url}asset/image/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="{base_url}asset/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="{base_url}asset/bootstrap/css/bootstrap-theme.min.css" />
        <!--<link rel="stylesheet" type="text/css" href="{base_url}asset/jquery-easyui/themes/bootstrap/easyui.css" />-->
        <link rel="stylesheet" type="text/css" href="{base_url}asset/jquery-easyui/themes/gray/easyui.css" />
        <!--        <link rel="stylesheet" type="text/css" href="{base_url}asset/jquery-easyui/themes/icon.css" />
                <link rel="stylesheet" type="text/css" href="{base_url}asset/jquery-easyui/themes/color.css" />-->
        <link rel="stylesheet" type="text/css" href="{base_url}asset/css/global.css" />
        <script type="text/javascript" src="{base_url}asset/js/jquery.min.js"></script>
        <script type="text/javascript" src="{base_url}asset/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{base_url}asset/jquery-easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="{base_url}asset/jquery-easyui/locale/easyui-lang-id.js"></script>
        <script type="text/javascript" src="{base_url}asset/js/jquery.inputmask.bundle.min.js"></script> 

        <script type="text/javascript">
            $(document).ready(function () {
                $('#email').textbox('textbox').focus();

                pindah_focus('#email', '#password', 'textbox');

                var t = $('#password');
                t.textbox('textbox').bind('keydown', function (e) {
                    if (e.keyCode == 13) {
                        loginUser();
                    }
                });
            });

            function pindah_focus(asal, tujuan, component_tujuan) {
                var a = $(asal);
                var t = $(tujuan);
                if (component_tujuan == 'textbox') {
                    a.textbox('textbox').bind('keydown', function (e) {
                        if (e.keyCode == 13) {
                            t.textbox('textbox').focus();
                        }
                    });
                }
            }

            function loginUser() {
                if ($('#email').val().trim() === '') {
                    $.messager.alert('Email', 'email belum diisi.', 'warning');
                } else if ($('#password').val().trim() === '') {
                    $.messager.alert('Password', 'password belum diisi.', 'warning');
                } else {
                    $.ajax({
                        url: "index.php/login/masuk",
                        data: {
                            email: $('#email').val(),
                            password: $('#password').val()
                        },
                        type: 'post',
                        success: function (result) {
                            var result = eval('(' + result + ')');
                            if (result.success) {
                                window.location = 'index.php/home';
                            } else {
                                $.messager.alert('Error', result.msg, 'error');
                            }
                        }
                    });
                }
            }


        </script>

        <style>

            /*            #pan {
                            background-image: url("asset/image/bg-login.jpg");
                            background-size: 100%;
                        }*/
            .bfooter{
                background:url('{base_url}asset/image/bg_footer.jpg') no-repeat;
                height:115px;
                position: fixed;
                bottom: 0;
                width: 100%;
            }

            .bfooter2{
                background:url('{base_url}asset/image/bg_footer2.jpg');
                height:115px;
                position: fixed;
                bottom: 0;
                width: 100%;
            }

        </style>
    </head>
    <body>
        <div id="pan" class="easyui-panel" fit="true">
            <div id="win" class="easyui-dialog login" title="Login" data-options="collapsible:false,minimizable:false,maximizable:false,closable:false" 
                 style="width: 330px; padding: 10px 10px;">
                <div style="text-align: center; padding-bottom: 4px; height:100px;"><img src="{base_url}/asset/img/logo.png" height="90px"/></div>
                <form id="login_form" method="post">
                    <table width="280" border="0" align="center">
                        <tr>
                            <td><input name="email" id="email" class="easyui-textbox login-input" style="width:100%;height:40px; padding-bottom: 3px;" data-options="prompt:'Email',iconCls:'icon-man',iconWidth:38" /></td>
                        </tr>
                        <tr>
                            <td><input type="password" name="password" id="password" class="easyui-textbox login-input" style="width:100%;height:40px; padding-bottom: 3px;" data-options="prompt:'Password',iconCls:'icon-lock',iconWidth:38" /></td>
                        </tr>
                        <tr>
                            <td>
                                <a id="btnLogin" href="#" class="easyui-linkbutton" style="width:100%;height:40px;"
                                   iconcls="icon-ok" onclick="loginUser()">Login</a>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>
