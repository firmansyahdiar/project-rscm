<table id="laporan" fitcolumns="true" ></table>
    <form id="fmLaporan" class="form-horizontal fm" method="post" novalidate="novalidate">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-4">
                    <div class="fitem">
                        <label>Nama Pasien :</label> 
                        <input id="id_pasien" name="id_pasien" class="easyui-combobox" required="true" type="text" maxlength="254" style="width: 55%;" />
                    </div>
                    <div class="fitem">
                        <label>Kunjungan Ke :</label> 
                        <input id="k" name="k" class="easyui-textbox" required="true" type="text" style="width: 55%;" />
                    </div>
                    <div class="fitem">
                        <label></label> 
                        <a href="#" class="easyui-linkbutton" onclick="cetakLaporan()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Cetak</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
<script type="text/javascript" src="{base_url}asset/js/helper/pasien.js"></script>
<script type="text/javascript">
    combobox_pasien('#id_pasien', '{base_url}');
    function cetakLaporan() {
        if ($('#fmLaporan').form('validate')) {
            window.open("{base_url}index.php/laporan/laporan/cetak?p="+$('#id_pasien').combobox('getValue')+"&k="+$('#k').textbox('getValue')+"");
        }
    }
</script>
