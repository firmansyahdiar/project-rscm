<table id="import" fitcolumns="true" ></table>
    <form id="fmImport" class="form-horizontal fm" method="post" enctype="multipart/form-data">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-8">
                    <div class="fitem">
                        <label>Pilih File :</label> 
                        <div class="fileUpload btn btn-xs btn-success">
                            <span>Pilih File</span>
                            <input id="fl" name="fl" type="file" accept=".bak" class="upload" onchange="previewFile(this)" required="true"/>
                        </div>
                        <input id="fls" name="fls" class="easyui-textbox" data-options="disabled:true" style="width: 47%;" required="true"/>
                    </div>
                    <div class="fitem">
                        <label></label> 
                        <a href="#" class="easyui-linkbutton" onclick="importData()"><span class="glyphicon glyphicon-import" aria-hidden="true"></span> Import</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
<script type="text/javascript">
    function previewFile(input) {
        $('#fls').textbox('setValue', input.files[0].name);
    }
    function importData() {
        //if ($('#fmImport').form('validate')) {            
        if (document.getElementById("fl").files.length !== 0) {
            $.messager.progress({title: 'Simpan Data'});                
            var formData = new FormData($('#fmImport')[0]);
            $.ajax({
                url: "{base_url}index.php/laporan/import/upload",
                type: "POST",
                data: formData,
                async: false,
                success: function (data) {
                    var result = eval('(' + data + ')');
                    if (result.success) {
                        $.messager.show({title: 'File', msg: 'Import data berhasil', showType: 'show'});
                    } else {
                        $.messager.alert('Error', result.msg, 'error');
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
            $.messager.progress('close');
        }else{
            $.messager.alert('Error','Pilih file terlebih dahulu', 'error');
        }            
        /*}else{
            
        }*/
    }
</script>
