<table id="export" fitcolumns="true" ></table>
    <form id="fmExport" class="form-horizontal fm" method="post" novalidate="novalidate">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-8">
                    <div class="fitem">
                        <label>Periode :</label> 
                        <input id="dari" name="dari" class="easyui-textbox" required="true" type="text" style="width: 35%;" /> s/d
                        <input id="sampai" name="sampai" class="easyui-textbox" required="true" type="text" style="width: 35%;" />                        
                    </div>
                    <div class="fitem">
                        <label></label> 
                        <a href="#" class="easyui-linkbutton" onclick="exportData()"><span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
<script type="text/javascript" src="{base_url}asset/js/helper/tanggal.js"></script>
<script type="text/javascript">
    datebox_tanggal('#dari', '{base_url}');
    datebox_tanggal('#sampai', '{base_url}');
    function exportData() {
        if ($('#fmExport').form('validate')) {
            window.open("{base_url}index.php/laporan/export/exportData?d="+$('#dari').datebox('getValue')+"&s="+$('#sampai').datebox('getValue')+"");
        }
    }
</script>
