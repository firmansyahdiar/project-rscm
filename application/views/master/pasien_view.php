<div id="tbPasien">
    <a href="#" class="easyui-linkbutton" plain="true" onclick="tambahPasien()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah</a> 
    <a href="#" class="easyui-linkbutton" plain="true" onclick="ubahPasien()"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Ubah</a> 
    <a href="#" class="easyui-linkbutton" plain="true" onclick="hapusPasien()"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Hapus</a> 
    <!--<a href="#" class="easyui-linkbutton" plain="true" onclick="cetakIndividu()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Cetak</a>-->
    <div style="float: right; padding-top: 2px">
        <input id="cari" class="easyui-searchbox" style="width: 300px" data-options="searcher:doSearch,prompt:'Masukan kata pencarian'">
    </div>
</div>
<table id="dgPasien" fitcolumns="true"></table>
<div id="dlgPasien" class="easyui-dialog" data-options="modal:true, closed:true" style="width: 85%; padding: 5px; max-height: 500px" buttons="#dlgPasien-buttons">
    <div class="ftitle">Master Pasien</div>
    <form id="fmPasien" class="form-horizontal fm" method="post" novalidate="novalidate">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-6">
                    <div class="fitem">
                        <label>Nama Pasien :</label> 
                        <input id="nama_pasien" name="nama_pasien" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Nama Orang Tua :</label> 
                        <input id="namaOrangTua" name="namaOrangTua" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Usia :</label> 
                        <input id="usia" name="usia" class="easyui-numberspinner" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Pendidikan :</label> 
                        <input id="pendidikan" name="pendidikan" type="text" class="easyui-textbox"  required="true" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Tempat Lahir :</label> 
                        <input id="tempatLahir" name="tempatLahir" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Pekerjaan :</label> 
                        <input id="pekerjaan" name="pekerjaan" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Bangsa </label> 
                    </div>
                    <div class="fitem">
                        <label>Indonesia asli / suku :</label> 
                        <input id="suku" name="suku" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Indonesia Keturunan :</label> 
                        <input id="keturunan" name="keturunan" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Asing berasal dari :</label> 
                        <input id="asing" name="asing" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label style="vertical-align:top;">No Rekam Medis :</label> 
                        <input id="noRekamMedis" name="noRekamMedis" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Tanggal Lahir :</label> 
                        <input id="tglLahir" name="tglLahir" class="easyui-textbox" required="true" style="width: 30%;" />
                    </div>
                    <div class="fitem">
                        <label>Agama :</label> 
                        <input id="agama" name="agama" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Daerah :</label> 
                        <input id="daerah" name="daerah" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="ftitle">Tempat Tinggal di Jakarta</div>
                    <div class="fitem">
                        <label>Jalan :</label> 
                        <input id="jJalan" name="jJalan" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>RT :</label> 
                        <input id="jRt" name="jRt" class="easyui-textbox" required="true" type="text" style="width: 10%;" /> 
                        <label>RW :</label> 
                        <input id="jRw" name="jRw" class="easyui-textbox" required="true" type="text" style="width: 10%;" /> <br>
                        <label>No :</label> 
                        <input id="jNo" name="jNo" class="easyui-textbox" required="true" type="text" style="width: 10%;" />
                        <label>Telp :</label> 
                        <input id="jTelp" name="jTelp" class="easyui-textbox" required="true" type="text" style="width: 10%;" /> 
                    </div>
                    <div class="fitem">
                        <label>Kelurahan :</label> 
                        <input id="jKelurahan" name="jKelurahan" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Kecamatan :</label> 
                        <input id="jKecamatan" name="jKecamatan" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="ftitle">Tempat Tinggal Asal</div>
                    <div class="fitem">
                        <label>Jalan :</label> 
                        <input id="aJalan" name="aJalan" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>RT :</label> 
                        <input id="aRt" name="aRt" class="easyui-textbox" required="true" type="text" style="width: 10%;" /> 
                        <label>RW :</label> 
                        <input id="aRw" name="aRw" class="easyui-textbox" required="true" type="text" style="width: 10%;" /> <br>
                        <label>No :</label> 
                        <input id="aNo" name="aNo" class="easyui-textbox" required="true" type="text" style="width: 10%;" />
                        <label>Telp :</label> 
                        <input id="aTelp" name="aTelp" class="easyui-textbox" required="true" type="text" style="width: 10%;" /> 
                    </div>
                    <div class="fitem">
                        <label>Kelurahan :</label> 
                        <input id="aKelurahan" name="aKelurahan" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Kecamatan :</label> 
                        <input id="aKecamatan" name="aKecamatan" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Kabupaten / Kota:</label> 
                        <input id="aKabupaten" name="aKabupaten" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Provinsi :</label> 
                        <input id="aProvinsi" name="aProvinsi" class="easyui-textbox" required="true" type="text" style="width: 69%;" />
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div id="dlgPasien-buttons"> 
    <a href="#" class="easyui-linkbutton" onclick="simpanPasien()"><span class="glyphicon glyphicon-save" aria-hidden="true"></span> Simpan</a> 
    <a href="#" class="easyui-linkbutton" onclick="batalPasien()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</a>
</div>

<script type="text/javascript" src="{base_url}asset/js/helper/tanggal.js"></script>

<script type="text/javascript">
    datebox_tanggal("#tglLahir");
    var method;
    var id;
    
    /*function show(val){
        
    }*/
    
    $('#dgPasien').datagrid({
        url: '{base_url}index.php/master/pasien/get_page',
        title: 'Master Pasien',
        fit: true, border:false, striped:true, singleSelect: true, rownumbers: true, pagination: true, toolbar: '#tbPasien',
        columns: [[
                {field: 'id_pasien', title: 'id', hidden: true},
                {field: 'nama_pasien', width: 100, title: 'Nama Pasien'},
                {field: 'namaOrangTua', width: 100, title: 'Nama Orang Tua'},
                {field: 'usia', width: 100, title: 'Usia'},
                {field: 'pendidikan', width: 100, title: 'Pendidikan'},
                {field: 'tempatLahir', width: 120, title: 'Tempat Lahir'},
//                {field: 'tempatLahir', width: 120, title: 'Tempat Lahir', formatter: function(value, row, index){return value == 1 ? 'Laki-laki':'Perempuan';}},
                {field: 'tglLahir', width: 120, title: 'Tanggal Lahir', formatter: function (value, row, index) {
                            return decode_tanggal(value);
                        }},
                {field: 'pekerjaan', width: 120, title: 'Pekerjaan'},
                {field: 'suku', width: 120, title: 'Suku'},
                {field: 'keturunan', width: 120, title: 'Keturunan'},
                {field: 'asing', width: 120, title: 'Asing'}
                //{field: 'status', width: 50, title: 'Status', formatter: function (value, row, index) {return value == 1 ? 'Terminasi' : 'Aktif';}}
            ]]
    });
    
    function tambahPasien() {
        $('#fmPasien').form('clear');
        $('#dlgPasien').dialog('open').dialog('setTitle', 'Tambah');
        method='tambah';
        id='';
    }
    
    function ubahPasien() {
        var row = $('#dgPasien').datagrid('getSelected');
		console.log(row);
        if (row) {
//            $('#fmPasien').form('clear');
            $('#fmPasien').form('load', row);
            $('#dlgPasien').dialog('open').dialog('setTitle', 'Ubah');
            method = 'ubah';
            id = row.id_pasien;
        }
    }
    
    function batalPasien() {
        $('#dlgPasien').dialog('close');
    }
    
    function doSearch(value) {
        $('#dgPasien').datagrid('load', {
            cari: value
        });
    }
    
    function simpanPasien(){
         if ($('#fmPasien').form('validate')) {
            $.messager.progress({title: 'Simpan Data'});
            $.ajax({
                url: '{base_url}index.php/master/pasien/simpan',
                data: {
                    'method': method,
                    'id_pasien': id,
                    'nama_pasien':$('#nama_pasien').textbox('getValue'),
                    'namaOrangTua':$('#namaOrangTua').textbox('getValue'),
                    'usia':$('#usia').textbox('getValue'),
                    'pendidikan':$('#pendidikan').textbox('getValue'),
                    'tempatLahir':$('#tempatLahir').textbox('getValue'),
                    'tglLahir':$('#tglLahir').datebox('getValue'),
                    'pekerjaan':$('#pekerjaan').textbox('getValue'),
                    'suku':$('#suku').textbox('getValue'),
                    'keturunan':$('#keturunan').textbox('getValue'),
                    'asing':$('#asing').textbox('getValue'),
                    'noRekamMedis':$('#noRekamMedis').textbox('getValue'),
                    'agama':$('#agama').textbox('getValue'),
                    'daerah':$('#daerah').textbox('getValue'),
                    'jJalan':$('#jJalan').textbox('getValue'),
                    'jRt':$('#jRt').textbox('getValue'),
                    'jRw':$('#jRw').textbox('getValue'),
                    'jNo':$('#jNo').textbox('getValue'),
                    'jTelp':$('#jTelp').textbox('getValue'),
                    'jKelurahan':$('#jKelurahan').textbox('getValue'),
                    'jKecamatan':$('#jKecamatan').textbox('getValue'),
                    'aJalan':$('#aJalan').textbox('getValue'),
                    'aRt':$('#aRt').textbox('getValue'),
                    'aRw':$('#aRw').textbox('getValue'),
                    'aNo':$('#aNo').textbox('getValue'),
                    'aTelp':$('#aTelp').textbox('getValue'),
                    'aKelurahan':$('#aKelurahan').textbox('getValue'),
                    'aKecamatan':$('#aKecamatan').textbox('getValue'),
                    'aKabupaten':$('#aKabupaten').textbox('getValue'),
                    'aProvinsi':$('#aProvinsi').textbox('getValue'),
                    'user_id':'<?php echo $user_id;?>',
                    'modified': <?php echo date('Ymd');?>
                },
                type: 'post',
                success: function (data) {                    
                    var result = eval('(' + data + ')');
                    if (result.success) {
                        $.messager.show({title: 'Simpan', msg: 'Data Tersimpan.', showType: 'show'});
                        $('#dlgPasien').dialog('close');
                        $('#dgPasien').datagrid('reload');
                    } else {
                        $.messager.alert('Error', result.msg, 'error');
                    }
                }
            });
            $.messager.progress('close');
        }
    }
    
    function hapusPasien() {
        var row = $('#dgPasien').datagrid('getSelected');
        if (row) {
            $.messager.confirm('Konfirmasi', 'Anda yakin akan menghapus data ini?', function (r) {
                if (r) {
                    $.post('{base_url}index.php/master/pasien/hapus', {id_pasien: row.id_pasien}, function (result) {
                        if (result.success) {
                            $.messager.show({title: 'Hapus', msg: 'Data Terhapus.', showType: 'slide'});
                            $('#dgPasien').datagrid('reload');
                        } else {
                            $.messager.alert('Error', result.msg, 'error');
                        }
                    }, 'json');
                }
            });
        }
    }
</script>