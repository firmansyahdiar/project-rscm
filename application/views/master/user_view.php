<div id="tbUser">
    <a href="#" class="easyui-linkbutton" plain="true" onclick="tambahUser()"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah</a> 
    <a href="#" class="easyui-linkbutton" plain="true" onclick="ubahUser()"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Ubah</a> 
    <a href="#" class="easyui-linkbutton" plain="true" onclick="hapusUser()"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Hapus</a> 
    <!--<a href="#" class="easyui-linkbutton" plain="true" onclick="cetakIndividu()"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Cetak</a>-->
    <div style="float: right; padding-top: 2px">
        <input id="cari" class="easyui-searchbox" style="width: 300px" data-options="searcher:doSearch,prompt:'Masukan kata pencarian'">
    </div>
</div>
<table id="dgUser" fitcolumns="true"></table>
<div id="dlgUser" class="easyui-dialog" data-options="modal:true, closed:true" style="width: 30%; padding: 5px" buttons="#dlgUser-buttons">
    <div class="ftitle">Master User</div>
    <form id="fmUser" class="form-horizontal fm" method="post" novalidate="novalidate">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="fitem">
                        <label>Nama:</label> 
                        <input id="nama_user" name="nama_user" class="easyui-textbox" required="true" type="text" maxlength="9" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Email :</label> 
                        <input id="email" name="email" class="easyui-textbox" required="true" type="email" maxlength="100" style="width: 69%;" />
                    </div>
                    <div class="fitem">
                        <label>Password :</label> 
                        <input id="password" name="password" type="password" class="easyui-textbox"  required="true" style="width: 69%;" onkeyup="cpassword(this.value)" />
                    </div>
					<div class="fitem">
                        <label>Konfirmasi Password :</label> 
                        <input id="cpassword" name="cpassword" type="password" class="easyui-textbox"  required="true" style="width: 69%;" />
                    </div>
					<div class="fitem">
                        <label>Hak Akses :</label> 
                        <select id="hak_akses" name="hak_akses" class="easyui-combobox"  required="true" style="width: 69%;">
							<option value="">--Pilih Hak Akses---</option>
							<?php if($user_hak_akses == 0) echo "<option value=\"0\">Super Administrator</option>"; ?>
							<?php if($user_hak_akses == 0) echo "<option value=\"1\">Administrator</option>"; ?>
							<?php if($user_hak_akses == 1) echo "<option value=\"2\">User</option>"; ?>
						</select>
                    </div>                    
                </div>
            </div>
        </div>
    </form>
</div>
<div id="dlgUser-buttons"> 
    <a href="#" class="easyui-linkbutton" onclick="simpanUser()"><span class="glyphicon glyphicon-save" aria-hidden="true"></span> Simpan</a> 
    <a href="#" class="easyui-linkbutton" onclick="batalUser()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</a>
</div>

<script type="text/javascript">    
    var method;
    var id;
    
//        $('#hak_akses').combobox({
//            onBeforeLoad: function (param) {
//                if ('{user_hak_akses}'=='1') {
//                    return '<option value=1>Administrator</option>';
//                    return '<option value=1>Administrator</option>';
//                }
//            }
//        });
//    function cpassword(val){
//        console.log(val);
//        if(val!='')
//            $('#cpassword').textbox({required:true});
//        else
//            $('#cpassword').textbox({required:false});
//    }
    
    $('#dgUser').datagrid({
        url: '{base_url}index.php/master/user/get_page',
        title: 'Master User',
        onBeforeLoad: function (param) {
                param.by = {'m_user.parent': '<?php echo $user_id;?>'};
            },
        fit: true, border:false, striped:true, singleSelect: true, rownumbers: true, pagination: true, toolbar: '#tbUser',
        columns: [[
                {field: 'id_user', title: 'id_user', hidden: true},
                {field: 'nama_user', width: 100, title: 'Nama User'},
                {field: 'email', width: 100, title: 'Email'},
                {field: 'hak_akses', width: 100, title: 'Hak Akses',
                    formatter: function(value){
                        if(value == 0)
                            return 'Super Administrator';
                        else if(value == 1)
                            return 'Administrator';
                        else 
                            return 'User';
                    }
				},
                {field: 'parent_user', width: 120, title: 'Parent User'},
                {field: 'modified', width: 120, title: 'Terakhir Update', hidden: true}
            ]]
    });
    
    function tambahUser() {
        $('#fmUser').form('clear');
        $('#password').textbox({required:true});
        $('#cpassword').textbox({required:true});
        $('#dlgUser').dialog('open').dialog('setTitle', 'Tambah');
        method='tambah';
        id='';
//        console.log({user_hak_akses});
    }
    
    function ubahUser() {
        var row = $('#dgUser').datagrid('getSelected');
        if (row) {
            $('#fmUser').form('clear');
            $('#fmUser').form('load', {
               id_user  : row.id_user,
               nama_user: row.nama_user,
               password : '',
               email    : row.email,
               hak_akses: row.hak_akses
            });
            $('#password').textbox({required:false});
            $('#cpassword').textbox({required:false});
            $('#dlgUser').dialog('open').dialog('setTitle', 'Ubah');
            method = 'ubah';
            id = row.id_user;
        }
    }
    
    function batalUser() {
        $('#dlgUser').dialog('close');
    }
    
    function doSearch(value) {
        $('#dgUser').datagrid('load', {
            cari: value
        });
    }
    
    function simpanUser(){
//        if ('{user_hak_akses}'=='0' && $('#hak_akses').combobox('getValue')>='0') {
//                        $.messager.show({
//                            title:'Pemberitahuan',
//                            msg:'Maaf anda tidak memiliki akses untuk meng-Approve',
//                            timeout:10000,
//                            showType:'slide'
//                        });
//                    } else {
//                        
//                    }
        if ($('#fmUser').form('validate')) {
                if($('#password').textbox('getValue') == $('#cpassword').textbox('getValue')){
                        $.messager.progress({title: 'Simpan Data'});
                        $.ajax({
                                url: '{base_url}index.php/master/user/simpan',
                                data: {
                                        'method': method,
                                        'id_user': id,
                                        'nama_user':$('#nama_user').textbox('getValue'),
                                        'email':$('#email').textbox('getValue'),
                                        'password':$('#password').textbox('getValue'),
                                        'hak_akses':$('#hak_akses').combobox('getValue'),
                                        'parent':'<?php echo $user_id;?>',
                                        'modified': <?php echo date('Ymd');?>
                                },
                                type: 'post',
                                success: function (data) {                    
                                        var result = eval('(' + data + ')');
                                        if (result.success) {
                                                $.messager.show({title: 'Simpan', msg: 'Data Tersimpan.', showType: 'show'});
                                                $('#dlgUser').dialog('close');
                                                $('#dgUser').datagrid('reload');
                                        } else {
                                                $.messager.alert('Error', result.msg, 'error');
                                        }
                                }
                        });
                        $.messager.progress('close');
                }else
                        $.messager.alert('Error', 'Password Tidak Sama', 'error');
        }
    }
    
    function hapusUser() {
        var row = $('#dgUser').datagrid('getSelected');
        if (row) {
            $.messager.confirm('Konfirmasi', 'Anda yakin akan menghapus data ini?', function (r) {
                if (r) {
                    $.post('{base_url}index.php/master/user/hapus', {id_user: row.id_user}, function (result) {
                        if (result.success) {
                            $.messager.show({title: 'Hapus', msg: 'Data Terhapus.', showType: 'slide'});
                            $('#dgUser').datagrid('reload');
                        } else {
                            $.messager.alert('Error', result.msg, 'error');
                        }
                    }, 'json');
                }
            });
        }
    }
</script>
