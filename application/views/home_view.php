<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>{title}</title> 
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="robots" content="noindex,nofollow"/>
        <!--<script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
        
        <script src="{base_url}asset/js/jquery.js" type="text/javascript"></script>
        <link rel="shortcut icon" href="{base_url}asset/image/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="{base_url}asset/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="{base_url}asset/bootstrap/css/bootstrap-theme.min.css" />
        <!--<link rel="stylesheet" type="text/css" href="{base_url}asset/jquery-easyui/themes/bootstrap/easyui.css" />-->
        <link rel="stylesheet" type="text/css" href="{base_url}asset/jquery-easyui/themes/gray/easyui.css" />
        <link rel="stylesheet" type="text/css" href="{base_url}asset/jquery-easyui/themes/icon.css" />
        <link rel="stylesheet" type="text/css" href="{base_url}asset/jquery-easyui/themes/color.css" />
        <link rel="stylesheet" type="text/css" href="{base_url}asset/css/global.css" />
        <script type="text/javascript" src="{base_url}asset/js/jquery.min.js"></script>
        <script type="text/javascript" src="{base_url}asset/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{base_url}asset/jquery-easyui/jquery.easyui.min.js"></script>
        <script type="text/javascript" src="{base_url}asset/jquery-easyui/locale/easyui-lang-id.js"></script>
        <script type="text/javascript" src="{base_url}asset/js/jquery.inputmask.bundle.min.js"></script>    
        <script type="text/javascript" src="{base_url}asset/js/helper/tanggal.js"></script>    
        <script type="text/javascript" src="{base_url}asset/js/jquery-dateFormat.min.js"></script>
        
        <script src="{base_url}asset/js/highcharts.js" type="text/javascript"></script>
        <script src="{base_url}asset/js/exporting.js" type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function () {
                showcontent('Dashboard', 'dashboard');
            });

            function showcontent(title, menu) {
                if ($('#tt').tabs('exists', title)) {
                    $('#tt').tabs('select', title);
                } else {
                    $('#tt').tabs('add', {
                        title: title,
                        href: '{base_url}index.php/home/menu/' + menu,
                        closable: true
                    });
                }
            }
            
            function loginUser() {
                if ($('#email').val().trim() === '') {
                    $.messager.alert('Email', 'email belum diisi.', 'warning');
                } else if ($('#password').val().trim() === '') {
                    $.messager.alert('Password', 'password belum diisi.', 'warning');
                } else {
                    $.ajax({
                        url: "index.php/login/masuk",
                        data: {
                            email: $('#email').val(),
                            password: $('#password').val()
                        },
                        type: 'post',
                        success: function (result) {
                            var result = eval('(' + result + ')');
                            if (result.success) {
                                window.location = 'index.php/home';
                            } else {
                                $.messager.alert('Error', result.msg, 'error');
                            }
                        }
                    });
                }
            }
        </script>
    </head>

    <body class="easyui-layout">
        <div data-options="region:'north'" style="height: 50px">
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-menu">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"><img src="{base_url}/asset/img/logo.png" height="30px" alt="Logo"></img></a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-menu">
                        <ul class="nav navbar-nav">
							<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Proses <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
									<li><a href="#" onclick="showcontent('Anamnesis', 'anamnesis')">Anamnesis</a></li>
									<!--<li><a href="#" onclick="showcontent('Pemeriksaan', 'pemeriksaan')">Pemeriksaan</a></li>-->
									<li><a href="#" onclick="showcontent('Pemeriksaan Fisik', 'pemeriksaanfisik')">Pemeriksaan Fisik </a></li>
								</ul>
							</li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data Master <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" onclick="showcontent('Master Pasien', 'pasien')">Master Pasien</a></li>
                                    <?php if($user_hak_akses != 2) echo "<li><a href=\"#\" onclick=\"showcontent('Master User', 'user')\">Master User</a></li>"; ?>
                                </ul>
                            </li>
							<li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Laporan <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" onclick="showcontent('Laporan Pemeriksaan', 'laporan')">Laporan Pemeriksaan</a></li>                                    
                                    <li><a href="#" onclick="showcontent('Export Data', 'export')">Export Data</a></li>
                                    <li><a href="#" onclick="showcontent('Import Data', 'import')">Import Data</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="glyphicon glyphicon-user"></i> {user_nama} <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#" onclick="menuButton('profil')">Profil</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{base_url}index.php/login/keluar"><i class="glyphicon glyphicon-off"></i> Keluar</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div data-options="region:'center'">
            <div id="tt" class="easyui-tabs" fit="true" border="false" style=""></div>
        </div>
    </body>
</html>
