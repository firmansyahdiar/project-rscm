<h4>Dashboard</h4>
<style>
  #GrafikTahunan,#GrafikPeruser
  {
  	height:41%;
  	padding-right:10px;
  	margin:auto auto;
  	float:left;
  	width:75%;
  } 
  #FilterKanan
  {
  	height:90%;
  	padding:10px;
  	width:24%;
  	float:right;
  	background-color:#FFF;  
  }
  #DataApprove
  {
  	height:65%;
/*  	padding:10px;
  	width:29%;
  	float:right;
  	background-color:#FFF;  */
  }
   
</style>
    <div id="FilterKanan">
        <div class="fitem">
            <label>Approve :</label>
            <select id="data" name="data" class="easyui-combobox" data-options="editable: false" style="width: 69%;">
                <!--<option value="Langsung">Langsung</option>-->
                <option value="Anamnesis">Anamnesis</option>
                <option value="Pemeriksaan">Pemeriksaan </option>
                <option value="PemeriksaanFisik">Pemeriksaan Fisik </option>
            </select>
        </div>
        <div class="fitem">
            <label>User :</label> 
            <select id="user" name="user" class="easyui-combobox" data-options="editable: false" style="width: 69%;">
            </select>
        </div>
        <div class="fitem">
            <label>Kunjungan :</label> 
            <select id="kunjungan" name="kunjungan" class="easyui-numberspinner" data-options="editable: false" style="width: 69%;"></select>
        </div>
<!--        <div class="fitem">
            <label>Tanggal Periksa :</label> 
            <select id="tgl_periksa" name="tgl_periksa" class="easyui-textbox" data-options="editable: false" style="width: 69%;"></select>
        </div>-->
        <div class="fitem" style="float:right;">
            <!--<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="FilterPmPending()"> Tampilkan Data Pending </a>-->
            <a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="FilterData()"> Tampilkan Data </a>
        </div>
        <div style="clear:both; height:5px;"></div>
        <div id="DataApprove" title="Data Detail ">
            <table id="dgDataApprove" fitcolumns="true"></table><br>
        </div>
        <div class="fitem" style="float:right;">
                <!--<a href="#" class="easyui-linkbutton" iconCls="icon-back" plain="true" onclick="PendingPMLangsung()"> Pending </a>-->
                <a href="#" class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="RejectPMLangsung()"> Reject </a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="Approve()"> Approve </a>
        </div>
    </div>
    <div id="GrafikTahunan"></div> 
    <div id="GrafikPeruser"></div> 
<!--<script src="<?echo base_url();?>asset/highcharts/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{base_url}asset/highcharts/highcharts.js"></script>-->
<script type="text/javascript" src="{base_url}asset/highcharts/modules/exporting.js"></script>
<script type="text/javascript" src="{base_url}asset/highcharts/themes/sand.js"></script>
<script type="text/javascript" src="{base_url}asset/highcharts/highcharts.js"></script>
<script type="text/javascript" src="{base_url}asset/js/helper/tanggal.js"></script>
<script type="text/javascript" src="{base_url}asset/js/helper/user.js"></script>
<script type="text/javascript">
    combobox_user('#user','{base_url}');


$('#user').combobox({
    onBeforeLoad: function (param) {
//                console.log({user_hak_akses})
        if ('{user_hak_akses}'=='0') {
            param.by = {'m_user.parent': '{user_id}'};
        } 
        if ('{user_hak_akses}'=='1') {
            param.by = {'m_user.parent': '{user_id}'};
        }
        if ('{user_hak_akses}'=='2') {
            param.by = {'m_user.id_user': '{user_id}'};
        }
    }
});
        
$('document').ready(function(){
    showGrafikTahunan();
    showGrafikPeruser();
//		$.ajax({ 
});
//--------------------------- DATA ANAMNESIS -------------------------------------
//
function FilterData() {
    if ($('#data').combobox('getValue')==='Anamnesis'){
        if ('{user_hak_akses}'!=='0' && '{user_hak_akses}'!=='1') {
            $.messager.show({
                title:'Pemberitahuan',
                msg:'Maaf anda tidak memiliki akses untuk meng-Approve',
                timeout:10000,
                showType:'slide'
            });
        } else {
            $('#dgDataApprove').datagrid({
                url: '{base_url}index.php/home/Data/' + $('#data').combobox('getValue'),
                method: 'get',
                    queryParams : {
                            user:$('#user').combobox('getValue'),
                            kunjungan:$('#kunjungan').val(),
                        },
                fit: true, border:false, checkOnSelect:true, idField:'id', striped:true, singleSelect: false, rownumbers: true, pagination: false, //toolbar: '#tbLappmlIndividutdkLangsung',
                columns: [[
                            {field: 'ck', checkbox:true},
                            {field: 'id', width: 500, title: 'ID', hidden: true},
                            {field: 'nama', width: 500, title: 'Nama'},
//                                        {field: 'jejaring', width: 500, title: 'Jejaring'},
//                                        {field: 'status', width: 500, title: 'status'}
                         ]]
            });  
//                        var col=$.parseJSON(data);
//                        console.log(col);

            $.messager.show({
                title:'Pemberitahuan',
                msg:'Filter data berhasil ',
                timeout:5000,
                showType:'slide'
            });
        }
    }else{
//        if ('{user_hak_akses}'!=='APPROVE' && '{user_hak_akses}'!=='ADMINISTRATOR') {
//            $.messager.show({
//                title:'Pemberitahuan',
//                msg:'Maaf anda tidak memiliki akses untuk meng-Approve',
//                timeout:10000,
//                showType:'slide'
//            });
//        } else {
//            $('#dgLappmlangsungIndividuDetail').datagrid({
//                url: '{base_url}index.php/home/ApprovePmTdkLangsung/' + $('#kelompok_pmlangsung').combobox('getValue'),
//                method: 'get',
//                    queryParams : {
//                            jejaringPMLangsung:$('#jejaring_pmlangsung').combobox('getValue'),
//                            programPMLangsung:$('#program_pmlangsung').combobox('getValue'),
//                            angkatanPMLangsung:$('#angkatan_pmlangsung').combobox('getValue')
//                        },
//                fit: true, border:false, checkOnSelect:true, idField:'id', striped:true, singleSelect: false, rownumbers: true, pagination: false, //toolbar: '#tbLappmlIndividutdkLangsung',
//                columns: [[
//                            {field: 'ck', checkbox:true},
//                            {field: 'id', width: 500, title: 'ID', hidden: true},
//                            {field: 'nama', width: 500, title: 'Nama'},
////                                        {field: 'jejaring', width: 500, title: 'Jejaring'},
////                                        {field: 'status', width: 500, title: 'status'}
//                         ]]
//            });  
////                        var col=$.parseJSON(data);
////                        console.log(col);
//
//            $.messager.show({
//                title:'Pemberitahuan',
//                msg:'Data ' + $('#kelompok_pmlangsung').combobox('getValue') + ', Jejaring ' + $('#jejaring_pmlangsung').combobox('getText'),
//                timeout:5000,
//                showType:'slide'
//            });
//        }                    
    }
}
$('document').ready(function(){
		$.ajax({
			url:'home/dataGraph/L',
			async:true,
			dataType: 'json',
			success:function (res){
				//console.log(res)
				showGrafikTahunan(res);
			}	
		});

//		$.ajax({
//			url:'home/dataGraph/TL',
//			async:true,
//			dataType: 'json',
//			success:function (res){
//				//console.log(res)
//				showGraphTL(res);
//			}	
//		});
	
});

    function tampil(){      
    }

function showGrafikTahunan(){	
    $('#GrafikTahunan').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'GRAFIK DATA TAHUN <?php echo date('Y');?>'
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agst', 'Sept', 'Okt', 'Nov', 'Des']
        },
        yAxis: {
            title: {
                text: 'Jumlah Penerima Manfaat'
            }
        },
        series: ['01','02','03']
    });
}

function showGrafikPeruser(data){	
    $('#GrafikPeruser').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'GRAFIK DATA PER USER TAHUN <?php echo date('Y');?>'
        },        
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agst', 'Sept', 'Okt', 'Nov', 'Des']
        },
        yAxis: {
            title: {
                text: 'Jumlah Penerima Manfaat '
            }
        },
        series: data
        
    });

}

function Approve(){
    var ids = [];
    var rows = $('#dgDataApprove').datagrid('getSelections');
    for(var i=0; i<rows.length; i++){
        ids.push(rows[i].id);
        if ($('#data').combobox('getValue')==='Anamnesis'){
            $.ajax({
                url: '{base_url}index.php/home/updateApprove/p_anamnesis/id_anamnesis/' + ids[i],
            });
        }
        if ($('#data').combobox('getValue')==='Pemeriksaan'){
            $.ajax({
                url: '{base_url}index.php/home/updateApprove/p_pemeriksaan/id_pemeriksaan/' + ids[i],
            });
        }
        if ($('#data').combobox('getValue')==='Pemeriksaan Fisik'){
            $.ajax({
                url: '{base_url}index.php/home/updateApprove/p_pemeriksaan_fisik/id_pemeriksaan_fisik/' + ids[i],
            });
        }
        if (i>0){
            $('#dgDataApprove').datagrid('reload');
            $.messager.show({
                title:'Pemberitahuan',
                msg: i + ' data berhasil di approve',
                timeout:5000,
                showType:'slide'
            });
        }
            $('#dgDataApprove').datagrid('reload');
    }
}
</script>