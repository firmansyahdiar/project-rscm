<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    var $model;
    var $model_name;
    var $pkField;
    var $uniqueFields = array();
    var $fields = array();
    var $data = array();
    var $detail_model;
    var $detail_pkField;
    var $detail_fkField;
    var $detail_uniqueFields = array();
    var $detail_fields = array();
    var $detail_data = array();
    var $q;
    var $cari;
    var $by;
    var $page;
    var $rows;
    var $total;
    var $convert = array();
    var $convertDetail = array();
    var $removeDetail = array();
    var $removeAdd = array();
    var $removeUpdate = array();
    var $removeDetailAdd = array();
    var $removeDetailUpdate = array();
   // var $tambah = true;
   // var $ubah = true;
   // var $hapus = true;
    var $beforeSave = array();
    var $afterSave = array();

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged_in')) {
            redirect('');
        }
        $this->q = "q";
        $this->cari = "cari";
        $this->by = "by";
        $this->page = "page";
        $this->rows = "rows";
        $this->total = "total";

        //$this->tambah = $this->session->userdata('tambah');
        //$this->ubah = $this->session->userdata('ubah');
        //$this->hapus = $this->session->userdata('hapus');
    }

//    public function simpan() {
//        
//    }

    public function simpan() {
        $method = $this->input->post('method');
        $isValid = array();
        $validasi = TRUE;
        $pesan = "";
        $data = array();
        $dataBaru = array();

        unset($this->data);
        $this->data = array();
        unset($this->detail_data);
        $this->detail_data = array();

        if (!empty($this->fields)) {
            foreach ($this->fields as $key => $value) {
                $input = $this->input->post($key);
                $isValid = $this->validasi($input, $value);
                $pesan .= $isValid["pesan"];
                if ($isValid["valid"] == FALSE) {
                    $validasi = FALSE;
                } else {
                    $this->data[$key] = $input;
                }
            }
        }
        
        if (!empty($this->detail_fields)) {
            $rows = $this->input->post("data_detail");

            if ($rows) {
                foreach ($rows as $row) {
                    foreach ($this->detail_fields as $key => $value) {
                        $input = $row[$key];
                        $isValid = $this->validasi($input, $value);
                        $pesan .= $isValid["pesan"];
                        if ($isValid["valid"] == FALSE) {
                            $validasi = FALSE;
                        }
                    }
                    if ($validasi) {
                        $this->convertDataDetail();
                        $dataDetail = $this->convertDetail;
                        $dataBaruDetail = array_replace($row, $dataDetail);
                        array_push($this->detail_data, $dataBaruDetail);
                    }
                }
            }
        }
        
        if($validasi) {
            $this->beforeSave();
            if($this->beforeSave["success"] == FALSE) {
                $pesan .= $this->beforeSave["msg"];
                $validasi = FALSE;
            }
        }
            
        if ($validasi) {
            $this->convertData();
            $data = $this->convert;
            if ($method == "tambah") {
                if ($this->fields) {
                    $id = $this->uuid->v4();
                    $this->data[$this->pkField] = $id;
                    $dataBaru = array_replace($this->data, $data);
                    $this->removeFieldAdd();
                    $this->add($dataBaru);
                }
                if ($this->detail_fields) {
                    $this->addDetail($id, $this->detail_data);
                }
            } elseif ($method == "ubah") {
                if ($this->fields) {
                    $dataBaru = array_replace($this->data, $data);
                    if(isset($dataBaru['password'])){
                    	if($dataBaru['password']=='' || $dataBaru['password']==NULL)
                    		unset($dataBaru['password']);
                    }
                    $this->update($this->input->post($this->pkField), $dataBaru);
                }
                if ($this->detail_fields) {
                    $by = array();
                    $by[$this->detail_fkField] = $this->input->post($this->pkField);
                    $this->detail_model->delete_by($by);
                    $this->addDetail($this->input->post($this->detail_fkField), $this->detail_data);
                }
            } else {
                echo json_encode(array('success' => false, "msg" => "Maaf, Anda tidak berhak."));
            }
        } else {
            echo json_encode(array("msg" => $pesan));
        }
    }
    
    public function beforeSave() {
        return $this->beforeSave = array("success" => TRUE);
    }


    public function save($model, $method, $inputs, $fields, $pkField, $pkFieldValue, $uniqueFields = array(), $remove = array(), $convert = array()) {
        $isValid = array();
        $validasi = TRUE;
        $pesan = "";
        $data = array();
        $dataBaru = array();

        try {
            if ($fields) {
                foreach ($fields as $field => $type) {
                    $input = $inputs[$field];
                    $isValid = $this->validasi($input, $type);
                    $pesan .= $isValid["pesan"];
                    if ($isValid["valid"] == FALSE) {
                        $validasi = FALSE;
                    } else {
                        $data[$field] = $input;
                    }
                }
                if ($method == "tambah") {
                    $unique = array();
                    if ($uniqueFields) {
                        foreach ($uniqueFields as $uniqueField) {
                            $unique[$uniqueField] = $data[$uniqueField];
                        }
                    }
                    $isUnique = $this->is_unique($unique);
                    $validasi = $isUnique["unique"];
                    $pesan .= $isUnique["pesan"];
                }

                if ($validasi) {
                    foreach ($remove as $field) {
                        unset($data[$field]);
                    }
                    $dataBaru = array_replace($data, $convert);
                    $this->load->model($model);
                    if ($method == "tambah") {
                        try {
                            //$id = $this->uuid->v4();
                            //$dataBaru[$pkField] = $id;
                            if ($this->$model->add($dataBaru)) {
                                return array('success' => true);
                            } else {
                                return array('success' => FALSE, 'msg' => 'Proses simpan gagal.');
                            }
                        } catch (Exception $exc) {
                            var_dump($exc->getMessage());
                        }
                    } elseif ($method == "ubah") {
                        try {
                            if ($this->$model->update($pkFieldValue, $dataBaru)) {
                                return array('success' => true, 'id' => $pkFieldValue);
                            } else {
                                return array('success' => FALSE, 'msg' => 'Proses update gagal.');
                            }
                        } catch (Exception $exc) {
                            var_dump($exc->getTraceAsString());
                        }
                    } else {
                        echo json_encode(array('success' => false, "msg" => "Maaf, Anda tidak berhak."));
                    }
                } else {
                    return array('success' => FALSE, "msg" => $pesan);
                }
            }
        } catch (Exception $exc) {
            var_dump($exc->getMessage());
        }
    }

    public function saveDetail($model, $method, $rows, $detail_fields, $detail_pkField, $detail_fkField, $detail_fkFieldValue, $detail_uniqueFields, $removeDetail = array(), $convertDetail = array()) {
        $isValid = array();
        $validasi = TRUE;
        $pesan = "";
        $detailData = array();
        $dataBaruDetail = array();

        if ($detail_fields) {
            if ($rows) {
                foreach ($rows as $row) {
                    foreach ($detail_fields as $field => $type) {
                        $input = $row[$field];
                        $isValid = $this->validasi($input, $type);
                        $pesan .= $isValid["pesan"];
                        if ($isValid["valid"] == FALSE) {
                            $validasi = FALSE;
                        }
                    }
                    if ($detail_uniqueFields) {
                        $unique = array();
                        foreach ($detail_uniqueFields as $uniqueField) {
                            $unique[$uniqueField] = $row[$uniqueField];
                        }
                        $isUnique = $this->is_unique($unique);
                        $validasi = $isUnique["unique"];
                        $pesan .= $isUnique["pesan"];
                    }
                    if ($validasi) {
                        foreach ($removeDetail as $field) {
                            unset($row[$field]);
                        }
                        $dataBaruDetail = array_replace($row, $convertDetail);
                        array_push($detailData, $dataBaruDetail);
                    }
                }
            }
            if ($validasi) {
                /*if ($method == "tambah" && !$this->tambah) {
                    echo json_encode(array('success' => false, "msg" => "Maaf, Anda tidak berhak menambah data."));
                } elseif ($method == "ubah" && !$this->ubah) {
                    echo json_encode(array('success' => false, "msg" => "Maaf, Anda tidak berhak merubah data."));
                } else {*/
                    $this->load->model($model);
                    $by = array();
                    $by[$detail_fkField] = $detail_fkFieldValue;
                    $this->$model->delete_by($by);
                    if ($detailData) {
                        foreach ($detailData as $row) {
                            $row[$detail_pkField] = $this->uuid->v4();
                            $row[$detail_fkField] = $detail_fkFieldValue;
                            $this->$model->add($row);
                        }
                    }
                    return array('success' => true);
                //}
            } else {
                return array('success' => FALSE, 'msg' => $pesan);
            }
        }
    }

    private function addDetail($id, $items) {
        if ($items) {
            foreach ($items as $item) {
                foreach ($this->removeDetail as $value) {
                    unset($item[$value]);
                }
                $item[$this->detail_pkField] = $this->uuid->v4();
                $item[$this->detail_fkField] = $id;
                $this->detail_model->add($item);
            }
        }
    }

    public function convertData() {
        unset($this->convert);
        $this->convert = array();
//        $this->convertDetail = array(
//            "field" => namaFunction($this->data["field"])
//        );
    }

    public function removeFieldAdd() {
//        unset($this->data["field"]);
    }

    public function removeFieldUpdate() {
//        unset($this->data["field"]);
    }

    public function convertDataDetail() {
        unset($this->convertDetail);
        $this->convertDetail = array();
//        $this->convertDetail = array(
//            "field" => namaFunction($this->data["field"])
//        );
    }

    private function add($data) {
        $cek["isValid"] = TRUE;
        $unique = array();
        if (!empty($this->uniqueFields)) {
            foreach ($this->uniqueFields as $uniqueField) {
                $unique[$uniqueField] = $this->input->post($uniqueField);
            }
        }
        $isUnique = $this->is_unique($unique);

        $cek["isValid"] = $isUnique["unique"];
        $cek["msg"] = $isUnique["pesan"];

        if ($cek["isValid"]) {
            try {
                if ($this->model->add($data)) {
                    echo json_encode(array('success' => true));
                } else {
                    echo json_encode(array('msg' => 'Proses simpan gagal.'));
                }
            } catch (Exception $ex) {
                echo json_encode(array('msg' => $ex));
            }
        } else {
            echo json_encode(array('msg' => $cek["msg"]));
        }
    }

    private function update($id, $data) {
        if ($this->model->update($id, $data)) {
            echo json_encode(array('success' => true));
        } else {
            echo json_encode(array('msg' => 'Proses simpan gagal.'));
        }
    }

    public function get() {
        $cari = "";
        if ($this->input->post($this->cari))  {
            $cari = $this->input->post($this->cari);
        }
        if ($this->input->post($this->q))  {
            $cari = $this->input->post($this->q);
        }
        try {
            if ($this->input->post($this->by)) {
                $result = $this->model->get($cari, $this->input->post($this->by));
            } else {
                $result = $this->model->get($cari);
            }
            echo json_encode($result);
        } catch (Exception $ex) {
            log_message('error', $ex);
        }
    }

    public function gets($model, $cari = "", $by = array()) {
        try {
            if ($by) {
                $result = $this->$model->get(NULL, $by);
            } else {
                $result = $this->$model->get($cari);
            }
            return json_encode($result);
        } catch (Exception $ex) {
            log_message('error', $ex);
        }
    }

    public function get_detail() {
        $cari = "";
        if ($this->input->post($this->cari))  {
            $cari = $this->input->post($this->cari);
        }
        if ($this->input->post($this->q))  {
            $cari = $this->input->post($this->q);
        }
        try {
            if ($this->input->post($this->pkField)) {
                $by = array();
                $by[$this->detail_model->table . "." . $this->pkField] = $this->input->post($this->pkField);
                $result = $this->detail_model->get_by($by);
            } else {
                $result = $this->detail_model->get($cari);
            }
            echo json_encode($result);
        } catch (Exception $ex) {
            log_message('error', $ex);
        }
    }

    public function get_page() {
        $page = $this->input->post($this->page) ? intval($this->input->post($this->page)) : 1;
        $rows = $this->input->post($this->rows) ? intval($this->input->post($this->rows)) : 20;
        $offset = ($page - 1) * $rows;
        $cari = "";
        if ($this->input->post($this->cari))  {
            $cari = $this->input->post($this->cari);
        }
        if ($this->input->post($this->q))  {
            $cari = $this->input->post($this->q);
        }
        try {
            $result = array();
            $result[$this->total] = $this->model->total($cari);

            if ($this->input->post($this->by)) {
                $result[$this->rows] = $this->model->get($cari, $this->input->post($this->by), $rows, $offset);
            } else {
                $result[$this->rows] = $this->model->get($cari, array(), $rows, $offset);
            }
            echo json_encode($result);
        } catch (Exception $ex) {
            log_message('error', $ex);
        }
    }

    private function is_unique($fields) {
        $cek = array();
        $unique = TRUE;
        $pesan = "";
        if (!empty($fields)) {
            foreach ($fields as $key => $value) {
                if ($this->model->cek($key, $value)) {
                    $pesan .= $value . ' sudah ada.<br/>';
                    $unique = FALSE;
                }
            }
        }
        $cek["unique"] = $unique;
        $cek["pesan"] = $pesan;
        return $cek;
    }

    public function hapus() {
        try {
            //if($this->hapus) {
                if ($this->model->delete($this->input->post($this->pkField))) {
                    $this->hapusDetail($this->input->post($this->pkField));
                    echo json_encode(array('success' => true));
                } else {
                    echo json_encode(array('msg' => 'Proses penghapusan gagal.'));
                }
            /*} else {
                echo json_encode(array('msg' => 'Anda tidak berhak.'));
            }*/
        } catch (Exception $ex) {
            log_message('error', $ex);
        }
    }

    public function hapusDetail($id) {
        try {
            //if($this->hapus) {
                if (!empty($this->detail_fields)) {
                    $this->detail_model->delete($id);
                }
            //}
        } catch (Exception $ex) {
            log_message('error', $ex);
        }
    }

    private function validasi($input, $val) {
        $isValid = array();
        $valid = TRUE;
        $pesan = "";

        if ($input) {
            if (isset($val)) {
                $TIPE = $val["TIPE"];
                switch ($TIPE) {
                    case "EMAIL":
                        $email = filter_var($input, FILTER_SANITIZE_EMAIL);
                        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                            $valid = FALSE;
                            $pesan .= $val["LABEL"] . " : " . $email . " tidak valid.<br/>";
                        }
                        break;
                    case "STRING":
                        if (!filter_var($input, FILTER_DEFAULT)) {
                            $valid = FALSE;
                            $pesan .= $val["LABEL"] . " : " . $input . ' tidak valid.<br/>';
                        }
                        break;
                    case "INT":
                        if (!filter_var($input, FILTER_VALIDATE_INT)) {
                            $valid = FALSE;
                            $pesan .= $val["LABEL"] . " : " . $input . ' tidak valid.<br/>';
                        }
                        break;
                    case "FLOAT":
                        if (!filter_var($input, FILTER_VALIDATE_FLOAT)) {
                            $valid = FALSE;
                            $pesan .= $val["LABEL"] . " : " . $input . ' tidak valid.<br/>';
                        }
                        break;
                    case "BOOLEAN":
                        if (!filter_var($input, FILTER_VALIDATE_BOOLEAN)) {
                            $valid = FALSE;
                            $pesan .= $val["LABEL"] . " : " . $input . ' tidak valid.<br/>';
                        }
                        break;
                    case "URL":
                        if (!filter_var($input, FILTER_VALIDATE_URL)) {
                            $valid = FALSE;
                            $pesan .= $val["LABEL"] . " : " . $input . ' tidak valid.<br/>';
                        }
                        break;
                    case "DATE":
                        // DD/MM/YYYY Format
                        if (preg_match("/^(\d{2})\/(\d{2})\/(\d{4})$/", $input, $matches)) {
                            if (!checkdate($matches[2], $matches[1], $matches[3])) {
                                $valid = FALSE;
                                $pesan .= $val["LABEL"] . " : " . $input . ' tidak valid.<br/>';
                            }
                        }
                        break;
                    case "PASSWORD":
                        if (!filter_var($input, FILTER_UNSAFE_RAW)) {
                            $valid = FALSE;
                            $pesan .= $val["LABEL"] . " : " . $input . ' tidak valid.<br/>';
                        }
                        break;
                    case "RAW":
                        if (!filter_var($input, FILTER_UNSAFE_RAW)) {
                            $valid = FALSE;
                            $pesan .= $val["LABEL"] . " : " . $input . ' tidak valid.<br/>';
                        }
                        break;
                    case "DATA":
                        $model = $val["MODEL"];
                        $field = $val["FIELD"];
                        $this->load->model($model);
                        if (!$this->$model->cek($field, $input)) {
                            $valid = FALSE;
                            $pesan .= $val["LABEL"] . " : " . $input . ' tidak valid.<br/>';
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        $isValid["valid"] = $valid;
        $isValid["pesan"] = $pesan;
        return $isValid;
    }

    public function unsetAll() {
        $this->model;
        $this->model_name = "";
        $this->pkField = "";
        $this->uniqueFields = array();
        $this->fields = array();
        $this->data = array();
        $this->detail_model;
        $this->detail_pkField = "";
        $this->detail_fkField = "";
        $this->detail_uniqueFields = array();
        $this->detail_fields = array();
        $this->detail_data = array();
        $this->convert = array();
        $this->convertDetail = array();
        $this->removeDetail = array();
        $this->removeAdd = array();
        $this->removeUpdate = array();
        $this->removeDetailAdd = array();
        $this->removeDetailUpdate = array();
    }

}
