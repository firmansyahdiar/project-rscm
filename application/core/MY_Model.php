<?php

class MY_Model extends CI_Model {

    public $table;
    public $primaryKey;
    public $defaultField;
    public $orderBy = array();
    public $fields = array();
        public $joins = array();
    public $relations = array();

    function __construct() {
        parent::__construct();
    }

    public function get($find = NULL, $condition = array(), $limit = NULL, $offset = NULL) {
        $select = $this->primaryKey;
        $this->defaultField != "" ? $select .= ", " . $this->defaultField : $select .= ""; 
        if(!empty($this->fields)) {
            foreach ($this->fields as $field) {
                $select .= ", ".$field; 
            }
        }
        $this->db->select($select);
        if (!empty($this->relations)) {
            foreach ($this->relations as $key => $value) {
                $this->db->join($key, $value, 'left');
            }
        }
        $this->db->where($this->primaryKey . " is not null");
        if (!empty($condition)) {
            foreach ($condition as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        if ($find) {
            if($this->defaultField) {
                $this->db->like($this->defaultField, $find);
            }
            if(!empty($this->fields)) {
                foreach ($this->fields as $field) {
                    if (stristr(trim($field), ' ') === FALSE) {
                        $this->db->or_like($field, $find);
                    } else {
                        $this->db->or_like(stristr($field, ' ', TRUE), $find);
                    }
                }
            }
        }
        if (!empty($this->orderBy)) {
            foreach ($this->orderBy as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if ($limit) {
            $query = $this->db->get($this->table, $limit, $offset);
        } else {
            $query = $this->db->get($this->table);
        }
        return $query->result();
    }

    public function get_by($by = NULL, $limit = NULL, $offset = NULL) {
        $select = $this->primaryKey;
        $this->defaultField != "" ? $select .= ", ".$this->defaultField : $select .= ""; 
        if(!empty($this->fields)) {
            foreach ($this->fields as $field) {
                $select .= ", ".$field; 
            }
        }
        $this->db->select($select);
        if (!empty($this->relations)) {
            foreach ($this->relations as $key => $value) {
                $this->db->join($key, $value, 'left');
            }
        }
        if (!empty($this->orderBy)) {
            foreach ($this->orderBy as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if ($limit) {
            $query = $this->db->get_where($this->table, $by, $limit, $offset);
        } else {
            $query = $this->db->get_where($this->table, $by);
        }
        return $query->result();
    }
    
    public function total($find = NULL, $condition = array()) {
        $select = "";
        $this->defaultField != "" ? $select .= $this->defaultField : $select .= ""; 
        if(!empty($this->fields)) {
            foreach ($this->fields as $field) {
                $select .= ", ".$field; 
            }
        }
        $this->db->select($select);
        if (!empty($this->relations)) {
            foreach ($this->relations as $key => $value) {
                $this->db->join($key, $value, 'left');
            }
        }
        if (!empty($condition)) {
            foreach ($condition as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        if ($find) {
            if($this->defaultField) {
                $this->db->like($this->defaultField, $find);
            }
            if(!empty($this->fields)) {
                foreach ($this->fields as $field) {
                    if (stristr(trim($field), ' ') === FALSE) {
                        $this->db->or_like($field, $find);
                    } else {
                        $this->db->or_like(stristr($field, ' ', TRUE), $find);
                    }
                }
            }
        }
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function add($data) {
        $this->db->insert($this->table, $data);
        return ($this->db->affected_rows() > 0);
    }

    public function update($id, $data) {
        try {
            $this->db->where($this->primaryKey, $id);
            $this->db->update($this->table, $data);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function delete($id) {
        $this->db->where($this->primaryKey, $id);
        $this->db->delete($this->table);
        return ($this->db->affected_rows() > 0);
    }
    
    public function delete_by($condition=array()) {
        $this->db->where($condition);
        $this->db->delete($this->table);
        return ($this->db->affected_rows() > 0);
    }

    public function cek($field, $value) {
        $this->db->where($field, $value);
        $query = $this->db->get($this->table);
        return ($query->num_rows() > 0) ? TRUE : FALSE;
    }

}
