<?php

class M_user_model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->table = "m_user";
        $this->primaryKey = "m_user.id_user";
        $this->defaultField = "m_user.email";
        $this->fields = array(
            "m_user.nama_user",
            "m_user.password",
            "m_user.email",
            "m_user.hak_akses",
            "m_user.parent",
            "m_user.modified",
            "user_parent.nama_user AS parent_user"
            );
        $this->orderBy = array("m_user.modified" => "DESC","m_user.hak_akses" => "ASC");
        $this->relations = array("m_user AS user_parent" => "user_parent.id_user = m_user.parent");
        
    }

}
