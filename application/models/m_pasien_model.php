<?php

class M_pasien_model extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->table = "m_pasien";
        $this->primaryKey = "m_pasien.id_pasien";
        $this->defaultField = "m_pasien.nama_pasien";
        $this->fields = array(
            "m_pasien.nama_pasien",
            "m_pasien.namaOrangTua",
            "m_pasien.usia",
            "m_pasien.pendidikan",
            "m_pasien.tempatLahir",
            "m_pasien.tglLahir",
            "m_pasien.pekerjaan",
            "m_pasien.suku",
            "m_pasien.keturunan",
            "m_pasien.asing",
            "m_pasien.noRekamMedis",
            "m_pasien.agama",
            "m_pasien.daerah",
            "m_pasien.jJalan",
            "m_pasien.jRt",
            "m_pasien.jRw",
            "m_pasien.jNo",
            "m_pasien.jTelp",
            "m_pasien.jKecamatan",
            "m_pasien.jKelurahan",
            "m_pasien.aJalan",
            "m_pasien.aRt",
            "m_pasien.aRw",
            "m_pasien.aNo",
            "m_pasien.aTelp",
            "m_pasien.aKecamatan",
            "m_pasien.aKelurahan",
            "m_pasien.aKabupaten",
            "m_pasien.aProvinsi",
            "m_pasien.user_id",
            "m_pasien.modified"
            );
        $this->orderBy = array("m_pasien.nama_pasien" => "ASC");
        
    }

}