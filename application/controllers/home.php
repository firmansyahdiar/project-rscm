<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    var $base_url;

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('logged_in')) {
            redirect('');
        }
    }

    public function index() {
        $data = array(
            'title' => 'RSCM',
            'perusahaan' => 'RSCM',
            'slogan' => '',
            'base_url' => base_url(),
            'keluar' => base_url() . 'index.php/login/keluar',
            'user_id' => $this->session->userdata('sess_user_id'),
            'user_nama' => $this->session->userdata('sess_nama'),
            'user_email' => $this->session->userdata('sess_email'),            
            'user_hak_akses' => $this->session->userdata('sess_hak_akses'),
            'user_parent' => $this->session->userdata('sess_parent')
            //'default_content' => base_url() . 'index.php/home/menu/dashboard'
        );
        $this->parser->parse('home_view', $data);
    }

    public function menu($menuList) {
        $view = "";
        if ($menuList == 'pasien') {
            $view = 'master/pasien_view';
        } elseif ($menuList == 'user') {
            $view = 'master/user_view';
        } elseif ($menuList == 'anamnesis') {
            $view = 'proses/anamnesis_view';
        } elseif ($menuList == 'pemeriksaan') {
            $view = 'proses/pemeriksaan_view';
        } elseif ($menuList == 'pemeriksaanfisik') {
            $view = 'proses/pemeriksaan_fisik_view';
        } elseif ($menuList == 'laporan') {
            $view = 'laporan/laporan_view';
        } elseif ($menuList == 'export') {
            $view = 'laporan/export_view';
        } elseif ($menuList == 'import') {
            $view = 'laporan/import_view';
        }else {
            $view = 'dashboard_view';
//            $data = array(
//                'base_url' => base_url(), 
//                'user_id' => $this->session->userdata('sess_user_id'),
//                'user_email' => $this->session->userdata('sess_email'),
//                'user_nama' => $this->session->userdata('sess_nama'),
//                'user_jejaring' => $this->session->userdata('sess_jejaring'),
//                'user_jabatan' => $this->session->userdata('sess_jabatan'),
//                'user_jejaring' => $this->session->userdata('sess_jejaring'),
//                'user_hak_akses' => $this->session->userdata('sess_hak_akses')
//            );
//            $this->parser->parse($view, $data);
        }
        
        $data = array(
            'base_url' => base_url(),
            'user_id' => $this->session->userdata('sess_user_id'),
            'user_nama' => $this->session->userdata('sess_nama'),
            'user_email' => $this->session->userdata('sess_email'),            
            'user_hak_akses' => $this->session->userdata('sess_hak_akses'),
            'user_parent' => $this->session->userdata('sess_parent')           
        );
        $this->parser->parse($view, $data);
        
    }
    
    public function Data($opt){
//        $this->load->model('m_pmlangsung_model');
        $this->load->model('m_dashboard');
    	if($opt=='Anamnesis'){
//            extract($_GET);
            $data = $this->m_dashboard->getDataAnamnesis();
                    echo json_encode($data,JSON_NUMERIC_CHECK);    
        }
//        elseif($opt=='Sekolah'){
//            $data = $this->m_dashboard->getDataApprovePmLangsungSekolah();
//                    echo json_encode($data,JSON_NUMERIC_CHECK);    
//        }
        else
                echo json_encode($data,JSON_NUMERIC_CHECK); 

    }
    
    function updateApprove($tabel, $filed, $id){
        $data = array(
            'grafik' => '1',
        );
        $this->db->where($filed, $id);
        $this->db->update($tabel, $data);
    }
    
}