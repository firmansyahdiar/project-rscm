<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User extends MY_Controller {
        
    function __construct() {
        parent::__construct();
        $mdl = "m_user_model";
        $this->model_name = $mdl;
        $this->load->model($mdl);
        $this->model = $this->$mdl;
        $this->pkField = "id_user";
        $this->uniqueFields = array("email");
//        $this->by = array ("parent" => $this->session->userdata('sess_user_id'));
//        $this->cari = "hak_akses";
//        $this->q = $this->session->userdata('sess_hak_akses');

        //pair key value (field => TYPE)
        //TYPE: EMAIL/STRING/INT/FLOAT/BOOLEAN/DATE/PASSWORD/URL/IP/MAC/RAW/DATA(TYPE,LABEL,MODEL,FIELD)
        $this->fields = array(
            "nama_user" => array("TIPE" => "STRING", "LABEL" => "Nama User"),
            "password" => array("TIPE" => "PASSWORD", "LABEL" => "Password"),
            "email" => array("TIPE" => "EMAIL", "LABEL" => "Email"),
            "hak_akses" => array("TIPE" => "STRING", "LABEL" => "Hak Akses"),
            "parent" => array("TIPE" => "STRING", "LABEL" => "Parent User"),
            "modified" => array("TIPE" => "DATE", "LABEL" => "Modified")
        );
    }

    public function index() {
        $data = array(
            'base_url' => base_url(),
            'user_id' => $this->session->userdata('sess_user_id'),
            'user_email' => $this->session->userdata('sess_email'),
            'user_nama' => $this->session->userdata('sess_nama'),
            'user_hak_akses' => $this->session->userdata('sess_hak_akses'),
            'user_parent' => $this->session->userdata('sess_parent')
        );
        $this->parser->parse("master/user_view", $data);        
    }    
    
    public function convertData() {
    	if($this->data["password"]!=''){
	        $this->convert = array(
	            "password" => md5($this->data["password"])
	        );
    	}
    }
}
