<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pasien extends MY_Controller {
        
    function __construct() {
        parent::__construct();
        $mdl = "m_pasien_model";
        $this->model_name = $mdl;
        $this->load->model($mdl);
        $this->model = $this->$mdl;
        $this->pkField = "id_pasien";
//        $this->uniqueFields = array("nama");

        //pair key value (field => TYPE)
        //TYPE: EMAIL/STRING/INT/FLOAT/BOOLEAN/DATE/PASSWORD/URL/IP/MAC/RAW/DATA(TYPE,LABEL,MODEL,FIELD)
        $this->fields = array(
            "nama_pasien" => array("TIPE" => "STRING", "LABEL" => "Nama Pasien"),
            "namaOrangTua" => array("TIPE" => "STRING", "LABEL" => "Nama Orang Tua Pasien"),
            "usia" => array("TIPE" => "STRING", "LABEL" => "Usia"),
            "pendidikan" => array("TIPE" => "STRING", "LABEL" => "Pendidikan"),
            "tempatLahir" => array("TIPE" => "STRING", "LABEL" => "Tempat Lahir"),
            "tglLahir" => array("TIPE" => "DATE", "LABEL" => "Tanggal Lahir"),
            "pekerjaan" => array("TIPE" => "STRING", "LABEL" => "Pekerjaan"),
            "suku" => array("TIPE" => "STRING", "LABEL" => "Suku"),
            "keturunan" => array("TIPE" => "STRING", "LABEL" => "Keturunan"),
            "asing" => array("TIPE" => "STRING", "LABEL" => "Telepon Asing"),
            "noRekamMedis" => array("TIPE" => "STRING", "LABEL" => "No Rekam Medis"),
            "agama" => array("TIPE" => "STRING", "LABEL" => "Agama"),
            "daerah" => array("TIPE" => "STRING", "LABEL" => "Daerah"),
            "jJalan" => array("TIPE" => "STRING", "LABEL" => "Jalan"),
            "jRt" => array("TIPE" => "STRING", "LABEL" => "RT"),
            "jRw" => array("TIPE" => "STRING", "LABEL" => "RW"),
            "jNo" => array("TIPE" => "STRING", "LABEL" => "No"),
            "jTelp" => array("TIPE" => "STRING", "LABEL" => "Telp"),
            "jKelurahan" => array("TIPE" => "STRING", "LABEL" => "Kelurahan"),
            "jKecamatan" => array("TIPE" => "STRING", "LABEL" => "Kecamatan"),
            "aJalan" => array("TIPE" => "STRING", "LABEL" => "Jalan"),
            "aRt" => array("TIPE" => "STRING", "LABEL" => "RT"),
            "aRw" => array("TIPE" => "STRING", "LABEL" => "RW"),
            "aNo" => array("TIPE" => "STRING", "LABEL" => "No"),
            "aTelp" => array("TIPE" => "STRING", "LABEL" => "Telp"),
            "aKelurahan" => array("TIPE" => "STRING", "LABEL" => "Kelurahan"),
            "aKecamatan" => array("TIPE" => "STRING", "LABEL" => "Kecamatan"),
            "aKabupaten" => array("TIPE" => "STRING", "LABEL" => "Kabupaten"),
            "aProvinsi" => array("TIPE" => "STRING", "LABEL" => "Provinsi"),
            "user_id" => array("TIPE" => "STRING", "LABEL" => "User ID"),
            "modified" => array("TIPE" => "DATE", "LABEL" => "Modified")
        );
    }

    public function index() {
        $data = array(
            'base_url' => base_url(),
            'user_id' => $this->session->userdata('sess_user_id'),
            'user_email' => $this->session->userdata('sess_email'),
            'user_nama' => $this->session->userdata('sess_nama'),
            'user_hak_akses' => $this->session->userdata('sess_hak_akses')
        );
        $this->parser->parse("master/pasien_view", $data);        
    }
    
    public function convertData() {
    	if($this->data["tglLahir"]!=''){
	        $this->convert = array(
	            "tglLahir" => encode_date($this->data["tglLahir"])
	        );
    	}
    }
}