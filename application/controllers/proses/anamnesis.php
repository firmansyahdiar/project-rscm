<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Anamnesis extends MY_Controller {
        
    function __construct() {
        parent::__construct();
        $mdl = "p_anamnesis_model";
        $this->model_name = $mdl;
        $this->load->model($mdl);
        $this->model = $this->$mdl;
        $this->pkField = "id_anamnesis";
//        $this->uniqueFields = array("nama");

        //pair key value (field => TYPE)
        //TYPE: EMAIL/STRING/INT/FLOAT/BOOLEAN/DATE/PASSWORD/URL/IP/MAC/RAW/DATA(TYPE,LABEL,MODEL,FIELD)
        $this->fields = array(
            "id_pasien" => array("TIPE" => "DATA", "LABEL" => "Data Pasien", "MODEL" => "m_pasien_model", "FIELD" => "id_pasien"),
//            "id_pasien" => array("TIPE" => "STRING", "LABEL" => "Data Pasien"),
            "kunjungan" => array("TIPE" => "STRING", "LABEL" => "Kunjungan"),
            "tgl_periksa" => array("TIPE" => "DATE", "LABEL" => "Tanggal Periksa"),
            
//      KARSINOMA NASOFARING (A)            
            "Aa1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Aa2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Aa3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ab1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ab2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ab3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ac1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ac2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ac3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ad1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ad2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ad3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ae1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ae2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ae3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Af1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Af2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Af3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ag1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ag2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ag3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ah1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ah2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ah3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ai1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ai2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ai3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Aj1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Aj2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Aj3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ak1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ak2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ak3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Al1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Al2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Al3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Am1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Am2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Am3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR VACUM NASI (B)
            "Ba1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ba2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ba3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bb3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bc2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bc3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bd2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bd3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Be1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Be2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Be3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bf1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bf2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bf3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bg1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bg2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bg3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bh1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bh2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bh3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bi1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bi2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bi3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bj1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bj2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bj3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//       TUMOR ORAL CAVITY (C)
            "Ca1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ca2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ca3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Cb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cb3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Cc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cc2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cc3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Cd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cd2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cd3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ce1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ce2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ce3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Cf1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cf2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cf3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR OROFARING (D)
            "Da1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Da2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Da3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Db1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Db2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Db3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Dc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Dc2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Dc3" => array("TIPE" => "STRING", "LABEL" => " "),           
            "Dd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Dd2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Dd3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "De1" => array("TIPE" => "STRING", "LABEL" => " "),
            "De2" => array("TIPE" => "STRING", "LABEL" => " "),
            "De3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Df1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Df2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Df3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      ANGIOFIBROMA NASOFARING (E)
            "Ea1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ea2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ea3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Eb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eb3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ec1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ec2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ec3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ed1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ed2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ed3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ee1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ee2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ee3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ef1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ef2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ef2Lainnya" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Eg1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eg2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eg3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eg4" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eg5" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eg6" => array("TIPE" => "STRING", "LABEL" => " "),
            "EgLainnya" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR LARING (F)
            "Fa1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fa2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fc2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fd2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fe1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fe2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ff1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ff2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fg1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fg2" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR PAROTIS (G)
            "Ga1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ga2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ga3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gb3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gd2" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR TELINGA (H)
            "Ha1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ha2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ha3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hb3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hc2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hc3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hd2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hd3" => array("TIPE" => "STRING", "LABEL" => " "),
            "He1" => array("TIPE" => "STRING", "LABEL" => " "),
            "He2" => array("TIPE" => "STRING", "LABEL" => " "),
            "He3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hf1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hf2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hf3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hg1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hg2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hg3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hh1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hh2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hh3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hi1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hi2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hi3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hj1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hj2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hj3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR ESOFAGUS (I)
            "Ia1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ia2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ia3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ib1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ib2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ib3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ic1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ic2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ic3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Id1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Id2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Id3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ie1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ie2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ie3" => array("TIPE" => "STRING", "LABEL" => " "),
            "If1" => array("TIPE" => "STRING", "LABEL" => " "),
            "If2" => array("TIPE" => "STRING", "LABEL" => " "),
            "If3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ig1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ig2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ig3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ih1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ih2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ih3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      RIWAYAT KELUARGA (J)
            "Ja1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ja2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ja2Lainnya" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ja3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ja3Lainnya" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      FAKTOR RISIKO (K)
            "Ka1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ka2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ka3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ka4" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Kb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Kb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Kb3" => array("TIPE" => "STRING", "LABEL" => " "),   
            "Kb4" => array("TIPE" => "STRING", "LABEL" => " "),   
            
            "user_id" => array("TIPE" => "STRING", "LABEL" => "User ID"),
            "modified" => array("TIPE" => "DATE", "LABEL" => "Modified")
        );
    }

    public function index() {
        $data = array(
            'base_url' => base_url(),
            'user_id' => $this->session->userdata('sess_user_id'),
            'user_email' => $this->session->userdata('sess_email'),
            'user_nama' => $this->session->userdata('sess_nama'),
            'user_hak_akses' => $this->session->userdata('sess_hak_akses')
        );
        $this->parser->parse("proses/anamnesis_view", $data);        
    }

    public function convertData() {
//    	if($this->data["tgl_periksa"]!=''){
//	        $this->convert = array(
//	            "tgl_periksa" => encode_date($this->data["tgl_periksa"])
//	        );
//    	}
        $this->convert = array(
            
            "Eg1" => $this->data["Eg1"] == "true" ? 1 : 0,
            "Eg2" => $this->data["Eg2"] == "true" ? 1 : 0,
            "Eg3" => $this->data["Eg3"] == "true" ? 1 : 0,
            "Eg4" => $this->data["Eg4"] == "true" ? 1 : 0,
            "Eg5" => $this->data["Eg5"] == "true" ? 1 : 0,
            "Eg6" => $this->data["Eg6"] == "true" ? 1 : 0,
            
//            TAB KARSINOMA NASOFARING (A)
            
//            TAB TUMOR VACUM NASI (B)
            
//            TAB TUMOR ORAL CAVITY (C)
            
//            TAB TUMOR OROFARING (D)
            
//            TAB ANGIOFIBROMA NASOFARING (E)
            
            "tgl_periksa" => encode_date($this->data["tgl_periksa"])
        );
        
    }
}