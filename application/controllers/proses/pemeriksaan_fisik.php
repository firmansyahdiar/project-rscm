<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pemeriksaan_fisik extends MY_Controller {
        
    function __construct() {
        parent::__construct();
        $mdl = "p_pemeriksaan_fisik_model";
        $this->model_name = $mdl;
        $this->load->model($mdl);
        $this->model = $this->$mdl;
        $this->pkField = "id_pemeriksaan_fisik";
//        $this->uniqueFields = array("nama");

        //pair key value (field => TYPE)
        //TYPE: EMAIL/STRING/INT/FLOAT/BOOLEAN/DATE/PASSWORD/URL/IP/MAC/RAW/DATA(TYPE,LABEL,MODEL,FIELD)
        $this->fields = array(
            "id_pasien" => array("TIPE" => "DATA", "LABEL" => "Data Pasien", "MODEL" => "m_pasien_model", "FIELD" => "id_pasien"),
//            "id_pasien" => array("TIPE" => "STRING", "LABEL" => "Data Pasien"),
            "kunjungan" => array("TIPE" => "STRING", "LABEL" => "Kunjungan"),
            "tgl_periksa" => array("TIPE" => "DATE", "LABEL" => "Tanggal Periksa"),
            "td" => array("TIPE" => "STRING", "LABEL" => "TD"),
            "nadi" => array("TIPE" => "STRING", "LABEL" => "Nadi"),
            "tb" => array("TIPE" => "STRING", "LABEL" => "Tinggi Badan"),
            "bb" => array("TIPE" => "STRING", "LABEL" => "Berat Badan"),
                        
            "ks1" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks2" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks3" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks4" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks5" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks6" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks7" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks8" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks9" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks10" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks11" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks12" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks13" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks14" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks15" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks16" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks17" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks18" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks19" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks20" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks21" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks22" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks23" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks24" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks25" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks26" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks27" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks28" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks29" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks30" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks31" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks32" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks33" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks34" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks35" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks36" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks37" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks38" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks39" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks40" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks41" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks42" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks43" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks44" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks45" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks46" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks47" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks48" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks49" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks50" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks51" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks52" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks53" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks54" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks55" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks56" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks57" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks58" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks59" => array("TIPE" => "STRING", "LABEL" => " "),
            
            "a1" => array("TIPE" => "DATE", "LABEL" => " "),
            "a2" => array("TIPE" => "STRING", "LABEL" => " "),
            "a3" => array("TIPE" => "STRING", "LABEL" => " "),
            "a4" => array("TIPE" => "STRING", "LABEL" => " "),
            "a5" => array("TIPE" => "STRING", "LABEL" => " "),
            "a6" => array("TIPE" => "STRING", "LABEL" => " "),
            "a7" => array("TIPE" => "STRING", "LABEL" => " "),
            "a7a" => array("TIPE" => "STRING", "LABEL" => " "),
            "a8" => array("TIPE" => "STRING", "LABEL" => " "),
            "a9" => array("TIPE" => "DATE", "LABEL" => " "),
            "a10" => array("TIPE" => "STRING", "LABEL" => " "),
            "a11" => array("TIPE" => "STRING", "LABEL" => " "),
            "a12" => array("TIPE" => "STRING", "LABEL" => " "),
            "a13" => array("TIPE" => "STRING", "LABEL" => " "),
            "a14" => array("TIPE" => "STRING", "LABEL" => " "),
            "a15" => array("TIPE" => "STRING", "LABEL" => " "),
            "a16" => array("TIPE" => "STRING", "LABEL" => " "),
            "a17" => array("TIPE" => "DATE", "LABEL" => " "),
            "a18" => array("TIPE" => "STRING", "LABEL" => " "),
            "a19" => array("TIPE" => "STRING", "LABEL" => " "),
            "a20" => array("TIPE" => "STRING", "LABEL" => " "),
            "a21" => array("TIPE" => "DATE", "LABEL" => " "),
            "a22" => array("TIPE" => "STRING", "LABEL" => " "),
            "a23" => array("TIPE" => "STRING", "LABEL" => " "),
            "a24" => array("TIPE" => "STRING", "LABEL" => " "),
            "a25" => array("TIPE" => "DATE", "LABEL" => " "),
            "a26" => array("TIPE" => "STRING", "LABEL" => " "),
            "a27" => array("TIPE" => "STRING", "LABEL" => " "),
            "a28" => array("TIPE" => "STRING", "LABEL" => " "),
            "a29" => array("TIPE" => "DATE", "LABEL" => " "),
            "a30" => array("TIPE" => "STRING", "LABEL" => " "),
            "a31" => array("TIPE" => "STRING", "LABEL" => " "),
            "a32" => array("TIPE" => "STRING", "LABEL" => " "),
            "a33" => array("TIPE" => "DATE", "LABEL" => " "),
            "a34" => array("TIPE" => "STRING", "LABEL" => " "),
            "a35" => array("TIPE" => "STRING", "LABEL" => " "),
            "a36" => array("TIPE" => "STRING", "LABEL" => " "),
            "a37" => array("TIPE" => "DATE", "LABEL" => " "),
            "a38" => array("TIPE" => "STRING", "LABEL" => " "),
            "a39" => array("TIPE" => "STRING", "LABEL" => " "),
            "a40" => array("TIPE" => "STRING", "LABEL" => " "),
            "a41" => array("TIPE" => "DATE", "LABEL" => " "),
            "a42" => array("TIPE" => "STRING", "LABEL" => " "),
            "a43" => array("TIPE" => "STRING", "LABEL" => " "),
            "a44" => array("TIPE" => "STRING", "LABEL" => " "),
            "a45" => array("TIPE" => "STRING", "LABEL" => " "),
            "a46" => array("TIPE" => "STRING", "LABEL" => " "),
            "a47" => array("TIPE" => "STRING", "LABEL" => " "),
            "a48" => array("TIPE" => "DATE", "LABEL" => " "),
            "a49" => array("TIPE" => "STRING", "LABEL" => " "),
            "a50" => array("TIPE" => "STRING", "LABEL" => " "),
            "a51" => array("TIPE" => "STRING", "LABEL" => " "),
            "a52" => array("TIPE" => "DATE", "LABEL" => " "),
            "a53" => array("TIPE" => "STRING", "LABEL" => " "),
            "a54" => array("TIPE" => "STRING", "LABEL" => " "),
            "a55" => array("TIPE" => "STRING", "LABEL" => " "),
            
            "b1" => array("TIPE" => "STRING", "LABEL" => " "),
            "b2" => array("TIPE" => "DATE", "LABEL" => " "),
            "b3" => array("TIPE" => "STRING", "LABEL" => " "),
            "b4" => array("TIPE" => "STRING", "LABEL" => " "),
            "b5" => array("TIPE" => "STRING", "LABEL" => " "),
            "b6" => array("TIPE" => "DATE", "LABEL" => " "),
            "b7" => array("TIPE" => "DATE", "LABEL" => " "),
            "b8" => array("TIPE" => "STRING", "LABEL" => " "),
            "b9" => array("TIPE" => "STRING", "LABEL" => " "),
            "b10" => array("TIPE" => "DATE", "LABEL" => " "),
            "b11" => array("TIPE" => "DATE", "LABEL" => " "),
            "b12" => array("TIPE" => "STRING", "LABEL" => " "),
            "b13" => array("TIPE" => "STRING", "LABEL" => " "),
            "b14" => array("TIPE" => "STRING", "LABEL" => " "),
            "b15" => array("TIPE" => "DATE", "LABEL" => " "),
            "b16" => array("TIPE" => "DATE", "LABEL" => " "),
            "b17" => array("TIPE" => "STRING", "LABEL" => " "),
            "b18" => array("TIPE" => "STRING", "LABEL" => " "),
            "b19" => array("TIPE" => "STRING", "LABEL" => " "),
            "b20" => array("TIPE" => "STRING", "LABEL" => " "),
            "b21" => array("TIPE" => "STRING", "LABEL" => " "),
            "b22a" => array("TIPE" => "STRING", "LABEL" => " "),
            "b22b" => array("TIPE" => "STRING", "LABEL" => " "),
            "b22c" => array("TIPE" => "STRING", "LABEL" => " "),
            "b22d" => array("TIPE" => "STRING", "LABEL" => " "),
            "b22e" => array("TIPE" => "STRING", "LABEL" => " "),
            "b22f" => array("TIPE" => "STRING", "LABEL" => " "),
            "b22g" => array("TIPE" => "STRING", "LABEL" => " "),
            "b23" => array("TIPE" => "STRING", "LABEL" => " "),
            "b24" => array("TIPE" => "STRING", "LABEL" => " "),
            "b25" => array("TIPE" => "STRING", "LABEL" => " "),
            "b26" => array("TIPE" => "DATE", "LABEL" => " "),
            "b27" => array("TIPE" => "STRING", "LABEL" => " "),
            "b28" => array("TIPE" => "STRING", "LABEL" => " "),
            "b29" => array("TIPE" => "STRING", "LABEL" => " "),
            "b30" => array("TIPE" => "STRING", "LABEL" => " "),
            "b31" => array("TIPE" => "STRING", "LABEL" => " "),
                        
            "user_id" => array("TIPE" => "STRING", "LABEL" => "User ID"),
            "modified" => array("TIPE" => "DATE", "LABEL" => "Modified")
        );
    }

    public function index() {
        $data = array(
            'base_url' => base_url(),
            'user_id' => $this->session->userdata('sess_user_id'),
            'user_email' => $this->session->userdata('sess_email'),
            'user_nama' => $this->session->userdata('sess_nama'),
            'user_hak_akses' => $this->session->userdata('sess_hak_akses')
        );
        $this->parser->parse("proses/pemeriksaan_fisik_view", $data);        
    }

    public function convertData() {
//    	if($this->data["tgl_periksa"]!=''){
//	        $this->convert = array(
//	            "tgl_periksa" => encode_date($this->data["tgl_periksa"])
//	        );
//    	}
        $this->convert = array(
            
            /*"ks1" => $this->data["ks1"] == "true" ? 1 : 0,
            "ks2" => $this->data["ks2"] == "true" ? 1 : 0,
            "ks3" => $this->data["ks3"] == "true" ? 1 : 0,
            "ks4" => $this->data["ks4"] == "true" ? 1 : 0,
            "ks5" => $this->data["ks5"] == "true" ? 1 : 0,
            "ks6" => $this->data["ks6"] == "true" ? 1 : 0,
            "ks7" => $this->data["ks7"] == "true" ? 1 : 0,
            "ks8" => $this->data["ks8"] == "true" ? 1 : 0,
            "ks9" => $this->data["ks9"] == "true" ? 1 : 0,
            "ks10" => $this->data["ks10"] == "true" ? 1 : 0,
            "ks11" => $this->data["ks11"] == "true" ? 1 : 0,*/
            
            "tgl_periksa" => encode_date($this->data["tgl_periksa"]),
            "a1" => encode_date($this->data["a1"]),
            "a9" => encode_date($this->data["a9"]),
            "a17" => encode_date($this->data["a17"]),
            "a21" => encode_date($this->data["a21"]),
            "a25" => encode_date($this->data["a25"]),
            "a29" => encode_date($this->data["a29"]),
            "a33" => encode_date($this->data["a33"]),
            "a37" => encode_date($this->data["a37"]),
            "a41" => encode_date($this->data["a41"]),
            "b2" => encode_date($this->data["b2"]),
            "b6" => encode_date($this->data["b6"]),
            "b7" => encode_date($this->data["b7"]),
            "b10" => encode_date($this->data["b10"]),
            "b11" => encode_date($this->data["b11"]),
            "b15" => encode_date($this->data["b15"]),
            "b16" => encode_date($this->data["b16"]),
            "b26" => encode_date($this->data["b26"]),
            
            "b22a" => $this->data["b22a"] == "true" ? 1 : 0,
            "b22b" => $this->data["b22b"] == "true" ? 1 : 0,
            "b22c" => $this->data["b22c"] == "true" ? 1 : 0,
            "b22d" => $this->data["b22d"] == "true" ? 1 : 0,
            "b22e" => $this->data["b22e"] == "true" ? 1 : 0,
            "b22f" => $this->data["b22f"] == "true" ? 1 : 0,
            "b22g" => $this->data["b22g"] == "true" ? 1 : 0,
        );
        
    }
    
    public function upload($name) {
        $dir = date('Ym');
    	$lokasi = "./files/".$dir."/";
    	$id = $name;
        $i=0;
    	if (($_FILES[$id]["size"] < 50000000)) {
            if ($_FILES[$id]["error"] > 0) {
                echo json_encode(array('msg' => "Return Code: " . $_FILES[$id]["error"] . "<br>"));
            } else {
                if(!is_dir($lokasi)){
                    mkdir($lokasi);
                }
                while (file_exists($lokasi . $_FILES[$id]["name"])) {
                    $_FILES[$id]["name"]= pathinfo($_FILES[$id]["name"],PATHINFO_FILENAME).$i.".".pathinfo($_FILES[$id]["name"],PATHINFO_EXTENSION);
                    $i++;
                }
                move_uploaded_file($_FILES[$id]["tmp_name"], $lokasi . $_FILES[$id]["name"]);
                echo json_encode(array('success' => true, 'filename' => $lokasi.$_FILES[$id]["name"]));
            } 
    			/*$import = $this->import($_FILES[$id]["name"]);
    			if ($import != "") {
    				echo json_encode($import);
    			}*/
    	} else {
            echo json_encode(array('msg' => "File terlalu besar."));
    	}
    }
}