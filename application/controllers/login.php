<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('m_user_model');
    }

    public function index() {
        if ($this->session->userdata('logged_in')) {
            redirect('index.php/Home');
        } else {
            $data = array(
                'title' => 'Login',
                'base_url' => base_url()
            );
            $this->parser->parse('login_view', $data);
        }
    }

    public function masuk() {
        $email = $this->input->post('email');
        if ($email) {
            $condition["m_user.email"] = $email;
            $user = $this->m_user_model->get(NULL, $condition);
            if (count($user) > 0) {
                foreach ($user as $row) {
                    if (md5($this->input->post('password')) == $row->password) {
                        $sess_data = array(
                            'sess_user_id' => $row->id_user,
                            'sess_nama' => $row->nama_user,
                            'sess_email' => $row->email,                         
                            'sess_hak_akses' => $row->hak_akses,
                            'sess_parent' => $row->parent,
                            'logged_in' => TRUE
                        );
                        $this->session->set_userdata($sess_data);
                        echo json_encode(array(
                            'success' => true
                        ));
                    } else {
                        echo json_encode(array(
                            'msg' => 'Password Anda salah.'
                        ));
                    }
                }
            } else {
                echo json_encode(array(
                    'msg' => 'Email Anda salah.'
                ));
            }
        } else {
            echo json_encode(array(
                'msg' => 'Email belum diisi.'
            ));
        }
    }

    public function keluar() {
        $this->session->sess_destroy();
        redirect('');
    }
}
