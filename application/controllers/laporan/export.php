<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Export extends MY_Controller {

    function __construct() {
        parent::__construct();
        $mdl = "l_export";
        $this->model_name = $mdl;
        $this->load->model($mdl);
        $this->model = $this->$mdl;
//        $this->pkField = "id_pasien";
//        $this->uniqueFields = array("id_pasien");

        //pair key value (field => TYPE)
        //TYPE: EMAIL/STRING/INT/FLOAT/BOOLEAN/DATE/PASSWORD/URL/IP/MAC/RAW/DATA(TYPE,LABEL,MODEL,FIELD)
//        $this->fields = array(
//            "nama" => array("TIPE" => "STRING", "LABEL" => "Nama"),
//            "keterangan" => array("TIPE" => "STRING", "LABEL" => "Keterangan"),
//            "status" => array("TIPE" => "STRING", "LABEL" => "Status")
//        );
    }

    public function index() {
        
    }
    
    public function exportData(){
        $list = $this->model->getPasien();
        $this->load->library('excel');
        $sheet_title=array('Pasien','Anamnesis','Pemeriksaan Fisik');
        $sheet = 0;
        try{
            foreach ($list as $rs){
                if($sheet !=0){
                    $this->excel->createSheet();
                }
                $this->excel->setActiveSheetIndex($sheet);
                $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                $this->excel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
                $this->excel->getActiveSheet()->setTitle($sheet_title[$sheet]);
                $col=1;             
                foreach($rs as $result){
                    $row=0;
                    foreach($result as $k => $v){
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($row, $col, $v);
                        $row++;
                    }
                    $col++;
                }
                $sheet++;
            }
        } catch (Exception $ex) {
            var_dump($ex);
        }
        
        $this->excel->setActiveSheetIndex(0);
        $filename='rscm.bak';
        //header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter->save('php://output');
    }
}