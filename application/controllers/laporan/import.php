<?php

class Import extends MY_Controller {
    var $model_pasien;
    var $model_anamnesis;
    var $model_pemeriksaan_fisik;
    var $fields_pasien;
    var $fields_anamnesis;
    var $fields_pemeriksaan_fisik;

    function __construct() {
        parent::__construct();
        parent::__construct();
        /*$mdl = "l_import";
        $this->model_name = $mdl;
        $this->load->model($mdl);
        $this->model = $this->$mdl;*/
        $this->load->model('m_pasien_model');
        $this->model_pasien = $this->m_pasien_model;
        $this->load->model('p_anamnesis_model');
        $this->model_anamnesis = $this->p_anamnesis_model;
        $this->load->model('p_pemeriksaan_fisik_model');
        $this->model_pemeriksaan_fisik = $this->p_pemeriksaan_fisik_model;
        
        $this->fields_pasien = array(
            "id_pasien" => array("TIPE" => "Data", "LABEL" => "iD Pasien"),
            "nama_pasien" => array("TIPE" => "STRING", "LABEL" => "Nama Pasien"),
            "namaOrangTua" => array("TIPE" => "STRING", "LABEL" => "Nama Orang Tua Pasien"),
            "usia" => array("TIPE" => "STRING", "LABEL" => "Usia"),
            "pendidikan" => array("TIPE" => "STRING", "LABEL" => "Pendidikan"),
            "tempatLahir" => array("TIPE" => "STRING", "LABEL" => "Tempat Lahir"),
            "tglLahir" => array("TIPE" => "DATE", "LABEL" => "Tanggal Lahir"),
            "pekerjaan" => array("TIPE" => "STRING", "LABEL" => "Pekerjaan"),
            "suku" => array("TIPE" => "STRING", "LABEL" => "Suku"),
            "keturunan" => array("TIPE" => "STRING", "LABEL" => "Keturunan"),
            "asing" => array("TIPE" => "STRING", "LABEL" => "Telepon Asing"),
            "noRekamMedis" => array("TIPE" => "STRING", "LABEL" => "No Rekam Medis"),
            "agama" => array("TIPE" => "STRING", "LABEL" => "Agama"),
            "daerah" => array("TIPE" => "STRING", "LABEL" => "Daerah"),
            "jJalan" => array("TIPE" => "STRING", "LABEL" => "Jalan"),
            "jRt" => array("TIPE" => "STRING", "LABEL" => "RT"),
            "jRw" => array("TIPE" => "STRING", "LABEL" => "RW"),
            "jNo" => array("TIPE" => "STRING", "LABEL" => "No"),
            "jTelp" => array("TIPE" => "STRING", "LABEL" => "Telp"),
            "jKelurahan" => array("TIPE" => "STRING", "LABEL" => "Kelurahan"),
            "jKecamatan" => array("TIPE" => "STRING", "LABEL" => "Kecamatan"),
            "aJalan" => array("TIPE" => "STRING", "LABEL" => "Jalan"),
            "aRt" => array("TIPE" => "STRING", "LABEL" => "RT"),
            "aRw" => array("TIPE" => "STRING", "LABEL" => "RW"),
            "aNo" => array("TIPE" => "STRING", "LABEL" => "No"),
            "aTelp" => array("TIPE" => "STRING", "LABEL" => "Telp"),
            "aKelurahan" => array("TIPE" => "STRING", "LABEL" => "Kelurahan"),
            "aKecamatan" => array("TIPE" => "STRING", "LABEL" => "Kecamatan"),
            "aKabupaten" => array("TIPE" => "STRING", "LABEL" => "Kabupaten"),
            "aProvinsi" => array("TIPE" => "STRING", "LABEL" => "Provinsi"),
            "user_id" => array("TIPE" => "STRING", "LABEL" => "User ID"),
            "modified" => array("TIPE" => "DATE", "LABEL" => "Modified")
        );
        $this->f_pasien = array(
            "id_pasien","nama_pasien","namaOrangTua","usia","pendidikan","tempatLahir","tglLahir","pekerjaan","suku",
            "keturunan","asing","noRekamMedis","agama","daerah","jJalan","jRt","jRw","jNo","jTelp","jKelurahan","jKecamatan",
            "aJalan","aRt","aRw","aNo","aTelp","aKelurahan","aKecamatan","aKabupaten","aProvinsi","user_id","modified"
        );
        $this->pkField_pasien = 'id_pasien';
        
        $this->fields_anamnesis = array(
            "id_anamnesis" => array("TIPE" => "Data", "LABEL" => "ID anamnesis"),
            "id_pasien" => array("TIPE" => "Data", "LABEL" => "Data Pasien"),
//            "id_pasien" => array("TIPE" => "STRING", "LABEL" => "Data Pasien"),
            "kunjungan" => array("TIPE" => "STRING", "LABEL" => "Kunjungan"),
            "tgl_periksa" => array("TIPE" => "DATE", "LABEL" => "Tanggal Periksa"),
            
//      KARSINOMA NASOFARING (A)            
            "Aa1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Aa2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Aa3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ab1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ab2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ab3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ac1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ac2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ac3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ad1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ad2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ad3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ae1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ae2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ae3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Af1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Af2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Af3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ag1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ag2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ag3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ah1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ah2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ah3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ai1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ai2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ai3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Aj1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Aj2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Aj3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ak1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ak2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ak3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Al1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Al2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Al3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Am1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Am2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Am3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR VACUM NASI (B)
            "Ba1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ba2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ba3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bb3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bc2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bc3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bd2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bd3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Be1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Be2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Be3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bf1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bf2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bf3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bg1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bg2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bg3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bh1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bh2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bh3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bi1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bi2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bi3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Bj1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bj2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Bj3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//       TUMOR ORAL CAVITY (C)
            "Ca1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ca2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ca3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Cb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cb3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Cc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cc2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cc3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Cd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cd2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cd3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ce1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ce2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ce3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Cf1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cf2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Cf3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR OROFARING (D)
            "Da1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Da2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Da3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Db1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Db2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Db3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Dc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Dc2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Dc3" => array("TIPE" => "STRING", "LABEL" => " "),           
            "Dd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Dd2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Dd3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "De1" => array("TIPE" => "STRING", "LABEL" => " "),
            "De2" => array("TIPE" => "STRING", "LABEL" => " "),
            "De3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Df1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Df2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Df3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      ANGIOFIBROMA NASOFARING (E)
            "Ea1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ea2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ea3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Eb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eb3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ec1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ec2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ec3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ed1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ed2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ed3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ee1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ee2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ee3" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Ef1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ef2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ef2Lainnya" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Eg1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eg2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eg3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eg4" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eg5" => array("TIPE" => "STRING", "LABEL" => " "),
            "Eg6" => array("TIPE" => "STRING", "LABEL" => " "),
            "EgLainnya" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR LARING (F)
            "Fa1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fa2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fc2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fd2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fe1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fe2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ff1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ff2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fg1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Fg2" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR PAROTIS (G)
            "Ga1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ga2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ga3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gb3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Gd2" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR TELINGA (H)
            "Ha1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ha2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ha3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hb3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hc1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hc2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hc3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hd1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hd2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hd3" => array("TIPE" => "STRING", "LABEL" => " "),
            "He1" => array("TIPE" => "STRING", "LABEL" => " "),
            "He2" => array("TIPE" => "STRING", "LABEL" => " "),
            "He3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hf1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hf2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hf3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hg1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hg2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hg3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hh1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hh2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hh3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hi1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hi2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hi3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hj1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hj2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Hj3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      TUMOR ESOFAGUS (I)
            "Ia1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ia2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ia3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ib1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ib2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ib3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ic1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ic2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ic3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Id1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Id2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Id3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ie1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ie2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ie3" => array("TIPE" => "STRING", "LABEL" => " "),
            "If1" => array("TIPE" => "STRING", "LABEL" => " "),
            "If2" => array("TIPE" => "STRING", "LABEL" => " "),
            "If3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ig1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ig2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ig3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ih1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ih2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ih3" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      RIWAYAT KELUARGA (J)
            "Ja1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ja2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ja2Lainnya" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ja3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ja3Lainnya" => array("TIPE" => "STRING", "LABEL" => " "),
            
//      FAKTOR RISIKO (K)
            "Ka1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ka2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ka3" => array("TIPE" => "STRING", "LABEL" => " "),
            "Ka4" => array("TIPE" => "STRING", "LABEL" => " "),            
            "Kb1" => array("TIPE" => "STRING", "LABEL" => " "),
            "Kb2" => array("TIPE" => "STRING", "LABEL" => " "),
            "Kb3" => array("TIPE" => "STRING", "LABEL" => " "),   
            "Kb4" => array("TIPE" => "STRING", "LABEL" => " "),   
            
            "user_id" => array("TIPE" => "STRING", "LABEL" => "User ID"),
            "modified" => array("TIPE" => "DATE", "LABEL" => "Modified")
            
        );
        $this->f_anamnesis = array(
            "id_anamnesis",
            "id_pasien",
            "kunjungan",
            "tgl_periksa",
            
//                    KARSINOMA NASOFARING (A)
                        "Aa1",
                        "Aa2",
                        "Aa3",            
                        "Ab1",
                        "Ab2",
                        "Ab3",            
                        "Ac1",
                        "Ac2",
                        "Ac3",            
                        "Ad1",
                        "Ad2",
                        "Ad3",            
                        "Ae1",
                        "Ae2",
                        "Ae3",            
                        "Af1",
                        "Af2",
                        "Af3",            
                        "Ag1",
                        "Ag2",
                        "Ag3",            
                        "Ah1",
                        "Ah2",
                        "Ah3",            
                        "Ai1",
                        "Ai2",
                        "Ai3",            
                        "Aj1",
                        "Aj2",
                        "Aj3",            
                        "Ak1",
                        "Ak2",
                        "Ak3",            
                        "Al1",
                        "Al2",
                        "Al3",            
                        "Am1",
                        "Am2",
                        "Am3",

//                    TUMOR VACUM NASI (B)
                        "Ba1",
                        "Ba2",
                        "Ba3",            
                        "Bb1",
                        "Bb2",
                        "Bb3",            
                        "Bc1",
                        "Bc2",
                        "Bc3",            
                        "Bd1",
                        "Bd2",
                        "Bd3",            
                        "Be1",
                        "Be2",
                        "Be3",            
                        "Bf1",
                        "Bf2",
                        "Bf3",            
                        "Bg1",
                        "Bg2",
                        "Bg3",            
                        "Bh1",
                        "Bh2",
                        "Bh3",            
                        "Bi1",
                        "Bi2",
                        "Bi3",            
                        "Bj1",
                        "Bj2",
                        "Bj3",

//                    TUMOR ORAL CAVITY (C)
                        "Ca1",
                        "Ca2",
                        "Ca3",            
                        "Cb1",
                        "Cb2",
                        "Cb3",            
                        "Cc1",
                        "Cc2",
                        "Cc3",            
                        "Cd1",
                        "Cd2",
                        "Cd3",            
                        "Ce1",
                        "Ce2",
                        "Ce3",            
                        "Cf1",
                        "Cf2",
                        "Cf3",

//                    TUMOR OROFARNG (D)
                        "Da1",
                        "Da2",
                        "Da3",            
                        "Db1",
                        "Db2",
                        "Db3",            
                        "Dc1",
                        "Dc2",
                        "Dc3",            
                        "Dd1",
                        "Dd2",
                        "Dd3",            
                        "De1",
                        "De2",
                        "De3",            
                        "Df1",
                        "Df2",
                        "Df3",

//                    ANGIOFIBROMA NASOFARING (E)
                        "Ea1",
                        "Ea2",
                        "Ea3",            
                        "Eb1",
                        "Eb2",
                        "Eb3",            
                        "Ec1",
                        "Ec2",
                        "Ec3",            
                        "Ed1",
                        "Ed2",
                        "Ed3",            
                        "Ee1",
                        "Ee2",
                        "Ee3",            
                        "Ef1",
                        "Ef2",
                        "Ef2Lainnya",            
                        "Eg1",
                        "Eg2",
                        "Eg3",
                        "Eg4",
                        "Eg5",
                        "Eg6",
                        "EgLainnya",

//                    TUMOR LARING (F)
                        "Fa1",
                        "Fa2",
                        "Fb1",
                        "Fb2",
                        "Fc1",
                        "Fc2",
                        "Fd1",
                        "Fd2",
                        "Fe1",
                        "Fe2",
                        "Ff1",
                        "Ff2",
                        "Fg1",
                        "Fg2",

//                    TUMOR PAROTIS (G)
                        "Ga1",
                        "Ga2",
                        "Ga3",
                        "Gb1",
                        "Gb2",
                        "Gb3",
                        "Gc1",
                        "Gd1",
                        "Gd2",

//                    TUMOR TELINGA (H)
                        "Ha1",
                        "Ha2",
                        "Ha3",
                        "Hb1",
                        "Hb2",
                        "Hb3",
                        "Hc1",
                        "Hc2",
                        "Hc3",
                        "Hd1",
                        "Hd2",
                        "Hd3",
                        "He1",
                        "He2",
                        "He3",
                        "Hf1",
                        "Hf2",
                        "Hf3",
                        "Hg1",
                        "Hg2",
                        "Hg3",
                        "Hh1",
                        "Hh2",
                        "Hh3",
                        "Hi1",
                        "Hi2",
                        "Hi3",
                        "Hj1",
                        "Hj2",
                        "Hj3",

//                    TUMOR ESOFAGUS (I)
                        "Ia1",
                        "Ia2",
                        "Ia3",
                        "Ib1",
                        "Ib2",
                        "Ib3",
                        "Ic1",
                        "Ic2",
                        "Ic3",
                        "Id1",
                        "Id2",
                        "Id3",
                        "Ie1",
                        "Ie2",
                        "Ie3",
                        "If1",
                        "If2",
                        "If3",
                        "Ig1",
                        "Ig2",
                        "Ig3",
                        "Ih1",
                        "Ih2",
                        "Ih3",

//                    RIWAYAT KELUARGA (J)
                        "Ja1",
                        "Ja2",
                        "Ja2Lainnya",
                        "Ja3",
                        "Ja3Lainnya",  

//                    FAKTOR RISIKO (K)
                        "Ka1",
                        "Ka2",
                        "Ka3",
                        "Ka4",            
                        "Kb1",
                        "Kb2",
                        "Kb3",    
                        "Kb4",    
            
            "user_id",
            "modified");
        $this->pkField_anamnesis = 'id_anamnesis';
        
        
        $this->fields_pemeriksaan_fisik = array(
            "id_pemeriksaan_fisik" => array("TIPE" => "Data", "LABEL" => "ID pemeriksaan fisik"),
            "id_pasien" => array("TIPE" => "Data", "LABEL" => "Data Pasien"),
//            "id_pasien" => array("TIPE" => "STRING", "LABEL" => "Data Pasien"),
            "kunjungan" => array("TIPE" => "STRING", "LABEL" => "Kunjungan"),
            "tgl_periksa" => array("TIPE" => "DATE", "LABEL" => "Tanggal Periksa"),
            "td" => array("TIPE" => "STRING", "LABEL" => "TD"),
            "nadi" => array("TIPE" => "STRING", "LABEL" => "Nadi"),
            "tb" => array("TIPE" => "STRING", "LABEL" => "Tinggi Badan"),
            "bb" => array("TIPE" => "STRING", "LABEL" => "Berat Badan"),
                        
            "ks1" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks2" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks3" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks4" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks5" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks6" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks7" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks8" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks9" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks10" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks11" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks12" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks13" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks14" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks15" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks16" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks17" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks18" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks19" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks20" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks21" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks22" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks23" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks24" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks25" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks26" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks27" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks28" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks29" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks30" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks31" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks32" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks33" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks34" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks35" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks36" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks37" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks38" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks39" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks40" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks41" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks42" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks43" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks44" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks45" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks46" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks47" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks48" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks49" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks50" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks51" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks52" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks53" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks54" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks55" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks56" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks57" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks58" => array("TIPE" => "STRING", "LABEL" => " "),
            "ks59" => array("TIPE" => "STRING", "LABEL" => " "),
            
            "a1" => array("TIPE" => "DATE", "LABEL" => " "),
            "a2" => array("TIPE" => "STRING", "LABEL" => " "),
            "a3" => array("TIPE" => "STRING", "LABEL" => " "),
            "a4" => array("TIPE" => "STRING", "LABEL" => " "),
            "a5" => array("TIPE" => "STRING", "LABEL" => " "),
            "a6" => array("TIPE" => "STRING", "LABEL" => " "),
            "a7" => array("TIPE" => "STRING", "LABEL" => " "),
            "a7a" => array("TIPE" => "STRING", "LABEL" => " "),
            "a8" => array("TIPE" => "STRING", "LABEL" => " "),
            "a9" => array("TIPE" => "DATE", "LABEL" => " "),
            "a10" => array("TIPE" => "STRING", "LABEL" => " "),
            "a11" => array("TIPE" => "STRING", "LABEL" => " "),
            "a12" => array("TIPE" => "STRING", "LABEL" => " "),
            "a13" => array("TIPE" => "STRING", "LABEL" => " "),
            "a14" => array("TIPE" => "STRING", "LABEL" => " "),
            "a15" => array("TIPE" => "STRING", "LABEL" => " "),
            "a16" => array("TIPE" => "STRING", "LABEL" => " "),
            "a17" => array("TIPE" => "DATE", "LABEL" => " "),
            "a18" => array("TIPE" => "STRING", "LABEL" => " "),
            "a19" => array("TIPE" => "STRING", "LABEL" => " "),
            "a20" => array("TIPE" => "STRING", "LABEL" => " "),
            "a21" => array("TIPE" => "DATE", "LABEL" => " "),
            "a22" => array("TIPE" => "STRING", "LABEL" => " "),
            "a23" => array("TIPE" => "STRING", "LABEL" => " "),
            "a24" => array("TIPE" => "STRING", "LABEL" => " "),
            "a25" => array("TIPE" => "DATE", "LABEL" => " "),
            "a26" => array("TIPE" => "STRING", "LABEL" => " "),
            "a27" => array("TIPE" => "STRING", "LABEL" => " "),
            "a28" => array("TIPE" => "STRING", "LABEL" => " "),
            "a29" => array("TIPE" => "DATE", "LABEL" => " "),
            "a30" => array("TIPE" => "STRING", "LABEL" => " "),
            "a31" => array("TIPE" => "STRING", "LABEL" => " "),
            "a32" => array("TIPE" => "STRING", "LABEL" => " "),
            "a33" => array("TIPE" => "DATE", "LABEL" => " "),
            "a34" => array("TIPE" => "STRING", "LABEL" => " "),
            "a35" => array("TIPE" => "STRING", "LABEL" => " "),
            "a36" => array("TIPE" => "STRING", "LABEL" => " "),
            "a37" => array("TIPE" => "DATE", "LABEL" => " "),
            "a38" => array("TIPE" => "STRING", "LABEL" => " "),
            "a39" => array("TIPE" => "STRING", "LABEL" => " "),
            "a40" => array("TIPE" => "STRING", "LABEL" => " "),
            "a41" => array("TIPE" => "DATE", "LABEL" => " "),
            "a42" => array("TIPE" => "STRING", "LABEL" => " "),
            "a43" => array("TIPE" => "STRING", "LABEL" => " "),
            "a44" => array("TIPE" => "STRING", "LABEL" => " "),
            "a45" => array("TIPE" => "STRING", "LABEL" => " "),
            "a46" => array("TIPE" => "STRING", "LABEL" => " "),
            "a47" => array("TIPE" => "STRING", "LABEL" => " "),
            "a48" => array("TIPE" => "DATE", "LABEL" => " "),
            "a49" => array("TIPE" => "STRING", "LABEL" => " "),
            "a50" => array("TIPE" => "STRING", "LABEL" => " "),
            "a51" => array("TIPE" => "STRING", "LABEL" => " "),
            "a52" => array("TIPE" => "DATE", "LABEL" => " "),
            "a53" => array("TIPE" => "STRING", "LABEL" => " "),
            "a54" => array("TIPE" => "STRING", "LABEL" => " "),
            "a55" => array("TIPE" => "STRING", "LABEL" => " "),
            
            "b1" => array("TIPE" => "STRING", "LABEL" => " "),
            "b2" => array("TIPE" => "DATE", "LABEL" => " "),
            "b3" => array("TIPE" => "STRING", "LABEL" => " "),
            "b4" => array("TIPE" => "STRING", "LABEL" => " "),
            "b5" => array("TIPE" => "STRING", "LABEL" => " "),
            "b6" => array("TIPE" => "DATE", "LABEL" => " "),
            "b7" => array("TIPE" => "DATE", "LABEL" => " "),
            "b8" => array("TIPE" => "STRING", "LABEL" => " "),
            "b9" => array("TIPE" => "STRING", "LABEL" => " "),
            "b10" => array("TIPE" => "DATE", "LABEL" => " "),
            "b11" => array("TIPE" => "DATE", "LABEL" => " "),
            "b12" => array("TIPE" => "STRING", "LABEL" => " "),
            "b13" => array("TIPE" => "STRING", "LABEL" => " "),
            "b14" => array("TIPE" => "STRING", "LABEL" => " "),
            "b15" => array("TIPE" => "DATE", "LABEL" => " "),
            "b16" => array("TIPE" => "DATE", "LABEL" => " "),
            "b17" => array("TIPE" => "STRING", "LABEL" => " "),
            "b18" => array("TIPE" => "STRING", "LABEL" => " "),
            "b19" => array("TIPE" => "STRING", "LABEL" => " "),
            "b20" => array("TIPE" => "STRING", "LABEL" => " "),
            "b21" => array("TIPE" => "STRING", "LABEL" => " "),
            "b22" => array("TIPE" => "STRING", "LABEL" => " "),
            "b23" => array("TIPE" => "STRING", "LABEL" => " "),
            "b24" => array("TIPE" => "STRING", "LABEL" => " "),
            "b25" => array("TIPE" => "STRING", "LABEL" => " "),
            "b26" => array("TIPE" => "DATE", "LABEL" => " "),
            "b27" => array("TIPE" => "STRING", "LABEL" => " "),
            "b28" => array("TIPE" => "STRING", "LABEL" => " "),
            "b29" => array("TIPE" => "STRING", "LABEL" => " "),
            "b30" => array("TIPE" => "STRING", "LABEL" => " "),
            "b31" => array("TIPE" => "STRING", "LABEL" => " "),
                        
            "user_id" => array("TIPE" => "STRING", "LABEL" => "User ID"),
            "modified" => array("TIPE" => "DATE", "LABEL" => "Modified"));
        $this->f_pemeriksaan_fisik = array(
            "id_pemeriksaan_fisik",
            "id_pasien",
            "kunjungan",
            "tgl_periksa",
            "td",
            "nadi",
            "tb",
            "bb",
            
            "ks1",
            "ks2",
            "ks3",
            "ks4",
            "ks5",
            "ks6",
            "ks7",
            "ks8",
            "ks9",
            "ks10",
            "ks11",
            "ks12",
            "ks13",
            "ks14",
            "ks15",
            "ks16",
            "ks17",
            "ks18",
            "ks19",
            "ks20",
            "ks21",
            "ks22",
            "ks23",
            "ks24",
            "ks25",
            "ks26",
            "ks27",
            "ks28",
            "ks29",
            "ks30",
            "ks31",
            "ks32",
            "ks33",
            "ks34",
            "ks35",
            "ks36",
            "ks37",
            "ks38",
            "ks39",
            "ks40",
            "ks41",
            "ks42",
            "ks43",
            "ks44",
            "ks45",
            "ks46",
            "ks47",
            "ks48",
            "ks49",
            "ks50",
            "ks51",
            "ks52",
            "ks53",
            "ks54",
            "ks55",
            "ks56",
            "ks57",
            "ks58",
            "ks59",
            
            "a1",
            "a2",
            "a3",
            "a4",
            "a5",
            "a6",
            "a7",
            "a7a",
            "a8",
            "a9",
            "a10",
            "a11",
            "a12",
            "a13",
            "a14",
            "a15",
            "a16",
            "a17",
            "a18",
            "a19",
            "a20",
            "a21",
            "a22",
            "a23",
            "a24",
            "a25",
            "a26",
            "a27",
            "a28",
            "a29",
            "a30",
            "a31",
            "a32",
            "a33",
            "a34",
            "a35",
            "a36",
            "a37",
            "a38",
            "a39",
            "a40",
            "a41",
            "a42",
            "a43",
            "a44",
            "a45",
            "a46",
            "a47",
            "a48",
            "a49",
            "a50",
            "a51",
            "a52",
            "a53",
            "a54",
            "a55",
                
            "b1",
            "b2",
            "b3",
            "b4",
            "b5",
            "b6",
            "b7",
            "b8",
            "b9",
            "b10",
            "b11",
            "b12",
            "b13",
            "b14",
            "b15",
            "b16",
            "b17",
            "b18",
            "b19",
            "b20",
            "b21",
            "b22",
            "b23",
            "b24",
            "b25",
            "b26",
            "b27",
            "b28",
            "b29",
            "b30",
            "b31",
            
            "user_id",
            "modified"
        );
        $this->pkField_pemeriksaan_fisik = 'id_pemeriksaan_fisik';
        
    }
    
    public function upload() {
        //$dir = date('Ym');
    	$lokasi = "./files/import/";
    	$id = 'fl';
        //$i=0;
    	if (($_FILES[$id]["size"] < 50000000)) {
            if ($_FILES[$id]["error"] > 0) {
                echo json_encode(array('msg' => "Return Code: " . $_FILES[$id]["error"] . "<br>"));
            } else {
                move_uploaded_file($_FILES[$id]["tmp_name"], $lokasi . $_FILES[$id]["name"]); 
                echo json_encode(array('success' => true));               
            } 
            $import = $this->import($_FILES[$id]["name"]);
            /*if ($import != "") {
                echo json_encode($import);
            }else{
                echo json_encode(array('success' => true));
            }*/
    	} else {
            echo json_encode(array('msg' => "File terlalu besar."));
    	}
    }
    
    public function import($file){
        $file = './files/import/'.$file;
        $this->load->library('excel');
        $objPHPExcel = PHPExcel_IOFactory::load($file);
        //$total=0;
        //$update=0;
        //$insert=0;
        //$skip=0;
        
        $pasien = array();
        foreach($objPHPExcel->getSheetByName('Pasien')->getRowIterator() as $row){
            $iterator = $row->getCellIterator();
            $iterator->setIterateOnlyExistingCells(false);
            $temp = array();
            foreach($iterator as $cell){
                if($cell->getCalculatedValue() == NULL){
                    $dt = '';
                }else{
                    $dt = $cell->getCalculatedValue();
                }
                array_push($temp,$dt);
            }
                //$total++;
            array_push($pasien,$temp);
        }
        //print_r($pasien);
        
        foreach($pasien as $val){
            $id = current($val);
            $modified = end($val);
            $cond = array ('id_pasien'=>$id,'modified'=>$modified);
            $check = $this->model_pasien->cek('id_pasien',$id);
            $val = array_combine($this->f_pasien, $val);
            if($check){
                $check = $this->model_pasien->cek($cond,$cond);
                if($check){
                   // $skip++;
                }else{
                    $result = $this->save('m_pasien_model', "ubah", $val, $this->fields_pasien, $this->pkField_pasien, $id, $this->uniqueFields, $this->removeAdd, $this->convert);
                    //$update++;
                }
            }else{
                $result = $this->save('m_pasien_model', "tambah", $val, $this->fields_pasien, $this->pkField_pasien, "", $this->uniqueFields, $this->removeAdd, $this->convert);
                //$insert++;
            }
        }
        
        $anamnesis = array();
        foreach($objPHPExcel->getSheetByName('Anamnesis')->getRowIterator() as $row){
            $iterator = $row->getCellIterator();
            $iterator->setIterateOnlyExistingCells(false);
            $temp = array();
            foreach($iterator as $cell){
                if($cell->getCalculatedValue() == NULL){
                    $dt = '';
                }else{
                    $dt = $cell->getCalculatedValue();
                }
                array_push($temp,$dt);
            }
                //$total++;
            array_push($anamnesis,$temp);
        }
        //print_r($anamnesis);
        
        foreach($anamnesis as $val){
            $id = current($val);
            $modified = end($val);
            $cond = array ('id_anamnesis'=>$id,'modified'=>$modified);
            $check = $this->model_anamnesis->cek('id_anamnesis',$id);
            $val = array_combine($this->f_anamnesis, $val);
            if($check){
                $check = $this->model_anamnesis->cek($cond,$cond);
                if($check){
                    //$skip++;
                }else{
                    $result = $this->save('p_anamnesis_model', "ubah", $val, $this->fields_anamnesis, $this->pkField_anamnesis, $id, $this->uniqueFields, $this->removeAdd, $this->convert);
                    //$update++;
                }
            }else{
                $result = $this->save('p_anamnesis_model', "tambah", $val, $this->fields_anamnesis, $this->pkField_anamnesis, "", $this->uniqueFields, $this->removeAdd, $this->convert);
                //$insert++;
            }
        }
        
        $pemeriksaan_fisik = array();
        foreach($objPHPExcel->getSheetByName('Pemeriksaan Fisik')->getRowIterator() as $row){
            $iterator = $row->getCellIterator();
            $iterator->setIterateOnlyExistingCells(false);
            $temp = array();
            foreach($iterator as $cell){
                if($cell->getCalculatedValue() == NULL){
                    $dt = '';
                }else{
                    $dt = $cell->getCalculatedValue();
                }                
                array_push($temp,$dt);
            }
                //$total++;
            array_push($pemeriksaan_fisik,$temp);
        }
        //print_r($pemeriksaan_fisik);
        
        foreach($pemeriksaan_fisik as $val){
            $id = current($val);
            $modified = end($val);
            $cond = array ('id_pemeriksaan_fisik'=>$id,'modified'=>$modified);
            $check = $this->model_pemeriksaan_fisik->cek('id_pemeriksaan_fisik',$id);
            //print_r($this->f_pemeriksaan_fisik);
            //print_r($val);
            $val = array_combine($this->f_pemeriksaan_fisik, $val);
            if($check){
                $check = $this->model_pemeriksaan_fisik->cek($cond,$cond);
                if($check){
                   // $skip++;
                }else{
                    $result = $this->save('p_pemeriksaan_fisik_model', "ubah", $val, $this->fields_pemeriksaan_fisik, $this->pkField_pemeriksaan_fisik, $id, $this->uniqueFields, $this->removeAdd, $this->convert);
                   // $update++;
                }
            }else{
                $result = $this->save('p_pemeriksaan_fisik_model', "tambah", $val, $this->fields_pemeriksaan_fisik, $this->pkField_pemeriksaan_fisik, "", $this->uniqueFields, $this->removeAdd, $this->convert);
               // $insert++;
            }
        }
        //echo json_encode(array('total'=>$total,'insert'=>$insert,'update'=>$update,'skip'=>$skip));
        $msg = "";
        if ($msg != "") {
            return $msg;
        } else {
            return true;
        }
    }
    
}