<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Laporan extends MY_Controller {

    function __construct() {
        parent::__construct();
        $mdl = "l_laporan";
        $this->model_name = $mdl;
        $this->load->model($mdl);
        $this->model = $this->$mdl;
        $this->pkField = "id_pasien";
        $this->uniqueFields = array("id_pasien");

        //pair key value (field => TYPE)
        //TYPE: EMAIL/STRING/INT/FLOAT/BOOLEAN/DATE/PASSWORD/URL/IP/MAC/RAW/DATA(TYPE,LABEL,MODEL,FIELD)
//        $this->fields = array(
//            "nama" => array("TIPE" => "STRING", "LABEL" => "Nama"),
//            "keterangan" => array("TIPE" => "STRING", "LABEL" => "Keterangan"),
//            "status" => array("TIPE" => "STRING", "LABEL" => "Status")
//        );
    }

    public function index() {
        
    }
    
    public function cetak() {
        //echo "id_pasien".$this->input->post('id_pasien');
        $condition = array("m_pasien.id_pasien" => $this->input->get('p'),
                    "an.kunjungan" => $this->input->get('k'),
                    "pf.kunjungan" => $this->input->get('k')
                    );
        $this->load->library('Pdf');

        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetFont('helvetica', '', 12);
        $pdf->SetTitle('Laporan Pemeriksaan');
        $pdf->SetHeaderMargin(10);
        $pdf->SetTopMargin(10);
        $pdf->setFooterMargin(20);
        $pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('RSCM');
        $pdf->SetDisplayMode('real', 'default');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(15, 15, 15);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        
        $pdf->SetPrintHeader(false);
        //$pdf->SetPrintFooter(false);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        

        // add a page
        $pdf->AddPage();

        $pdf->Image('./asset/img/logo.png','','','30','','','','T');
        $pdf->Image('./asset/img/innogene.png','','','30','','','','N','','','R');
        $pdf->Cell('','','DATA REGISTRY','0','1','C','','','','');
        $pdf->Cell('','','KANKER THT-KEPALA LEHER','0','1','C','','','','');
        $pdf->Cell('','','KELOMPOK STUDI ONKOLOGI BEDAH KEPALA LEHER','0','1','C','','','','');
        $pdf->Cell('','','INDONESIA','0','1','C','','','','');
        $pdf->Cell('','20','','0','1','C','','','','');


        $result = $this->model->get(NULL,$condition);
        //print_r($result);
        //if(sizeof($result) != 0){
        foreach ($result as $row) {
            $tgl_lahir = date_format(date_create($row->tglLahir),"d/m/Y");
        $bag1 = <<<EOD
        <table width="100%">
                <tr>
                    <td width="20%">NAMA</td>
                    <td width="5%">:</td>
                    <td width="25%">{$row->nama_pasien}</td>
                    <td width="20%">NO REKAM MEDIS</td>
                    <td width="5%">:</td>
                    <td width="25%">{$row->noRekamMedis}</td>
                </tr>
                <tr>
                    <td>NAMA ORANG TUA</td>
                    <td>:</td>
                    <td>{$row->namaOrangTua}</td>
                    <td>TANGGAL LAHIR</td>
                    <td>:</td>
                    <td>{$this->tgl($row->tglLahir)}</td>
                </tr>
                <tr>
                    <td>USIA</td>
                    <td>:</td>
                    <td>{$row->usia}</td>
                    <td>AGAMA</td>
                    <td>:</td>
                    <td>{$row->agama}</td>
                </tr>
                <tr>
                    <td>PENDIDIKAN</td>
                    <td>:</td>
                    <td>{$row->pendidikan}</td>
                    <td>DAERAH</td>
                    <td>:</td>
                    <td>{$row->daerah}</td>
                </tr>
                <tr>
                    <td>TEMPAT KELAHIRAN</td>
                    <td>:</td>
                    <td>{$row->pendidikan}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>PEKERJAAN</td>
                    <td>:</td>
                    <td>{$row->pekerjaan}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>BANGSA</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Asing berasal dari</td>
                    <td>:</td>
                    <td>{$row->asing}</td>
                </tr>    
                <tr>    
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Indonesia asli/ suku</td>
                    <td>:</td>
                    <td>{$row->suku}</td>
                </tr>    
                <tr>    
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Indonesia keturunan</td>
                    <td>:</td>
                    <td>{$row->keturunan}</td>
                </tr>    
                <tr>    
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;Indonesia keturunan</td>
                    <td>:</td>
                    <td>{$row->keturunan}</td>
                </tr>     
                <tr>    
                    <td colspan="6"></td>
                </tr>    
                <tr>    
                    <td colspan="6">TEMPAT TINGGAL DI JAKARTA</td>
                </tr>    
                <tr>    
                    <td colspan="6">Jalan : {$row->jJalan}</td>
                </tr>    
                <tr>    
                    <td colspan="6">RT : {$row->jRt} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RW : {$row->jRw} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No : {$row->jNo} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Telp : {$row->jTelp} &nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>    
                <tr>    
                    <td colspan="6">Kelurahan : {$row->jKelurahan}</td>
                </tr>       
                <tr>    
                    <td colspan="6">Kecamatan : {$row->jKecamatan}</td>
                </tr>     
                <tr>    
                    <td colspan="6"></td>
                </tr>    
                <tr>    
                    <td colspan="6">TEMPAT TINGGAL ASAL</td>
                </tr>    
                <tr>    
                    <td colspan="6">Jalan : {$row->aJalan}</td>
                </tr>    
                <tr>    
                    <td colspan="6">RT : {$row->aRt} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RW : {$row->aRw} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No : {$row->aNo} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Telp : {$row->aTelp} &nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>    
                <tr>    
                    <td colspan="6">Kelurahan : {$row->aKelurahan}</td>
                </tr>       
                <tr>    
                    <td colspan="6">Kecamatan : {$row->aKecamatan}</td>
                </tr>         
                <tr>    
                    <td colspan="6">Kabupaten : {$row->aKabupaten}</td>
                </tr>         
                <tr>    
                    <td colspan="6">Provinsi : {$row->aProvinsi}</td>
                </tr>            
        </table> 
EOD;
        
        $pdf->SetFont('helvetica', '', 10);
        $pdf->writeHTML($bag1, true, false, true, false, '');
        $pdf->AddPage();
        
        $arrRiwayat = array("GERD"=>$row->Eg1,
            "Barrett"=>$row->Eg2,
            "Achalasia"=>$row->Eg3,
            "Konsumsi Zat Korosif"=>$row->Eg4,
            $row->EgLainnya=>$row->Eg5);
        
        $arrJenisOperasi = array("Maksiletomi "=>$row->b22a,
            "Parotidektomi"=>$row->b22b,
            "Mandibulektomi"=>$row->b22c,
            "Laringektomi"=>$row->b22d,
            "Hemiglosektomi"=>$row->b22e,
            "Tiroidektomi"=>$row->b22f,
            "Endoskopi approach"=>$row->b22f);
        
        $bag2 = <<<EOD
        <table width="100%">
            <tr>
                <td colspan="3"><b>Kunjungan Ke : {$row->kunjungan_anamnesis}</b></td>
            </tr>        
            <tr>
                <td colspan="3"><b>Tanggal Pemeriksaan : {$this->tgl($row->tgl_periksa_anamnesis)}</b></td>
            </tr>
            <tr>
                <td colspan="3"><b>ANAMNESIS:</b></td>
            </tr>
            <tr>
                <td colspan="3"><b>KARSINOMA NASOFARING</b></td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Benjolan Leher</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Aa1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Aa2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Aa3)}</td>
            </tr>                
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Hidung tersumbat </td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ab1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ab2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ab3)}</td>
            </tr>                
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Ingus/ lendir bercampur darah </td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ac1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ac2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ac3)}</td>
            </tr>               
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Telinga terasa penuh </td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ad1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ad2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ad3)}</td>
            </tr>               
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Post nasal drip</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ae1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ae2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ae3)}</td>
            </tr>              
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Penglihatan dobel</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Af1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Af2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Af3)}</td>
            </tr>                             
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Gangguan dengar unit</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ag1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ag2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ag3)}</td>
            </tr>              
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Gangguan dengar bilat</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ah1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ah2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ah3)}</td>
            </tr>             
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Telinga berdengung</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ai1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ai2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ai3)}</td>
            </tr>             
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Nyeri telinga</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Aj1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Aj2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Aj3)}</td>
            </tr>             
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Cairan telinga</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ak1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ak2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ak3)}</td>
            </tr>            
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Sakit kepala unilateral</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Al1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Al2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Al3)}</td>
            </tr>           
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Parese n. kranial</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Am1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Am2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Am3)}</td>
            </tr>                
            
            <tr>
                <td colspan="3"><b>TUMOR KAVUM NASI</b></td>
            </tr>          
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Bengkak  hidung lateral </td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ba1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ba2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ba3)}</td>
            </tr>                          
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Rasa baal di pipi/ gigi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Bb1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Bb2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Bb3)}</td>
            </tr>                          
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Benjolan di samping mata</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Bc1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Bc2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Bc3)}</td>
            </tr>                         
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Gangguan penciuman</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Bd1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Bd2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Bd3)}</td>
            </tr>                        
            
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%"></td>
                <td width="5%"></td>
                <td width="65%"></td>
            </tr>                        
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%"></td>
                <td width="5%"></td>
                <td width="65%"></td>
            </tr>                      
                
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Benjolan di palatum</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Be1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Be2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Be3)}</td>
            </tr>                       
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Nyeri pipi/ gigi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Bf1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Bf2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Bf3)}</td>
            </tr>                      
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Mata menonjol/ buta</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Bg1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Bg2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Bg3)}</td>
            </tr>                     
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Muka mencong</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Bh1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Bh2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Bh3)}</td>
            </tr>                    
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Suara sengau</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Bi1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Bi2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Bi3)}</td>
            </tr>                    
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Gigi nyeri/ goyah</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Bj1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Bj2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Bj3)}</td>
            </tr>            
                
            <tr>
                <td colspan="3"><b>TUMOR ORAL CAVITY</b></td>
            </tr>    
            <tr>
                <td colspan="5">(lidah, bibir, pipi, gusi, palatum, dasar mulut, trigonum. Retromolar)</td>
            </tr>         
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Sariawan sukar sembuh</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ca1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ca2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ca3)}</td>
            </tr>        
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Perdarahan lokal</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Cb1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Cb2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Cb3)}</td>
            </tr>        
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Kesulitan menelan</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Cc1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Cc2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Cc3)}</td>
            </tr>        
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Nyeri pada lesi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Cd1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Cd2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Cd3)}</td>
            </tr>       
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Sulit berbicara</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ce1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ce2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ce3)}</td>
            </tr>       
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Luka bergaung</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Cf1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Cf2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Cf3)}</td>
            </tr>            
                
            <tr>
                <td colspan="3"><b>TUMOR OROFARING</b></td>
            </tr>    
            <tr>
                <td colspan="5">(parafaring, hipofaring, tonsil, dasar lidah, dinding faring)</td>
            </tr>         
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Gerak lidah terbatas</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Da1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Da2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Da3)}</td>
            </tr>        
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Berat badan turun</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Db1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Db2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Db3)}</td>
            </tr>        
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Nyeri menelan</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Dc1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Dc2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Dc3)}</td>
            </tr>        
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Hot potato voice </td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Dd1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Dd2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Dd3)}</td>
            </tr>       
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Mulut berbau</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->De1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->De2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->De3)}</td>
            </tr>
                       
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Sukar buka mulut</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Df1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Df2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Df3)}</td>
            </tr>            
                
            <tr>
                <td colspan="3"><b>ANGIOFIBROMA NASOFARING</b></td>
            </tr>           
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Hidung tersumbat</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ea1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ea2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ea3)}</td>
            </tr>           
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Mimisan</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Eb1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Eb2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Eb3)}</td>
            </tr>           
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Sakit kepala</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ec1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ec2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ec3)}</td>
            </tr>           
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Pembengkakan hidung</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ed1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ed2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ed3)}</td>
            </tr>            
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Sesak napas</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ee1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ee2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ee3)}</td>
            </tr>             
            
            <tr>
                <td colspan="5"></td>
            </tr>    
            <tr>
                <td colspan="5"><b>Pertama kali kedokter:</b> {$this->kedokter($row->Ef1)}</td>
            </tr>
            <tr>
                <td colspan="5"><b>Dokter</b> : {$this->jdokter($row->Ef2,$row->Ef2Lainnya)}</td>
            </tr>
            <tr>
                <td colspan="5"><b>Riwayat penyakit dahulu:</b> {$this->riwayat($arrRiwayat)}</td>
            </tr>
            <tr>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td colspan="5"><b>Tumor Laring:</b></td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Suara Serak</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Fa1)}</td>
            </tr>  
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Fa2)}</td>
            </tr> 
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Nafas Bunyi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Fb1)}</td>
            </tr>  
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Fb2)}</td>
            </tr> 
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Sesak Nafas</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Fc1)}</td>
            </tr>  
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Fc2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Nyeri Menelan</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Fd1)}</td>
            </tr>  
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Fd2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Sulit Menelan</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Fe1)}</td>
            </tr>  
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Fe2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Tersedak</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ff1)}</td>
            </tr>  
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ff2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Benjolan Pada Leher</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Fg1)}</td>
            </tr>  
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Fg2)}</td>
            </tr>
            <tr>
                <td colspan="5"></td>
            </tr>
            <tr>
                <td colspan="5"><b>Tumor Parotis:</b></td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Benjolan Pada Ujung Bawah Telinga</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ga1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ga2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ga3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Wajah Mencong</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Gb1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Gb2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Gb3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Demam</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Gc1)}</td>
            </tr>  
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Ludah Bercampur nanah</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Gd1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Gd2)}</td>
            </tr>
            <tr>
                <td colspan="5"></td>
            </tr> 
            <tr>
                <td colspan="5"><b>Tumor Telinga:</b></td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Benjolan di Telinga</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ha1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ha2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ha3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Keluar Cairan di Telinga</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Hb1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Hb2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Hb3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Penurunan Pendengaran</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Hc1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Hc2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Hc3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Telinga Terasa Penuh</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Hd1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Hd2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Hd3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Telinga Berbunyi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->He1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->He2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->He3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Pusing Berputar</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Hf1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Hf2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Hf3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Gangguan Keseimbangan</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Hg1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Hg2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Hg3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Muka Mencong</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Hh1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Hh2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Hh3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Nyeri Telinga</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Hi1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Hi2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Hi3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Keluar Darah Dari Telinga</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Hj1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Hj2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Hj3)}</td>
            </tr>
            <tr>
                <td colspan="5"></td>
            </tr> 
            <tr>
                <td colspan="5"><b>Tumor Esofagus:</b></td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Benjolan Leher</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ia1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ia2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ia3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Disfagia</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ib1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ib2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ib3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Penurunan Berat Badan</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ic1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ic2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ic3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Muntah Darah</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Id1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Id2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Id3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Nyeri Dada / Perut </td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ie1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ie2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ie3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Gangguan Pencernaan</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->If1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->If2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->If3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Batuk Kronis</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ig1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ig2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ig3)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%">-</td>
                <td width="30%">Suara Serak</td>
                <td width="5%">:</td>
                <td width="65%">{$this->yn($row->Ih1)}</td>
            </tr>   
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lokasi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lokasi($row->Ih2)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td width="5%"></td>
                <td width="30%">Lama</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lama($row->Ih3)}</td>
            </tr>
            <tr>
                <td colspan="5"></td>
            </tr> 
            <tr>
                <td colspan="5"><b>Riwayat keluarga:</b></td>
            </tr>
            <tr>
                <td colspan="5">Kanker di keluarga: {$this->yn($row->Ja1)}</td>
            </tr>
            <tr>
                <td colspan="5">Jika ya, sebutkan jenis kanker: {$this->jKanker($row->Ja2,$row->Ja2Lainnya)}</td>
            </tr>
            <tr>
                <td colspan="5">Jika ya, sebutkan jenis kanker: {$this->keluarga($row->Ja3,$row->Ja3Lainnya)}</td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
                
            <tr>
                <td colspan="3"><b>Faktor Risiko : </b></td>
            </tr>
            <tr>
                <td colspan="3">Konsumsi alkohol : {$this->yn($row->Ka1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td colspan="2" width="35%">Frekuensi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->frekuensi($row->Ka2)}</td>
            </tr>                
            <tr>
                <td width="5%"></td>
                <td colspan="2" width="35%">Lamanya</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lamanya($row->Ka3)}</td>
            </tr>                
            <tr>
                <td width="5%"></td>
                <td colspan="2" width="35%">Jumlah</td>
                <td width="5%">:</td>
                <td width="65%">{$this->jmAlkohol($row->Ka4)}</td>
            </tr>
            <tr>
                <td colspan="3">Riwayat merokok : {$this->yn($row->Kb1)}</td>
            </tr>
            <tr>
                <td width="5%"></td>
                <td colspan="2" width="35%">Frekuensi</td>
                <td width="5%">:</td>
                <td width="65%">{$this->frekuensi($row->Kb2)}</td>
            </tr>                
            <tr>
                <td width="5%"></td>
                <td colspan="2" width="35%">Lamanya</td>
                <td width="5%">:</td>
                <td width="65%">{$this->lamanya($row->Kb3)}</td>
            </tr>                
            <tr>
                <td width="5%"></td>
                <td colspan="2" width="35%">Jumlah</td>
                <td width="5%">:</td>
                <td width="65%">{$this->jmRokok($row->Kb4)}</td>
            </tr>
        </table>            
EOD;
        $pdf->SetFont('helvetica', '', 10);
        $pdf->writeHTML($bag2, true, false, true, false, '');
        $pdf->AddPage();
        
        
        $bag3 = <<<EOD
        <table width="100%">
            <tr>
                <td><b>PEMERIKSAAN FISIK : </b></td>
            </tr>
            <tr>
                <td>Tanggal Pemeriksaan : {$this->tgl($row->tgl_pemeriksaan_fisik)}</td>
            </tr>
            <tr>
                <td>Tekanan Darah : {$row->td} mmHg &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nadi: {$row->nadi} x/menit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tinggi Badan:{$row->tb} cm &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Berat Badan:{$row->bb} kg &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            </tr>
            <tr>
                <td>Karnofsky Scale : {$this->karnofsky($row->ks1)}</td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
        <br>
        <table widht="100%">
            <tr>
                <td><b>Nasoendoskopi</b></td>
            </tr>
        </table>
        <table width="100%" border="1">
            <tr>
                <td></td>
                <td> Hidung Kanan</td>
                <td> Hidung Kiri</td>
            </tr>
            <tr>
                <td> Kavum nasi</td>
                <td> {$this->naso($row->ks2)}</td>
                <td> {$this->naso($row->ks3)}</td>
            </tr>
            <tr>
                <td> Konka inferior</td>
                <td> {$this->naso($row->ks4)}</td>
                <td> {$this->naso($row->ks5)}</td>
            </tr>
            <tr>
                <td> Konka media</td>
                <td> {$this->naso($row->ks6)}</td>
                <td> {$this->naso($row->ks7)}</td>
            </tr>
            <tr>
                <td> Meatus media, Koana,</td>
                <td> {$this->naso($row->ks8)}</td>
                <td> {$this->naso($row->ks9)}</td>
            </tr>
            <tr>
                <td> Torus tubarius</td>
                <td> {$this->naso($row->ks10)}</td>
                <td> {$this->naso($row->ks11)}</td>
            </tr>
            <tr>
                <td> Fossa Rossenmuller</td>
                <td> {$this->naso($row->ks12)}</td>
                <td> {$this->naso($row->ks13)}</td>
            </tr>
            <tr>
                <td> Atap Nasofaring</td>
                <td> {$this->naso($row->ks14)}</td>
                <td> {$this->naso($row->ks15)}</td>
            </tr>
            <tr>
                <td> Hipofaring</td>
                <td> {$this->naso($row->ks16)}</td>
                <td> {$this->naso($row->ks17)}</td>
            </tr>
            <tr>
                <td> Supra Glotis</td>
            </tr>
            <tr>
                <td> &nbsp;&nbsp; - Plica Ariepiglotika</td>
                <td> {$this->naso($row->ks18)}</td>
                <td> {$this->naso($row->ks19)}</td>
            </tr>
            <tr>
                <td> &nbsp;&nbsp; - Aritenoid</td>
                <td> {$this->naso($row->ks20)}</td>
                <td> {$this->naso($row->ks21)}</td>
            </tr>
            <tr>
                <td> &nbsp;&nbsp; - Plica Ventrikularis</td>
                <td> {$this->naso($row->ks22)}</td>
                <td> {$this->naso($row->ks23)}</td>
            </tr>
            <tr>
                <td> Glotis</td>
                <td> {$this->naso($row->ks24)}</td>
                <td> {$this->naso($row->ks25)}</td>
            </tr>
            <tr>
                <td> Sub Glotis</td>
                <td> {$this->naso($row->ks26)}</td>
                <td> {$this->naso($row->ks27)}</td>
            </tr>
            <tr>
                <td> Rima</td>
                <td> {$this->naso2($row->ks28)}</td>
                <td> {$this->naso2($row->ks29)}</td>
            </tr>
        </table>
        <br>
        <table widht="100%">
            <tr>
                <td colspan="2"></td>
            </tr> 
            <tr>
                <td><b>Tenggorok</b></td>
            </tr>
        </table>
        <table width="100%" border="1">
            <tr>
                <td> </td>
                <td> Kanan</td>
                <td> Kiri</td>
            </tr>
            <tr>
                <td> Tonsil</td>
                <td> {$this->tonsil($row->ks30)}</td>
                <td> {$this->tonsil($row->ks31)}</td>
            </tr>
            <tr>
                <td> Palatum molle</td>
                <td> {$this->molle($row->ks32)}</td>
                <td> {$this->molle($row->ks33)}</td>
            </tr>
            <tr>
                <td> Palatum durum</td>
                <td> {$this->durum($row->ks34)}</td>
                <td> {$this->durum($row->ks35)}</td>
            </tr>
            <tr>
                <td> Lidah</td>
                <td> {$this->lidah($row->ks36)}</td>
                <td> {$this->lidah($row->ks37)}</td>
            </tr>
            <tr>
                <td> Dasar mulut</td>
                <td> {$this->mulut($row->ks38)}</td>
                <td> {$this->mulut($row->ks39)}</td>
            </tr>
            <tr>
                <td> Pangkal Lidah</td>
                <td> {$this->lidah($row->ks40)}</td>
                <td> {$this->lidah($row->ks41)}</td>
            </tr>
            <tr>
                <td> Gigi geligi</td>
                <td> {$this->geligi($row->ks42)}</td>
                <td> {$this->geligi($row->ks43)}</td>
            </tr>
        </table>
        <table>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2"><b>Telinga : </b></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp; - Membran Timpani : {$this->telingaTimpani($row->ks55)}</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp; - Jenis Perforasi : {$this->telingaJenis($row->ks56)}</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp; - Sekret : {$this->telingaSekret($row->ks57)}</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp; - Parese N VII : {$this->telingaParese($row->ks58)}</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Jika Perrifer : {$this->telingaParese2($row->ks59)}</td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2"><b>Parotis : </b>{$this->parotis($row->ks44)}</td>
            </tr>
            <tr>
                <td colspan="2">Bentuk Tumor Parotis :{$row->ks45}</td>
            </tr>
            <tr>
                <td colspan="2">Ukuran tumor :{$row->ks46}</td>
            </tr>
            <tr>
                <td colspan="2">Parese Fasial :{$this->yn($row->ks47)}</td>
            </tr>
            <tr>
                <td colspan="2">Jika Ya :{$this->lokasi($row->ks48)}</td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2"><b>Tiroid : </b>{$this->parotis($row->ks49)}</td>
            </tr>
            <tr>
                <td colspan="2">Ukuran tumor :{$row->ks50}</td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2"><b>Kelenjar Getah Bening :  </b>{$this->getah($row->ks51)}</td>
            </tr>
            <tr>
                <td colspan="2">Ukuran (dalam cm) :{$row->ks52}</td>
            </tr>
            <tr>
                <td colspan="2">Deskripsi KGB :{$this->dKGB($row->ks53)}</td>
            </tr>
            <tr>
                <td colspan="2">Lokasi KGB :{$this->lKGB($row->ks54)}</td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="2"><b>PEMERIKSAAN PENUNJANG </b></td>
            </tr>
            <tr>
                <td width="5%">1.</td>
                <td>BIOPSI</td>
            </tr>
            <tr>
                <td></td>
                <td>Tanggal: {$this->tgl($row->a1)} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RS: {$row->a2} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No.PA: {$row->a3} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Hasil: {$row->a4}</td>
            </tr>
            <tr>
                <td></td>
                <td>Karsinoma nasofaring (C11.9)</td>
            </tr>
            <tr>
                <td></td>
                <td>WHO : {$this->who($row->a5)}</td>
            </tr>
            <tr>
                <td></td>
                <td>WF : {$this->wf($row->a6)}</td>
            </tr>                
            <tr>
                <td></td>
                <td>Sinus paranasal (C 31.9) : {$this->parnasal($row->a7,$row->a7a)}</td>
            </tr>               
            <tr>
                <td></td>
                <td>Laring (Karsinoma sel skuamosa- C32.9) : {$this->laring($row->a8)}</td>
            </tr>
            
            <tr>
                <td width="5%">2.</td>
                <td>FNAB</td>
            </tr>
            <tr>
                <td></td>
                <td>Tanggal: {$this->tgl($row->a9)} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RS: {$row->a10} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No.PA: {$row->a11}</td>
            </tr>               
            <tr>
                <td></td>
                <td>Lokasi Lesi :</td>
            </tr>               
            <tr>
                <td></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tiroid : {$this->lokasi($row->a12)}</td>
            </tr>              
            <tr>
                <td></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Parotis : {$this->lokasi($row->a13)}</td>
            </tr>               
            <tr>
                <td></td>
                <td>Lokasi KGB : {$this->lokasi($row->a14)}, {$this->lKGB($row->a15)}</td>
            </tr>               
            <tr>
                <td></td>
                <td>Hasil : {$row->a16}</td>
            </tr>
                
            <tr>
                <td width="5%">3.</td>
                <td>Imaging</td>
            </tr>              
            <tr>
                <td></td>
                <td>- CT Scan</td>
            </tr>
            <tr>
                <td></td>
                <td>Tanggal: {$this->tgl($row->a17)} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RS: {$row->a18} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No: {$row->a19}</td>
            </tr>               
            <tr>
                <td></td>
                <td>- MRI</td>
            </tr>
            <tr>
                <td></td>
                <td>Tanggal: {$this->tgl($row->a21)} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RS: {$row->a22} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No: {$row->a23}</td>
            </tr>               
            <tr>
                <td></td>
                <td>- Rontgen thorax</td>
            </tr>
            <tr>
                <td></td>
                <td>Tanggal: {$this->tgl($row->a25)} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RS: {$row->a26} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No: {$row->a27}</td>
            </tr>                
            <tr>
                <td></td>
                <td>- USG abdomen</td>
            </tr>
            <tr>
                <td></td>
                <td>Tanggal: {$this->tgl($row->a29)} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RS: {$row->a30} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No: {$row->a31}</td>
            </tr>               
            <tr>
                <td></td>
                <td>- Bone scan</td>
            </tr>
            <tr>
                <td></td>
                <td>Tanggal: {$this->tgl($row->a33)} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RS: {$row->a34} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No: {$row->a35}</td>
            </tr>              
            <tr>
                <td></td>
                <td>- USG leher</td>
            </tr>
            <tr>
                <td></td>
                <td>Tanggal: {$this->tgl($row->a37)} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RS: {$row->a38} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No: {$row->a39}</td>
            </tr>               
            <tr>
                <td></td>
                <td>- USG tiroid</td>
            </tr>
            <tr>
                <td></td>
                <td>Tanggal: {$this->tgl($row->a41)} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RS: {$row->a42} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No: {$row->a43}</td>
            </tr> 
            <tr>
                <td colspan="2"></td>
            </tr>                
            <tr>
                <td></td>
                <td>Hasil masing-masing : {$row->a45}</td>
            </tr>                
            <tr>
                <td></td>
                <td>Metastase jauh : {$this->yn($row->a46)}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Lokasi : {$this->lokasi2($row->a47)}</td>
            </tr>        
            <tr>
                <td colspan="2"></td>
            </tr> 
            <tr>
                <td colspan="2"><b>Audiometri Nada Murni :</b></td>
            </tr>                  
            <tr>
                <td></td>
                <td>Tanggal : {$this->tgl($row->a48)}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Rumah Sakit : {$row->a49}</td>
            </tr>                 
            <tr>
                <td></td>
                <td>Hasil : </td>
            </tr>               
            <tr>
                <td></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp; Telinga Kanan : {$this->hasil($row->a50)}</td>
            </tr>               
            <tr>
                <td></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp; Telinga Kiri : {$this->hasil($row->a51)}</td>
            </tr>      
            <tr>
                <td colspan="2"></td>
            </tr> 
            <tr>
                <td colspan="2"><b>Timpanometri :</b></td>
            </tr>                  
            <tr>
                <td></td>
                <td>Tanggal : {$this->tgl($row->a52)}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Rumah Sakit : {$row->a53}</td>
            </tr>                 
            <tr>
                <td></td>
                <td>Hasil : </td>
            </tr>               
            <tr>
                <td></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp; Telinga Kanan : {$this->hasil($row->a54)}</td>
            </tr>               
            <tr>
                <td></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp; Telinga Kiri : {$this->hasil($row->a55)}</td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>        
            <tr>
                <td colspan="2"><b>DIAGNOSA</b></td>
            </tr>        
            <tr>
                <td colspan="2"><b>TUMOR MEETING : {$this->yn($row->b1)}</b></td>
            </tr>                              
            <tr>
                <td colspan="2"><b>Stadium : {$this->stadium($row->b31)}</b></td>
            </tr>
            <tr>
                <td colspan="2"><b>Tangal tumor meeting dilakukan : {$this->tgl($row->b2)}</b></td>
            </tr>         
            <tr>
                <td colspan="2"></td>
            </tr>          
            <tr>
                <td colspan="2"><b>Hasil tumor meeting :</b></td>
            </tr>                
            <tr>
                <td>1.</td>
                <td>Diagnosa : {$row->b3}</td>
            </tr>                  
            <tr>
                <td>2.</td>
                <td>Rencana Terapi : {$this->rTerapi($row->b4)}</td>
            </tr>         
            <tr>
                <td colspan="2"></td>
            </tr>          
            <tr>
                <td colspan="2"><b>TERAPI</b></td>
            </tr>                
            <tr>
                <td>1.</td>
                <td>Radioterapi : {$row->b5}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Mulai tanggal : {$this->tgl($row->b6)}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Selesai tanggal : {$this->tgl($row->b7)}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Dosis : {$row->b8}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Frekuensi : {$row->b9}</td>
            </tr>                   
            <tr>
                <td>2.</td>
                <td>Neo ajuvan kemoterapi</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Mulai tanggal : {$this->tgl($row->b10)}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Selesai tanggal : {$this->tgl($row->b11)}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Jenis : {$row->b12}</td>
            </tr>                   
            <tr>
                <td></td>
                <td>Dosis : {$row->b13}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Jumlah pemberian : {$this->jPemberian($row->b14)}</td>
            </tr>                   
            <tr>
                <td>3.</td>
                <td>Kemoradiasi</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Mulai tanggal : {$this->tgl($row->b15)}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Selesai tanggal : {$this->tgl($row->b16)}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Jenis radiasi : {$this->radioterapi($row->b17)}</td>
            </tr>                    
            <tr>
                <td></td>
                <td>Jenis kemoterapi : {$row->b18}</td>
            </tr>                   
            <tr>
                <td></td>
                <td>Dosis : {$row->b19}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Jumlah pemberian : {$this->jPemberian($row->b20)}</td>
            </tr>
            <tr>
                <td>4.</td>
                <td>Operasi : {$this->yn($row->b21)}</td>
            </tr>                  
            <tr>
                <td></td>
                <td colspan="3">Jenis Operasi : {$this->riwayat($arrJenisOperasi)}</td>
            </tr> 
            <tr>
                <td>5.</td>
                <td>Diseksi leher : {$this->yn($row->b23)}</td>
            </tr>                  
            <tr>
                <td></td>
                <td>Jika Ya, pilih : {$this->yn($row->b24)}</td>
            </tr>
                    
            <tr>
                <td colspan="2"></td>
            </tr>          
            <tr>
                <td colspan="2"><b>PASCA TERAPI</b></td>
            </tr>       
            <tr>
                <td colspan="2"><b>Follow Up : {$this->followup($row->b25)}</b></td>
            </tr>       
            <tr>
                <td colspan="2">Tanggal kunjungan follow Up : {$this->tgl($row->b26)}</td>
            </tr>        
            <tr>
                <td colspan="2"><b>Anamnesis : {$this->anamnesis($row->b27)}</b></td>
            </tr>         
            <tr>
                <td colspan="2">Lokal response : {$this->yn($row->b28)}</td>
            </tr>         
            <tr>
                <td colspan="2">Complete response : {$this->yn($row->b29)}</td>
            </tr>          
            <tr>
                <td colspan="2">Progresive : {$this->yn($row->b30)}</td>
            </tr>     
        </table>
EOD;
        
        
        $pdf->SetFont('helvetica', '', 10);
        $pdf->writeHTML($bag3, true, false, true, false, '');
        }
        
        $pdf->Output('Laporan Pemeriksaan.pdf', 'I');
    }
    
    public function anamnesis($param){
        if($param == 1){
            return "Ada keluhan";
        }else if($param == 2){
            return "Tidak ada keluhan";
        }else 
            return "-";
    }
    
    public function followup($param){
        if($param == 1){
            return "&lt;3 bulan";
        }else if($param == 2){
            return "6 bulan";
        }else if($param == 3){
            return "12 bulan";
        }else if($param == 4){
            return "&gt;12 bulan";
        }else if($param == 5){
            return "3-6 bulan";
        }else if($param == 6){
            return "12 bulan";
        }else 
            return "-";
    }
    
    public function diseksi($param){
        if($param == 1){
            return "Radikal";
        }else if($param == 2){
            return "Radikal modifikasi";
        }else if($param == 3){
            return "Selektif";
        }else 
            return "-";
    }
    
    public function jOperasi($param){
        if($param == 1){
            return "Maksilektomi";
        }else if($param == 2){
            return "Parotidektomi";
        }else if($param == 3){
            return "Manibulektomi";
        }else if($param == 4){
            return "Hemiglosektomi";
        }else if($param == 5){
            return "Tiroidektomi";
        }else if($param == 6){
            return "Endoskopi approach";
        }else 
            return "-";
    }
    
    public function jPemberian($param){
        if($param == 1){
            return "3x";
        }else if($param == 2){
            return "6x";
        }else if($param == 3){
            return "&gt;6x";
        }else 
            return "-";
    }
    
    public function radioterapi($param){
        if($param == 1){
            return "Jenis cobalt";
        }else if($param == 2){
            return "2D/3D";
        }else if($param == 3){
            return "IMRT";
        }else 
            return "-";
    }
    
    public function rTerapi($param){
        if($param == 1){
            return "Operasi";
        }else if($param == 2){
            return "Kemoradiasi";
        }else if($param == 3){
            return "Operasi + Radioterapi/ kemoradiasi";
        }else if($param == 4){
            return "Paliatif";
        }else 
            return "-";
    }
    
    
    public function stadium($param){
        if($param == 1){
            return "T1";
        }else if($param == 2){
            return "T2";
        }else if($param == 3){
            return "T3";
        }else if($param == 4){
            return "T4a";
        }else if($param == 5){
            return "T4b";
        }else if($param == 6){
            return "N1";
        }else if($param == 7){
            return "N2";
        }else if($param == 8){
            return "N2a";
        }else if($param == 9){
            return "N2b";
        }else if($param == 10){
            return "M0";
        }else if($param == 11){
            return "M1";
        }else 
            return "-";
    }
    
    public function lokasi2($param){
        if($param == 1){
            return "Paru";
        }else if($param == 2){
            return "Tulang";
        }else if($param == 3){
            return "Intrakranial";
        }else if($param == 4){
            return "Hati";
        }else if($param == 5){
            return "Ginjal";
        }else 
            return "-";
    }
    
    public function hasil($param){
        if($param == 1){
            return "Normal";
        }else if($param == 2){
            return "Konduktif";
        }else if($param == 3){
            return "Sensorineural";
        }else if($param == 4){
            return "Campur";
        }else if($param == 5){
            return "Ginjal";
        }else 
            return "-";
    }
    
    public function laring($param){
        if($param == 1){
            return "Keratin";
        }else if($param == 2){
            return "Diferensiasi baik";
        }else if($param == 3){
            return "Diferensiasi buruk";
        }else if($param == 4){
            return "Tidak keratin";
        }else if($param == 5){
            return "Diferensiasi sedang";
        }else if($param == 6){
            return "Jenis lain: neuroendokrin, dll";
        }else 
            return "-";
    }
    
    public function parnasal($param,$param2){
        if($param == 1){
            return "Limfoma maligna";
        }else if($param == 2){
            return "Karsinoma tidak berkeratin";
        }else if($param == 3){
            return "Diferensiasi sedang buruk";
        }else if($param == 4){
            return "T/NK Cell Lymphoma";
        }else if($param == 5){
            return "ACC";
        }else if($param == 6){
            return "Karsinoma sel skuamosa";
        }else if($param == 7){
            return "Diferensiasi baik";
        }else if($param == 8){
            return "Rhabdomyosarcoma";
        }else if($param == 9){
            return "AdenoCarsinoma";
        }else if($param == 10){
            return $param2;
        }else 
            return "-";
    }
    
    public function wf($param){
        if($param == 1){
            return "Tipe A";
        }else if($param == 2){
            return "Tipe B";
        }else 
            return "-";
    }
    
    public function who($param){
        if($param == 1){
            return "I";
        }else if($param == 2){
            return "II";
        }else if($param == 3){
            return "III";
        }else 
            return "-";
    }
    
    public function dKGB($param){
        if($param == 1){
            return "Keras";
        }else if($param == 2){
            return "Lunak";
        }else if($param == 3){
            return "Terfiksir";
        }else 
            return "-";
    }
    
    public function lKGB($param){
        if($param == 1){
            return "Level I";
        }else if($param == 2){
            return "Level II";
        }else if($param == 3){
            return "Level III";
        }else if($param == 4){
            return "Level IV";
        }else if($param == 5){
            return "Level V";
        }else if($param == 6){
            return "Level VI";
        }else 
            return "-";
    }
    
    public function getah($param){
        if($param == 1){
            return "Unilateral";
        }else if($param == 2){
            return "Kiri";
        }else if($param == 3){
            return "Kanan";
        }else if($param == 4){
            return "Bilateral";
        }else if($param == 5){
            return "Soliter";
        }else if($param == 6){
            return "Multipel";
        }else 
            return "-";
    }
    
    public function parotis($param){
        if($param == 1){
            return "Unilateral";
        }else if($param == 2){
            return "Kiri";
        }else if($param == 3){
            return "Kanan";
        }else if($param == 4){
            return "Bilateral";
        }else 
            return "-";
    }
    
    public function telingaTimpani($param){
        if($param == 1){
            return "Intak";
        }else if($param == 2){
            return "Perforasi";
        }else if($param == 3){
            return "Tidak Dapat di Nilai";
        }else 
            return "-";
    }
    public function telingaJenis($param){
        if($param == 1){
            return "Sentral";
        }else if($param == 2){
            return "Merginal";
        }else if($param == 3){
            return "Total";
        }else if($param == 4){
            return "Tidak Dapat di Nilai";
        }else 
            return "-";
    }
    public function telingaSekret($param){
        if($param == 1){
            return "Mukoid";
        }else if($param == 2){
            return "Serumukoid";
        }else if($param == 3){
            return "Mukopurulen";
        }else if($param == 4){
            return "Serous";
        }else if($param == 5){
            return "Purulen";
        }else 
            return "-";
    }
    public function telingaParese($param){
        if($param == 1){
            return "Sentral";
        }else if($param == 2){
            return "Perifer";
        }else 
            return "-";
    }
    public function telingaParese2($param){
        if($param == 1){
            return "HB I";
        }else if($param == 2){
            return "HB II";
        }else if($param == 3){
            return "HB III";
        }else if($param == 4){
            return "HB IV";
        }else if($param == 5){
            return "HB V";
        }else if($param == 6){
            return "HB VI";
        }else 
            return "-";
    }
    
    public function rino($param){
        if($param == 1){
            return "Supraglotis";
        }else if($param == 2){
            return "Glotis";
        }else if($param == 3){
            return "Subglotis";
        }else 
            return "-";
    }
    
    public function naso($param){
        if($param == 1){
            return "Normal";
        }else if($param == 2){
            return "Tumor Eksofitik";
        }else if($param == 3){
            return "Tumor Ulseratif";
        }else if($param == 4){
            return "Terfikasi";
        }else 
            return "-";
    }
    
    public function naso2($param){
        if($param == 1){
            return "Terbuka";
        }else if($param == 2){
            return "Tertutup";
        }else 
            return "-";
    }
    
    public function tonsil($param){
        if($param == 1){
            return "T1";
        }else if($param == 2){
            return "T2";
        }else if($param == 3){
            return "T3";
        }else if($param == 4){
            return "T4";
        }else 
            return "-";
    }
    
    public function molle($param){
        if($param == 1){
            return "Normal";
        }else if($param == 2){
            return "Destruksi";
        }else 
            return "-";
    }
    
    public function durum($param){
        if($param == 1){
            return "Normal";
        }else if($param == 2){
            return "Goyah";
        }else if($param == 3){
            return "Infiltrasi Tumor";
        }else 
            return "-";
    }
    
    public function lidah($param){
        if($param == 1){
            return "Normal";
        }else if($param == 2){
            return "Leukoplakia";
        }else if($param == 3){
            return "ulkus";
        }else if($param == 4){
            return "Eksofitik";
        }else 
            return "-";
    }
    
    public function mulut($param){
        if($param == 1){
            return "Normal";
        }else if($param == 2){
            return "Tumor";
        }else 
            return "-";
    }
    
    public function geligi($param){
        if($param == 1){
            return "Normal";
        }else if($param == 2){
            return "Goyah";
        }else if($param == 3){
            return "Infiltrasi Tumor";
        }else 
            return "-";
    }
    
    public function karnofsky($param){
        switch($param){
            case 1:
                return 0;
                break;
            case 2:
                return 10;
                break;
            case 3:
                return 30;
                break;
            case 4:
                return 40;
                break;
            case 5:
                return 50;
                break;
            case 6:
                return 60;
                break;
            case 7:
                return 70;
                break;
            case 8:
                return 80;
                break;
            case 9:
                return 90;
                break;
            case 10:
                return 100;
                break;
            default:
                return "-";
        }
    }
    
    public function frekuensi($param){
        if($param == 1){
            return "1 - 3x/minggu";
        }else if($param == 2){
            return "3 - 7x/minggu";
        }else if($param == 3){
            return "&gt; 7x/minggu";
        }else 
            return "-";
    }
    
    public function lamanya($param){
        if($param == 1){
            return "0 - 12 bulan";
        }else if($param == 2){
            return "12 - 60 bulan";
        }else if($param == 3){
            return "&gt; 60 bulan";
        }else 
            return "-";
    }
    
    public function jmAlkohol($param){
        if($param == 1){
            return "&lt;1 Gelas/hari";
        }else if($param == 2){
            return "&gt;1 Gelas/hari";
        }else 
            return "-";
    }
    
    public function jmRokok($param){
        if($param == 1){
            return "0 - 12 Batang/hari";
        }else if($param == 2){
            return "12 - 24 Batang/hari";
        }else if($param == 3){
            return "&gt; 24 Batang/hari";
        }else 
            return "-";
    }
    
    public function jmIkan($param){
        if($param == 1){
            return "&lt;1/9 dari jumlah nasi";
        }else if($param == 2){
            return "&gt;1/9 dari jumlah nasi";
        }else 
            return "-";
    }
    
    public function jKanker($param,$param2){
        if($param == 1){
            return "Sama";
        }else if($param == 2){
            return "Ca Cervic";
        }else if($param == 3){
            return "Ca Mammae";
        }else if($param == 4){
            return "Ca Prostat";
        }else if($param == 5){
            return "Ca Paru";
        }else if($param == 6){
            return "Ca Kolon";
        }else if($param == 7){
            return $param2;
        }else
            return "-";
    }
    
    public function keluarga($param,$param2){
        if($param == 1){
            return "Kakek";
        }else if($param == 2){
            return "Nenek";
        }else if($param == 3){
            return "Ayah";
        }else if($param == 4){
            return "Ibu";
        }else if($param == 5){
            return "Paman / Ibu";
        }else if($param == 6){
            return "Kakak / Adik";
        }else if($param == 7){
            return "Anak";
        }else if($param == 8){
            return $param2;
        }else
            return "-";
    }
    
    public function yn($param){
        if($param == 1){
            return "Ya";
        }else if($param == 2){
            return "Tidak";
        }else
            return "-";
    }
    
    public function lokasi($param){
        if($param == 1){
            return "Kanan";
        }else if($param == 2){
            return "Kiri";
        }else if($param == 3){
            return "Bilateral";
        }else 
            return "-";
    }
    
    public function lama($param){
        if($param == 1){
            return "0 - 6 Bulan";
        }else if($param == 2){
            return "6 - 12 Bulan";
        }else if($param == 3){
            return "&gt;12 Bulan";
        }else 
            return "-";
    }
    
    public function tgl($param){
        return date_format(date_create($param),'d/m/Y');
    }
    
    public function kedokter($param){
       if($param == 1){ 
           return "&lt; 6 Bulan";            
       }else if($param == 2 ){
           return "&gt; 6 Bulan";           
       } else 
           return "-";
    }
    
    public function jdokter($param,$param2){
       if($param == 1){ 
           return "Dokter Umum";            
       }else if($param == 2 ){
           return $param2;           
       } else 
           return "-";
    }
    
    public function riwayat($param1){
        $text = '';
        foreach($param1 as $key=>$val){
            if($val == 1){
                $text.=" ".$key.",";
            }
        }
        return $text;
    }
}


