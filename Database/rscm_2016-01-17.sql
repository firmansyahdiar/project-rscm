-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 18, 2016 at 05:29 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rscm`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_pasien`
--

CREATE TABLE IF NOT EXISTS `m_pasien` (
  `id_pasien` varchar(36) NOT NULL,
  `nama_pasien` varchar(100) NOT NULL,
  `namaOrangTua` varchar(100) NOT NULL,
  `usia` varchar(100) NOT NULL,
  `pendidikan` varchar(100) NOT NULL,
  `tempatLahir` varchar(100) NOT NULL,
  `tglLahir` date NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `suku` varchar(100) NOT NULL,
  `keturunan` varchar(100) NOT NULL,
  `asing` varchar(100) NOT NULL,
  `noRekamMedis` varchar(100) NOT NULL,
  `agama` varchar(100) NOT NULL,
  `daerah` varchar(100) NOT NULL,
  `jJalan` varchar(200) NOT NULL,
  `jRt` varchar(10) NOT NULL,
  `jRw` varchar(10) NOT NULL,
  `jNo` varchar(10) NOT NULL,
  `jTelp` varchar(10) NOT NULL,
  `jKelurahan` varchar(100) NOT NULL,
  `jKecamatan` varchar(100) NOT NULL,
  `aJalan` varchar(100) NOT NULL,
  `aRt` varchar(10) NOT NULL,
  `aRw` varchar(10) NOT NULL,
  `aNo` varchar(100) NOT NULL,
  `aTelp` varchar(100) NOT NULL,
  `aKelurahan` varchar(100) NOT NULL,
  `aKecamatan` varchar(100) NOT NULL,
  `aKabupaten` varchar(100) NOT NULL,
  `aProvinsi` varchar(100) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_pasien`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pasien`
--

INSERT INTO `m_pasien` (`id_pasien`, `nama_pasien`, `namaOrangTua`, `usia`, `pendidikan`, `tempatLahir`, `tglLahir`, `pekerjaan`, `suku`, `keturunan`, `asing`, `noRekamMedis`, `agama`, `daerah`, `jJalan`, `jRt`, `jRw`, `jNo`, `jTelp`, `jKelurahan`, `jKecamatan`, `aJalan`, `aRt`, `aRw`, `aNo`, `aTelp`, `aKelurahan`, `aKecamatan`, `aKabupaten`, `aProvinsi`, `user_id`, `modified`) VALUES
('b0bae5f5-21a8-421f-83fc-c7377e37aed6', 'Diar', 'Diar', '22', 'Sarjana', 'Bogor', '2016-01-08', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '--', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'cda92de8-2162-4e73-a180-e3ea76d2a1cf', '2016-01-15');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE IF NOT EXISTS `m_user` (
  `id_user` varchar(36) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `hak_akses` int(1) NOT NULL,
  `parent` varchar(36) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`id_user`, `nama_user`, `password`, `email`, `hak_akses`, `parent`, `modified`) VALUES
('08aa020e-0b94-47c3-9bb4-52d809053ece', 'user11', '03aa1a0b0375b0461c1b8f35b234e67a', 'user11@email.com', 2, 'db12a23e-b4f3-441b-95f4-9f9c1952ef6e', '2016-01-13'),
('6facc78f-f184-4cac-8d9c-9d1f5a2d42a6', 'user21', '2e129db15b6d6db5342ba5d328642262', 'user21@email.com', 2, '74c40c67-f1ad-45d1-ba12-4a9c5a89d34d', '2016-01-13'),
('710f5250-d755-416b-bf01-c0e078f35d5b', 'user22', '87dc1e131a1369fdf8f1c824a6a62dff', 'user22@email.com', 2, '74c40c67-f1ad-45d1-ba12-4a9c5a89d34d', '2016-01-13'),
('74c40c67-f1ad-45d1-ba12-4a9c5a89d34d', 'admin2', 'c84258e9c39059a89ab77d846ddab909', 'admin2@email.com', 1, 'cda92de8-2162-4e73-a180-e3ea76d2a1cf', '2016-01-13'),
('b7e9fd3d-10c3-4f19-a695-4fcf563e3388', 'user12', 'd781eaae8248db6ce1a7b82e58e60435', 'user12@email.com', 2, 'db12a23e-b4f3-441b-95f4-9f9c1952ef6e', '2016-01-13'),
('cda92de8-2162-4e73-a180-e3ea76d2a1cf', 'administrator', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', 0, '-', '2016-01-13'),
('db12a23e-b4f3-441b-95f4-9f9c1952ef6e', 'admin1', 'e00cf25ad42683b3df678c61f42c6bda', 'admin1@email.com', 1, 'cda92de8-2162-4e73-a180-e3ea76d2a1cf', '2016-01-13');

-- --------------------------------------------------------

--
-- Table structure for table `p_anamnesis`
--

CREATE TABLE IF NOT EXISTS `p_anamnesis` (
  `id_anamnesis` varchar(36) NOT NULL,
  `id_pasien` varchar(36) NOT NULL,
  `kunjungan` int(10) NOT NULL,
  `tgl_periksa` date NOT NULL,
  `Aa1` tinyint(1) NOT NULL,
  `Aa2` tinyint(1) NOT NULL,
  `Aa3` tinyint(1) NOT NULL,
  `Ab1` tinyint(1) NOT NULL,
  `Ab2` tinyint(1) NOT NULL,
  `Ab3` tinyint(1) NOT NULL,
  `Ac1` tinyint(1) NOT NULL,
  `Ac2` tinyint(1) NOT NULL,
  `Ac3` tinyint(1) NOT NULL,
  `Ad1` tinyint(1) NOT NULL,
  `Ad2` tinyint(1) NOT NULL,
  `Ad3` tinyint(1) NOT NULL,
  `Ae1` tinyint(1) NOT NULL,
  `Ae2` tinyint(1) NOT NULL,
  `Ae3` tinyint(1) NOT NULL,
  `Af1` tinyint(1) NOT NULL,
  `Af2` tinyint(1) NOT NULL,
  `Af3` tinyint(1) NOT NULL,
  `Ag1` tinyint(1) NOT NULL,
  `Ag2` tinyint(1) NOT NULL,
  `Ag3` tinyint(1) NOT NULL,
  `Ah1` tinyint(1) NOT NULL,
  `Ah2` tinyint(1) NOT NULL,
  `Ah3` tinyint(1) NOT NULL,
  `Ai1` tinyint(1) NOT NULL,
  `Ai2` tinyint(1) NOT NULL,
  `Ai3` tinyint(1) NOT NULL,
  `Aj1` tinyint(1) NOT NULL,
  `Aj2` tinyint(1) NOT NULL,
  `Aj3` tinyint(1) NOT NULL,
  `Ak1` tinyint(1) NOT NULL,
  `Ak2` tinyint(1) NOT NULL,
  `Ak3` tinyint(1) NOT NULL,
  `Al1` tinyint(1) NOT NULL,
  `Al2` tinyint(1) NOT NULL,
  `Al3` tinyint(1) NOT NULL,
  `Am1` tinyint(1) NOT NULL,
  `Am2` tinyint(1) NOT NULL,
  `Am3` tinyint(1) NOT NULL,
  `Ba1` tinyint(1) NOT NULL,
  `Ba2` tinyint(1) NOT NULL,
  `Ba3` tinyint(1) NOT NULL,
  `Bb1` tinyint(1) NOT NULL,
  `Bb2` tinyint(1) NOT NULL,
  `Bb3` tinyint(1) NOT NULL,
  `Bc1` tinyint(1) NOT NULL,
  `Bc2` tinyint(1) NOT NULL,
  `Bc3` tinyint(1) NOT NULL,
  `Bd1` tinyint(1) NOT NULL,
  `Bd2` tinyint(1) NOT NULL,
  `Bd3` tinyint(1) NOT NULL,
  `Be1` tinyint(1) NOT NULL,
  `Be2` tinyint(1) NOT NULL,
  `Be3` tinyint(1) NOT NULL,
  `Bf1` tinyint(1) NOT NULL,
  `Bf2` tinyint(1) NOT NULL,
  `Bf3` tinyint(1) NOT NULL,
  `Bg1` tinyint(1) NOT NULL,
  `Bg2` tinyint(1) NOT NULL,
  `Bg3` tinyint(1) NOT NULL,
  `Bh1` tinyint(1) NOT NULL,
  `Bh2` tinyint(1) NOT NULL,
  `Bh3` tinyint(1) NOT NULL,
  `Bi1` tinyint(1) NOT NULL,
  `Bi2` tinyint(1) NOT NULL,
  `Bi3` tinyint(1) NOT NULL,
  `Bj1` tinyint(1) NOT NULL,
  `Bj2` tinyint(1) NOT NULL,
  `Bj3` tinyint(1) NOT NULL,
  `Ca1` tinyint(1) NOT NULL,
  `Ca2` tinyint(1) NOT NULL,
  `Ca3` tinyint(1) NOT NULL,
  `Cb1` tinyint(1) NOT NULL,
  `Cb2` tinyint(1) NOT NULL,
  `Cb3` tinyint(1) NOT NULL,
  `Cc1` tinyint(1) NOT NULL,
  `Cc2` tinyint(1) NOT NULL,
  `Cc3` tinyint(1) NOT NULL,
  `Cd1` tinyint(1) NOT NULL,
  `Cd2` tinyint(1) NOT NULL,
  `Cd3` tinyint(1) NOT NULL,
  `Ce1` tinyint(1) NOT NULL,
  `Ce2` tinyint(1) NOT NULL,
  `Ce3` tinyint(1) NOT NULL,
  `Cf1` tinyint(1) NOT NULL,
  `Cf2` tinyint(1) NOT NULL,
  `Cf3` tinyint(1) NOT NULL,
  `Da1` tinyint(1) NOT NULL,
  `Da2` tinyint(1) NOT NULL,
  `Da3` tinyint(1) NOT NULL,
  `Db1` tinyint(1) NOT NULL,
  `Db2` tinyint(1) NOT NULL,
  `Db3` tinyint(1) NOT NULL,
  `Dc1` tinyint(1) NOT NULL,
  `Dc2` tinyint(1) NOT NULL,
  `Dc3` tinyint(1) NOT NULL,
  `Dd1` tinyint(1) NOT NULL,
  `Dd2` tinyint(1) NOT NULL,
  `Dd3` tinyint(1) NOT NULL,
  `De1` tinyint(1) NOT NULL,
  `De2` tinyint(1) NOT NULL,
  `De3` tinyint(1) NOT NULL,
  `Df1` tinyint(1) NOT NULL,
  `Df2` tinyint(1) NOT NULL,
  `Df3` tinyint(1) NOT NULL,
  `Ea1` tinyint(1) NOT NULL,
  `Ea2` tinyint(1) NOT NULL,
  `Ea3` tinyint(1) NOT NULL,
  `Eb1` tinyint(1) NOT NULL,
  `Eb2` tinyint(1) NOT NULL,
  `Eb3` tinyint(1) NOT NULL,
  `Ec1` tinyint(1) NOT NULL,
  `Ec2` tinyint(1) NOT NULL,
  `Ec3` tinyint(1) NOT NULL,
  `Ed1` tinyint(1) NOT NULL,
  `Ed2` tinyint(1) NOT NULL,
  `Ed3` tinyint(1) NOT NULL,
  `Ee1` tinyint(1) NOT NULL,
  `Ee2` tinyint(1) NOT NULL,
  `Ee3` tinyint(1) NOT NULL,
  `Ef1` tinyint(1) NOT NULL,
  `Ef2` varchar(100) NOT NULL,
  `Eg1` tinyint(1) NOT NULL,
  `Eg2` tinyint(1) NOT NULL,
  `Eg3` tinyint(1) NOT NULL,
  `Eg4` tinyint(1) NOT NULL,
  `Eg5` tinyint(1) NOT NULL,
  `Eg6` tinyint(1) NOT NULL,
  `EgLainnya` varchar(100) NOT NULL,
  `Ef2Lainnya` varchar(100) NOT NULL,
  `Fa1` tinyint(1) NOT NULL,
  `Fa2` tinyint(1) NOT NULL,
  `Fa2Lainnya` varchar(100) NOT NULL,
  `Fa3` tinyint(1) NOT NULL,
  `Fa3Lainnya` varchar(100) NOT NULL,
  `Ga1` tinyint(1) NOT NULL,
  `Ga2` tinyint(1) NOT NULL,
  `Ga3` tinyint(1) NOT NULL,
  `Ga4` tinyint(1) NOT NULL,
  `Gb1` tinyint(1) NOT NULL,
  `Gb2` tinyint(1) NOT NULL,
  `Gb3` tinyint(1) NOT NULL,
  `Gc1` tinyint(1) NOT NULL,
  `Gc2` tinyint(1) NOT NULL,
  `Gc3` tinyint(1) NOT NULL,
  `Gc4` tinyint(1) NOT NULL,
  `Gd1` tinyint(1) NOT NULL,
  `Gd2` tinyint(1) NOT NULL,
  `Gd3` tinyint(1) NOT NULL,
  `Ge1` tinyint(1) NOT NULL,
  `Ge2` tinyint(1) NOT NULL,
  `Ge3` tinyint(1) NOT NULL,
  `Ge4` tinyint(1) NOT NULL,
  `Gf1` tinyint(1) NOT NULL,
  `Gf2` tinyint(1) NOT NULL,
  `Gf3` tinyint(1) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL,
  `grafik` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_anamnesis`),
  KEY `id_pasien` (`id_pasien`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_anamnesis`
--

INSERT INTO `p_anamnesis` (`id_anamnesis`, `id_pasien`, `kunjungan`, `tgl_periksa`, `Aa1`, `Aa2`, `Aa3`, `Ab1`, `Ab2`, `Ab3`, `Ac1`, `Ac2`, `Ac3`, `Ad1`, `Ad2`, `Ad3`, `Ae1`, `Ae2`, `Ae3`, `Af1`, `Af2`, `Af3`, `Ag1`, `Ag2`, `Ag3`, `Ah1`, `Ah2`, `Ah3`, `Ai1`, `Ai2`, `Ai3`, `Aj1`, `Aj2`, `Aj3`, `Ak1`, `Ak2`, `Ak3`, `Al1`, `Al2`, `Al3`, `Am1`, `Am2`, `Am3`, `Ba1`, `Ba2`, `Ba3`, `Bb1`, `Bb2`, `Bb3`, `Bc1`, `Bc2`, `Bc3`, `Bd1`, `Bd2`, `Bd3`, `Be1`, `Be2`, `Be3`, `Bf1`, `Bf2`, `Bf3`, `Bg1`, `Bg2`, `Bg3`, `Bh1`, `Bh2`, `Bh3`, `Bi1`, `Bi2`, `Bi3`, `Bj1`, `Bj2`, `Bj3`, `Ca1`, `Ca2`, `Ca3`, `Cb1`, `Cb2`, `Cb3`, `Cc1`, `Cc2`, `Cc3`, `Cd1`, `Cd2`, `Cd3`, `Ce1`, `Ce2`, `Ce3`, `Cf1`, `Cf2`, `Cf3`, `Da1`, `Da2`, `Da3`, `Db1`, `Db2`, `Db3`, `Dc1`, `Dc2`, `Dc3`, `Dd1`, `Dd2`, `Dd3`, `De1`, `De2`, `De3`, `Df1`, `Df2`, `Df3`, `Ea1`, `Ea2`, `Ea3`, `Eb1`, `Eb2`, `Eb3`, `Ec1`, `Ec2`, `Ec3`, `Ed1`, `Ed2`, `Ed3`, `Ee1`, `Ee2`, `Ee3`, `Ef1`, `Ef2`, `Eg1`, `Eg2`, `Eg3`, `Eg4`, `Eg5`, `Eg6`, `EgLainnya`, `Ef2Lainnya`, `Fa1`, `Fa2`, `Fa2Lainnya`, `Fa3`, `Fa3Lainnya`, `Ga1`, `Ga2`, `Ga3`, `Ga4`, `Gb1`, `Gb2`, `Gb3`, `Gc1`, `Gc2`, `Gc3`, `Gc4`, `Gd1`, `Gd2`, `Gd3`, `Ge1`, `Ge2`, `Ge3`, `Ge4`, `Gf1`, `Gf2`, `Gf3`, `user_id`, `modified`, `grafik`) VALUES
('a90a7d68-01f4-4431-aaf5-ddff3c853e34', 'b0bae5f5-21a8-421f-83fc-c7377e37aed6', 3, '2016-01-18', 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '2', 1, 1, 1, 1, 1, 1, 'retert', '123', 1, 7, '123qwe', 7, '123qew', 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 'cda92de8-2162-4e73-a180-e3ea76d2a1cf', '2016-01-18', 0);

-- --------------------------------------------------------

--
-- Table structure for table `p_pemeriksaan_fisik`
--

CREATE TABLE IF NOT EXISTS `p_pemeriksaan_fisik` (
  `id_pemeriksaan_fisik` varchar(36) NOT NULL,
  `id_pasien` varchar(36) NOT NULL,
  `kunjungan` varchar(10) NOT NULL,
  `tgl_periksa` date NOT NULL,
  `td` varchar(100) NOT NULL,
  `nadi` varchar(100) NOT NULL,
  `tb` varchar(100) NOT NULL,
  `bb` varchar(100) NOT NULL,
  `ks1` tinyint(1) NOT NULL,
  `ks2` tinyint(1) NOT NULL,
  `ks3` tinyint(1) NOT NULL,
  `ks4` tinyint(1) NOT NULL,
  `ks5` tinyint(1) NOT NULL,
  `ks6` tinyint(1) NOT NULL,
  `ks7` tinyint(1) NOT NULL,
  `ks8` tinyint(1) NOT NULL,
  `ks9` tinyint(1) NOT NULL,
  `ks10` tinyint(1) NOT NULL,
  `ks11` tinyint(1) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_pemeriksaan_fisik`
--

INSERT INTO `p_pemeriksaan_fisik` (`id_pemeriksaan_fisik`, `id_pasien`, `kunjungan`, `tgl_periksa`, `td`, `nadi`, `tb`, `bb`, `ks1`, `ks2`, `ks3`, `ks4`, `ks5`, `ks6`, `ks7`, `ks8`, `ks9`, `ks10`, `ks11`, `user_id`, `modified`) VALUES
('d5bd793c-1473-4880-a2cf-82aba2460382', '5b36fc78-fb79-470f-a899-e6092107f6f4', '1', '2016-01-06', '1', '3', '2', '4', 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2016-01-07'),
('71e0e03c-6eb5-4c64-8d21-93e9650a27cb', '5b36fc78-fb79-470f-a899-e6092107f6f4', '54', '2016-01-06', '2', '2', '2', '2', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2016-01-07');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `m_pasien`
--
ALTER TABLE `m_pasien`
  ADD CONSTRAINT `m_pasien_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `m_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `p_anamnesis`
--
ALTER TABLE `p_anamnesis`
  ADD CONSTRAINT `p_anamnesis_ibfk_5` FOREIGN KEY (`id_pasien`) REFERENCES `m_pasien` (`id_pasien`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `p_anamnesis_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `m_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
