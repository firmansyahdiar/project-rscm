-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 03, 2016 at 05:09 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rscm`
--

-- --------------------------------------------------------

--
-- Table structure for table `p_pemeriksaan_fisik`
--

CREATE TABLE IF NOT EXISTS `p_pemeriksaan_fisik` (
  `id_pemeriksaan_fisik` varchar(36) NOT NULL,
  `id_pasien` varchar(36) NOT NULL,
  `kunjungan` varchar(10) NOT NULL,
  `tgl_periksa` date NOT NULL,
  `td` varchar(100) NOT NULL,
  `nadi` varchar(100) NOT NULL,
  `tb` varchar(100) NOT NULL,
  `bb` varchar(100) NOT NULL,
  `ks1` tinyint(2) NOT NULL,
  `ks2` tinyint(1) NOT NULL,
  `ks3` tinyint(1) NOT NULL,
  `ks4` tinyint(1) NOT NULL,
  `ks5` tinyint(1) NOT NULL,
  `ks6` tinyint(1) NOT NULL,
  `ks7` tinyint(1) NOT NULL,
  `ks8` tinyint(1) NOT NULL,
  `ks9` tinyint(1) NOT NULL,
  `ks10` tinyint(1) NOT NULL,
  `ks11` tinyint(1) NOT NULL,
  `ks12` tinyint(1) NOT NULL,
  `ks13` tinyint(1) NOT NULL,
  `ks14` tinyint(1) NOT NULL,
  `ks15` tinyint(1) NOT NULL,
  `ks16` tinyint(1) NOT NULL,
  `ks17` varchar(20) NOT NULL,
  `ks18` tinyint(1) NOT NULL,
  `ks19` tinyint(1) NOT NULL,
  `ks20` tinyint(1) NOT NULL,
  `ks21` tinyint(1) NOT NULL,
  `ks22` tinyint(1) NOT NULL,
  `ks23` tinyint(1) NOT NULL,
  `ks24` tinyint(1) NOT NULL,
  `ks25` tinyint(1) NOT NULL,
  `ks26` tinyint(1) NOT NULL,
  `ks27` tinyint(1) NOT NULL,
  `ks28` tinyint(1) NOT NULL,
  `ks29` tinyint(1) NOT NULL,
  `ks30` tinyint(1) NOT NULL,
  `ks31` tinyint(1) NOT NULL,
  `ks32` tinyint(1) NOT NULL,
  `ks33` varchar(20) NOT NULL,
  `ks34` tinyint(1) NOT NULL,
  `ks35` varchar(20) NOT NULL,
  `ks36` tinyint(1) NOT NULL,
  `ks37` varchar(20) NOT NULL,
  `ks38` tinyint(1) NOT NULL,
  `ks39` tinyint(1) NOT NULL,
  `ks40` tinyint(1) NOT NULL,
  `ks41` tinyint(1) NOT NULL,
  `ks42` tinyint(1) NOT NULL,
  `ks43` tinyint(1) NOT NULL,
  `ks44` tinyint(1) NOT NULL,
  `ks45` tinyint(1) NOT NULL,
  `ks46` tinyint(1) NOT NULL,
  `ks47` tinyint(1) NOT NULL,
  `ks48` tinyint(1) NOT NULL,
  `ks49` tinyint(1) NOT NULL,
  `ks50` tinyint(1) NOT NULL,
  `ks51` tinyint(1) NOT NULL,
  `ks52` tinyint(1) NOT NULL,
  `ks53` tinyint(1) NOT NULL,
  `ks54` tinyint(1) NOT NULL,
  `Ks55` tinyint(1) NOT NULL,
  `Ks56` tinyint(1) NOT NULL,
  `Ks57` tinyint(1) NOT NULL,
  `Ks58` tinyint(1) NOT NULL,
  `Ks59` tinyint(1) NOT NULL,
  `a1` date NOT NULL,
  `a2` varchar(100) NOT NULL,
  `a3` varchar(50) NOT NULL,
  `a4` varchar(100) NOT NULL,
  `a5` varchar(100) NOT NULL,
  `a6` varchar(100) NOT NULL,
  `a7` tinyint(1) NOT NULL,
  `a7a` varchar(50) NOT NULL,
  `a8` tinyint(1) NOT NULL,
  `a9` date NOT NULL,
  `a10` varchar(100) NOT NULL,
  `a11` varchar(50) NOT NULL,
  `a12` tinyint(1) NOT NULL,
  `a13` tinyint(1) NOT NULL,
  `a14` tinyint(1) NOT NULL,
  `a15` tinyint(1) NOT NULL,
  `a16` varchar(100) NOT NULL,
  `a17` date NOT NULL,
  `a18` varchar(100) NOT NULL,
  `a19` varchar(50) NOT NULL,
  `a20` text NOT NULL,
  `a21` date NOT NULL,
  `a22` varchar(100) NOT NULL,
  `a23` varchar(50) NOT NULL,
  `a24` text NOT NULL,
  `a25` date NOT NULL,
  `a26` varchar(100) NOT NULL,
  `a27` varchar(50) NOT NULL,
  `a28` text NOT NULL,
  `a29` date NOT NULL,
  `a30` varchar(100) NOT NULL,
  `a31` varchar(50) NOT NULL,
  `a32` text NOT NULL,
  `a33` date NOT NULL,
  `a34` varchar(100) NOT NULL,
  `a35` varchar(50) NOT NULL,
  `a36` text NOT NULL,
  `a37` date NOT NULL,
  `a38` varchar(100) NOT NULL,
  `a39` varchar(50) NOT NULL,
  `a40` text NOT NULL,
  `a41` date NOT NULL,
  `a42` varchar(100) NOT NULL,
  `a43` varchar(50) NOT NULL,
  `a44` text NOT NULL,
  `a45` varchar(100) NOT NULL,
  `a46` tinyint(1) NOT NULL,
  `a47` tinyint(1) NOT NULL,
  `a48` date NOT NULL,
  `a49` varchar(200) NOT NULL,
  `a50` tinyint(1) NOT NULL,
  `a51` tinyint(1) NOT NULL,
  `a52` date NOT NULL,
  `a53` varchar(200) NOT NULL,
  `a54` tinyint(1) NOT NULL,
  `a55` tinyint(1) NOT NULL,
  `b1` tinyint(1) NOT NULL,
  `b2` date NOT NULL,
  `b3` varchar(100) NOT NULL,
  `b4` tinyint(1) NOT NULL,
  `b5` tinyint(1) NOT NULL,
  `b6` date NOT NULL,
  `b7` date NOT NULL,
  `b8` varchar(50) NOT NULL,
  `b9` varchar(50) NOT NULL,
  `b10` date NOT NULL,
  `b11` date NOT NULL,
  `b12` varchar(50) NOT NULL,
  `b13` varchar(50) NOT NULL,
  `b14` tinyint(1) NOT NULL,
  `b15` date NOT NULL,
  `b16` date NOT NULL,
  `b17` tinyint(1) NOT NULL,
  `b18` varchar(50) NOT NULL,
  `b19` varchar(50) NOT NULL,
  `b20` tinyint(1) NOT NULL,
  `b21` tinyint(1) NOT NULL,
  `b22` tinyint(1) NOT NULL,
  `b23` tinyint(1) NOT NULL,
  `b24` tinyint(1) NOT NULL,
  `b25` tinyint(1) NOT NULL,
  `b26` date NOT NULL,
  `b27` tinyint(1) NOT NULL,
  `b28` tinyint(1) NOT NULL,
  `b29` tinyint(1) NOT NULL,
  `b30` tinyint(1) NOT NULL,
  `b31` tinyint(1) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_pemeriksaan_fisik`
--

INSERT INTO `p_pemeriksaan_fisik` (`id_pemeriksaan_fisik`, `id_pasien`, `kunjungan`, `tgl_periksa`, `td`, `nadi`, `tb`, `bb`, `ks1`, `ks2`, `ks3`, `ks4`, `ks5`, `ks6`, `ks7`, `ks8`, `ks9`, `ks10`, `ks11`, `ks12`, `ks13`, `ks14`, `ks15`, `ks16`, `ks17`, `ks18`, `ks19`, `ks20`, `ks21`, `ks22`, `ks23`, `ks24`, `ks25`, `ks26`, `ks27`, `ks28`, `ks29`, `ks30`, `ks31`, `ks32`, `ks33`, `ks34`, `ks35`, `ks36`, `ks37`, `ks38`, `ks39`, `ks40`, `ks41`, `ks42`, `ks43`, `ks44`, `ks45`, `ks46`, `ks47`, `ks48`, `ks49`, `ks50`, `ks51`, `ks52`, `ks53`, `ks54`, `Ks55`, `Ks56`, `Ks57`, `Ks58`, `Ks59`, `a1`, `a2`, `a3`, `a4`, `a5`, `a6`, `a7`, `a7a`, `a8`, `a9`, `a10`, `a11`, `a12`, `a13`, `a14`, `a15`, `a16`, `a17`, `a18`, `a19`, `a20`, `a21`, `a22`, `a23`, `a24`, `a25`, `a26`, `a27`, `a28`, `a29`, `a30`, `a31`, `a32`, `a33`, `a34`, `a35`, `a36`, `a37`, `a38`, `a39`, `a40`, `a41`, `a42`, `a43`, `a44`, `a45`, `a46`, `a47`, `a48`, `a49`, `a50`, `a51`, `a52`, `a53`, `a54`, `a55`, `b1`, `b2`, `b3`, `b4`, `b5`, `b6`, `b7`, `b8`, `b9`, `b10`, `b11`, `b12`, `b13`, `b14`, `b15`, `b16`, `b17`, `b18`, `b19`, `b20`, `b21`, `b22`, `b23`, `b24`, `b25`, `b26`, `b27`, `b28`, `b29`, `b30`, `b31`, `user_id`, `modified`) VALUES
('41499b9e-8579-47ce-9575-80f83a93fe25', 'b0bae5f5-21a8-421f-83fc-c7377e37aed6', '1', '2016-02-03', '', '', '', '', 0, 2, 3, 3, 2, 1, 2, 2, 1, 3, 1, 1, 3, 1, 1, 3, '3', 2, 4, 3, 1, 4, 2, 4, 4, 2, 2, 1, 2, 4, 1, 0, '1', 2, '3', 3, '2', 0, 0, 3, 1, 1, 3, 3, 2, 123, 1, 2, 2, 123, 2, 123, 2, 2, 1, 2, 3, 2, 6, '2016-02-03', '', '', '', '', '', 2, '', 2, '2016-02-03', '', '', 2, 1, 2, 2, '', '2016-02-03', '', '', '', '2016-02-03', '', '', '', '2016-02-03', '', '', '', '2016-02-03', '', '', '', '2016-02-03', '', '', '', '2016-02-03', '', '', '', '2016-02-03', '', '', '', '', 1, 3, '0000-00-00', 'qwe', 2, 1, '0000-00-00', 'asd', 4, 3, 0, '2016-02-03', '', 0, 0, '2016-02-03', '2016-02-03', '', '', '2016-02-03', '2016-02-03', '', '', 0, '2016-02-03', '2016-02-03', 0, '', '', 0, 0, 0, 0, 0, 0, '2016-02-03', 0, 0, 0, 0, 8, 'cda92de8-2162-4e73-a180-e3ea76d2a1cf', '2016-02-03');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
