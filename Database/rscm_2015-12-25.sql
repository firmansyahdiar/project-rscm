-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2015 at 10:44 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rscm`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_pasien`
--

CREATE TABLE IF NOT EXISTS `m_pasien` (
  `id_pasien` varchar(36) NOT NULL,
  `nama_pasien` varchar(100) NOT NULL,
  `namaOrangTua` varchar(100) NOT NULL,
  `usia` varchar(100) NOT NULL,
  `pendidikan` varchar(100) NOT NULL,
  `tempatLahir` varchar(100) NOT NULL,
  `tglLahir` date NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `suku` varchar(100) NOT NULL,
  `keturunan` varchar(100) NOT NULL,
  `asing` varchar(100) NOT NULL,
  `noRekamMedis` varchar(100) NOT NULL,
  `agama` varchar(100) NOT NULL,
  `daerah` varchar(100) NOT NULL,
  `jJalan` varchar(200) NOT NULL,
  `jRt` varchar(10) NOT NULL,
  `jRw` varchar(10) NOT NULL,
  `jNo` varchar(10) NOT NULL,
  `jTelp` varchar(10) NOT NULL,
  `jKelurahan` varchar(100) NOT NULL,
  `jKecamatan` varchar(100) NOT NULL,
  `aJalan` varchar(100) NOT NULL,
  `aRt` varchar(10) NOT NULL,
  `aRw` varchar(10) NOT NULL,
  `aNo` varchar(100) NOT NULL,
  `aTelp` varchar(100) NOT NULL,
  `aKelurahan` varchar(100) NOT NULL,
  `aKecamatan` varchar(100) NOT NULL,
  `aKabupaten` varchar(100) NOT NULL,
  `aProvinsi` varchar(100) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_pasien`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pasien`
--

INSERT INTO `m_pasien` (`id_pasien`, `nama_pasien`, `namaOrangTua`, `usia`, `pendidikan`, `tempatLahir`, `tglLahir`, `pekerjaan`, `suku`, `keturunan`, `asing`, `noRekamMedis`, `agama`, `daerah`, `jJalan`, `jRt`, `jRw`, `jNo`, `jTelp`, `jKelurahan`, `jKecamatan`, `aJalan`, `aRt`, `aRw`, `aNo`, `aTelp`, `aKelurahan`, `aKecamatan`, `aKabupaten`, `aProvinsi`, `user_id`, `modified`) VALUES
('5b36fc78-fb79-470f-a899-e6092107f6f4', 'Diar', '12', '12', '12', '12', '2015-12-23', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2015-12-23');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE IF NOT EXISTS `m_user` (
  `id_user` varchar(36) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `hak_akses` int(1) NOT NULL,
  `parent` int(10) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`id_user`, `nama_user`, `password`, `email`, `hak_akses`, `parent`, `modified`) VALUES
('3f0c0758-fcf8-43e1-ac82-813cc18fddcc', 'Super Administrator', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', 0, 0, '0000-00-00'),
('3f0c0758-fcf8-45e1-ac82-803cc18fddcc', 'qwe', '76d80224611fc919a5d54f0ff9fba446', 'qwe@qwe.qwe', 0, 1, '2015-12-20'),
('9e8d0756-f94d-460f-8a24-83e26fc0fbb4', 'jalaludin', '', 'asepjalaludin@gmail.com', 0, 1, '2015-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `p_anamnesis`
--

CREATE TABLE IF NOT EXISTS `p_anamnesis` (
  `id_anamnesis` varchar(36) NOT NULL,
  `id_pasien` varchar(36) NOT NULL,
  `kunjungan` varchar(10) NOT NULL,
  `tgl_periksa` date NOT NULL,
  `Aa1` tinyint(1) NOT NULL,
  `Aa2` tinyint(1) NOT NULL,
  `Aa3` tinyint(1) NOT NULL,
  `Aa4` tinyint(1) NOT NULL,
  `Aa5` tinyint(1) NOT NULL,
  `Aa6` tinyint(1) NOT NULL,
  `Aa7` tinyint(1) NOT NULL,
  `Aa8` tinyint(1) NOT NULL,
  `Ab1` tinyint(1) NOT NULL,
  `Ab2` tinyint(1) NOT NULL,
  `Ab3` tinyint(1) NOT NULL,
  `Ab4` tinyint(1) NOT NULL,
  `Ab5` tinyint(1) NOT NULL,
  `Ab6` tinyint(1) NOT NULL,
  `Ab7` tinyint(1) NOT NULL,
  `Ab8` tinyint(1) NOT NULL,
  `Ac1` tinyint(1) NOT NULL,
  `Ac2` tinyint(1) NOT NULL,
  `Ac3` tinyint(1) NOT NULL,
  `Ac4` tinyint(1) NOT NULL,
  `Ac5` tinyint(1) NOT NULL,
  `Ac6` tinyint(1) NOT NULL,
  `Ac7` tinyint(1) NOT NULL,
  `Ac8` tinyint(1) NOT NULL,
  `Ad1` tinyint(1) NOT NULL,
  `Ad2` tinyint(1) NOT NULL,
  `Ad3` tinyint(1) NOT NULL,
  `Ad4` tinyint(1) NOT NULL,
  `Ad5` tinyint(1) NOT NULL,
  `Ad6` tinyint(1) NOT NULL,
  `Ad7` tinyint(1) NOT NULL,
  `Ad8` tinyint(1) NOT NULL,
  `Ae1` tinyint(1) NOT NULL,
  `Ae2` tinyint(1) NOT NULL,
  `Ae3` tinyint(1) NOT NULL,
  `Ae4` tinyint(1) NOT NULL,
  `Ae5` tinyint(1) NOT NULL,
  `Ae6` tinyint(1) NOT NULL,
  `Ae7` tinyint(1) NOT NULL,
  `Ae8` tinyint(1) NOT NULL,
  `Af1` tinyint(1) NOT NULL,
  `Af2` tinyint(1) NOT NULL,
  `Af3` tinyint(1) NOT NULL,
  `Af4` tinyint(1) NOT NULL,
  `Af5` tinyint(1) NOT NULL,
  `Af6` tinyint(1) NOT NULL,
  `Af7` tinyint(1) NOT NULL,
  `Af8` tinyint(1) NOT NULL,
  `Ag1` tinyint(1) NOT NULL,
  `Ag2` tinyint(1) NOT NULL,
  `Ag3` tinyint(1) NOT NULL,
  `Ag4` tinyint(1) NOT NULL,
  `Ag5` tinyint(1) NOT NULL,
  `Ag6` tinyint(1) NOT NULL,
  `Ag7` tinyint(1) NOT NULL,
  `Ag8` tinyint(1) NOT NULL,
  `Ah1` tinyint(1) NOT NULL,
  `Ah2` tinyint(1) NOT NULL,
  `Ah3` tinyint(1) NOT NULL,
  `Ah4` tinyint(1) NOT NULL,
  `Ah5` tinyint(1) NOT NULL,
  `Ah6` tinyint(1) NOT NULL,
  `Ah7` tinyint(1) NOT NULL,
  `Ah8` tinyint(1) NOT NULL,
  `Ai1` tinyint(1) NOT NULL,
  `Ai2` tinyint(1) NOT NULL,
  `Ai3` tinyint(1) NOT NULL,
  `Ai4` tinyint(1) NOT NULL,
  `Ai5` tinyint(1) NOT NULL,
  `Ai6` tinyint(1) NOT NULL,
  `Ai7` tinyint(1) NOT NULL,
  `Ai8` tinyint(1) NOT NULL,
  `Aj1` tinyint(1) NOT NULL,
  `Aj2` tinyint(1) NOT NULL,
  `Aj3` tinyint(1) NOT NULL,
  `Aj4` tinyint(1) NOT NULL,
  `Aj5` tinyint(1) NOT NULL,
  `Aj6` tinyint(1) NOT NULL,
  `Aj7` tinyint(1) NOT NULL,
  `Aj8` tinyint(1) NOT NULL,
  `Ak1` tinyint(1) NOT NULL,
  `Ak2` tinyint(1) NOT NULL,
  `Ak3` tinyint(1) NOT NULL,
  `Ak4` tinyint(1) NOT NULL,
  `Ak5` tinyint(1) NOT NULL,
  `Ak6` tinyint(1) NOT NULL,
  `Ak7` tinyint(1) NOT NULL,
  `Ak8` tinyint(1) NOT NULL,
  `Al1` tinyint(1) NOT NULL,
  `Al2` tinyint(1) NOT NULL,
  `Al3` tinyint(1) NOT NULL,
  `Al4` tinyint(1) NOT NULL,
  `Al5` tinyint(1) NOT NULL,
  `Al6` tinyint(1) NOT NULL,
  `Al7` tinyint(1) NOT NULL,
  `Al8` tinyint(1) NOT NULL,
  `Am1` tinyint(1) NOT NULL,
  `Am2` tinyint(1) NOT NULL,
  `Am3` tinyint(1) NOT NULL,
  `Am4` tinyint(1) NOT NULL,
  `Am5` tinyint(1) NOT NULL,
  `Am6` tinyint(1) NOT NULL,
  `Am7` tinyint(1) NOT NULL,
  `Am8` tinyint(1) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_anamnesis`),
  KEY `id_pasien` (`id_pasien`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_anamnesis`
--

INSERT INTO `p_anamnesis` (`id_anamnesis`, `id_pasien`, `kunjungan`, `tgl_periksa`, `Aa1`, `Aa2`, `Aa3`, `Aa4`, `Aa5`, `Aa6`, `Aa7`, `Aa8`, `Ab1`, `Ab2`, `Ab3`, `Ab4`, `Ab5`, `Ab6`, `Ab7`, `Ab8`, `Ac1`, `Ac2`, `Ac3`, `Ac4`, `Ac5`, `Ac6`, `Ac7`, `Ac8`, `Ad1`, `Ad2`, `Ad3`, `Ad4`, `Ad5`, `Ad6`, `Ad7`, `Ad8`, `Ae1`, `Ae2`, `Ae3`, `Ae4`, `Ae5`, `Ae6`, `Ae7`, `Ae8`, `Af1`, `Af2`, `Af3`, `Af4`, `Af5`, `Af6`, `Af7`, `Af8`, `Ag1`, `Ag2`, `Ag3`, `Ag4`, `Ag5`, `Ag6`, `Ag7`, `Ag8`, `Ah1`, `Ah2`, `Ah3`, `Ah4`, `Ah5`, `Ah6`, `Ah7`, `Ah8`, `Ai1`, `Ai2`, `Ai3`, `Ai4`, `Ai5`, `Ai6`, `Ai7`, `Ai8`, `Aj1`, `Aj2`, `Aj3`, `Aj4`, `Aj5`, `Aj6`, `Aj7`, `Aj8`, `Ak1`, `Ak2`, `Ak3`, `Ak4`, `Ak5`, `Ak6`, `Ak7`, `Ak8`, `Al1`, `Al2`, `Al3`, `Al4`, `Al5`, `Al6`, `Al7`, `Al8`, `Am1`, `Am2`, `Am3`, `Am4`, `Am5`, `Am6`, `Am7`, `Am8`, `user_id`, `modified`) VALUES
('cc4cca53-3e35-4d74-895c-5eeec3bd0cf2', '5b36fc78-fb79-470f-a899-e6092107f6f4', '1', '2015-12-23', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2015-12-23');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `m_pasien`
--
ALTER TABLE `m_pasien`
  ADD CONSTRAINT `m_pasien_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `m_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `p_anamnesis`
--
ALTER TABLE `p_anamnesis`
  ADD CONSTRAINT `p_anamnesis_ibfk_5` FOREIGN KEY (`id_pasien`) REFERENCES `m_pasien` (`id_pasien`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `p_anamnesis_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `m_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
