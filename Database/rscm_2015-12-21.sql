-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2015 at 11:25 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rscm`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_pasien`
--

CREATE TABLE IF NOT EXISTS `m_pasien` (
  `id` varchar(36) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `namaOrangTua` varchar(100) NOT NULL,
  `usia` varchar(100) NOT NULL,
  `pendidikan` varchar(100) NOT NULL,
  `tempatLahir` varchar(100) NOT NULL,
  `tglLahir` date NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `suku` varchar(100) NOT NULL,
  `keturunan` varchar(100) NOT NULL,
  `asing` varchar(100) NOT NULL,
  `noRekamMedis` varchar(100) NOT NULL,
  `agama` varchar(100) NOT NULL,
  `daerah` varchar(100) NOT NULL,
  `jJalan` varchar(200) NOT NULL,
  `jRt` varchar(10) NOT NULL,
  `jRw` varchar(10) NOT NULL,
  `jNo` varchar(10) NOT NULL,
  `jTelp` varchar(10) NOT NULL,
  `jKelurahan` varchar(100) NOT NULL,
  `jKecamatan` varchar(100) NOT NULL,
  `aJalan` varchar(100) NOT NULL,
  `aRt` varchar(10) NOT NULL,
  `aRw` varchar(10) NOT NULL,
  `aNo` varchar(100) NOT NULL,
  `aTelp` varchar(100) NOT NULL,
  `aKelurahan` varchar(100) NOT NULL,
  `aKecamatan` varchar(100) NOT NULL,
  `aKabupaten` varchar(100) NOT NULL,
  `aProvinsi` varchar(100) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pasien`
--

INSERT INTO `m_pasien` (`id`, `nama`, `namaOrangTua`, `usia`, `pendidikan`, `tempatLahir`, `tglLahir`, `pekerjaan`, `suku`, `keturunan`, `asing`, `noRekamMedis`, `agama`, `daerah`, `jJalan`, `jRt`, `jRw`, `jNo`, `jTelp`, `jKelurahan`, `jKecamatan`, `aJalan`, `aRt`, `aRw`, `aNo`, `aTelp`, `aKelurahan`, `aKecamatan`, `aKabupaten`, `aProvinsi`, `user_id`, `modified`) VALUES
('10803550-5739-4d7b-91b9-ffe1949e1bd4', '222', '222', '222', '222', '222', '2015-12-20', '222', '222', '222', '222', '222', '2222', '2222', '222', '222', '222', '222', '222', '222', '222', '222', '222', '222', '222', '222', '222222', '222', '2222', '222', '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2015-12-20'),
('1439a82a-a6f3-11e5-8b9a-00158315a310', '222', '1', '1', '1', '1', '2015-12-20', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2015-12-20');

-- --------------------------------------------------------

--
-- Table structure for table `m_pasien_baackup`
--

CREATE TABLE IF NOT EXISTS `m_pasien_baackup` (
  `id_pasien` varchar(36) NOT NULL,
  `no_medrek` varchar(20) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `nama_pasien` varchar(100) NOT NULL,
  `umur` varchar(3) NOT NULL,
  `jenis_kelamin` varchar(2) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `pendidikan` varchar(50) NOT NULL,
  `dirujuk_oleh` varchar(50) NOT NULL,
  `alamat_asal` text NOT NULL,
  `tlp_asal` varchar(20) NOT NULL,
  `alamat_jkt` text NOT NULL,
  `tlp_jkt` varchar(20) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `hamil` varchar(2) NOT NULL,
  `menyusui` varchar(2) NOT NULL,
  `status` varchar(50) NOT NULL,
  `jumlah_anak` varchar(2) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `telepon2` varchar(20) NOT NULL,
  `ras` varchar(2) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_pasien`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pasien_baackup`
--

INSERT INTO `m_pasien_baackup` (`id_pasien`, `no_medrek`, `nik`, `nama_pasien`, `umur`, `jenis_kelamin`, `tgl_lahir`, `pendidikan`, `dirujuk_oleh`, `alamat_asal`, `tlp_asal`, `alamat_jkt`, `tlp_jkt`, `pekerjaan`, `hamil`, `menyusui`, `status`, `jumlah_anak`, `telepon`, `telepon2`, `ras`, `user_id`, `modified`) VALUES
('680661fa-b062-4b1d-a05b-c7d079d693b6', '123456789', '987654345677654', 'Asep Jalaludin', '24', '1', '2015-12-12', '2', '0', 'Bogor', '025145678', 'Jakarta', '021987654567', '2', '2', '2', '2', '0', '0858746356784', '0874674895833', '5', 'aece9f57-8994-4d29-ac4a-8bdfbbc4b8ab', '2015-12-12'),
('aa824b67-6ad0-46f9-8869-31de67720d5b', '2', '2', '2', '2', '1', '2015-12-19', '1', '0', '2', '2', '2', '2', '1', '1', '1', '1', '1', '2', '2', '1', '1', '2015-12-20'),
('f90006da-87a8-4f00-af7b-59d90b98bb22', 'a', '', 'jalaludin', 'a', '1', '2015-12-12', '1', '0', 'a', '0', 'a', 'b', '1', '2', '2', '1', '1', '0', '0', '5', 'aece9f57-8994-4d29-ac4a-8bdfbbc4b8ab', '2015-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE IF NOT EXISTS `m_user` (
  `id_user` varchar(36) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `hak_akses` int(1) NOT NULL,
  `parent` int(10) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`id_user`, `nama_user`, `password`, `email`, `hak_akses`, `parent`, `modified`) VALUES
('3f0c0758-fcf8-43e1-ac82-813cc18fddcc', 'Super Administrator', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', 0, 0, '0000-00-00'),
('3f0c0758-fcf8-45e1-ac82-803cc18fddcc', 'qwe', '76d80224611fc919a5d54f0ff9fba446', 'qwe@qwe.qwe', 0, 1, '2015-12-20'),
('9e8d0756-f94d-460f-8a24-83e26fc0fbb4', 'jalaludin', '', 'asepjalaludin@gmail.com', 0, 1, '2015-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `p_endoskopi`
--

CREATE TABLE IF NOT EXISTS `p_endoskopi` (
  `id_endoskopi` varchar(36) NOT NULL,
  `id_pasien` varchar(36) NOT NULL,
  `status_endoskopi` varchar(1) NOT NULL,
  `tgl_periksa` date NOT NULL,
  `pemeriksa` varchar(100) NOT NULL,
  `kesan` varchar(2) NOT NULL,
  `lokasi_tumor` int(11) NOT NULL,
  `id_user` varchar(36) NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_endoskopi`
--

INSERT INTO `p_endoskopi` (`id_endoskopi`, `id_pasien`, `status_endoskopi`, `tgl_periksa`, `pemeriksa`, `kesan`, `lokasi_tumor`, `id_user`, `modified`) VALUES
('2dd04193-edd6-4476-859f-3c674ddd4cdd', 'f90006da-87a8-4f00-af7b-59d90b98bb22', '1', '2015-12-06', 'a', '2', 2, '0', '2015-12-13');

-- --------------------------------------------------------

--
-- Table structure for table `p_karsinoma`
--

CREATE TABLE IF NOT EXISTS `p_karsinoma` (
  `id_karsinoma` varchar(36) NOT NULL,
  `id_pasien` varchar(36) NOT NULL,
  `no_rekam_medis` varchar(20) NOT NULL,
  `pemeriksa` varchar(100) NOT NULL,
  `tgl_registrasi` date NOT NULL,
  `id_user` varchar(36) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_karsinoma`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
