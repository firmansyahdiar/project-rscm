-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2016 at 06:26 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rscm`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE IF NOT EXISTS `m_user` (
  `id_user` varchar(36) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `hak_akses` int(1) NOT NULL,
  `parent` varchar(36) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`id_user`, `nama_user`, `password`, `email`, `hak_akses`, `parent`, `modified`) VALUES
('08aa020e-0b94-47c3-9bb4-52d809053ece', 'user11', '03aa1a0b0375b0461c1b8f35b234e67a', 'user11@email.com', 2, 'db12a23e-b4f3-441b-95f4-9f9c1952ef6e', '2016-01-13'),
('6facc78f-f184-4cac-8d9c-9d1f5a2d42a6', 'user21', '2e129db15b6d6db5342ba5d328642262', 'user21@email.com', 2, '74c40c67-f1ad-45d1-ba12-4a9c5a89d34d', '2016-01-13'),
('710f5250-d755-416b-bf01-c0e078f35d5b', 'user22', '87dc1e131a1369fdf8f1c824a6a62dff', 'user22@email.com', 2, '74c40c67-f1ad-45d1-ba12-4a9c5a89d34d', '2016-01-13'),
('74c40c67-f1ad-45d1-ba12-4a9c5a89d34d', 'admin2', 'c84258e9c39059a89ab77d846ddab909', 'admin2@email.com', 1, 'cda92de8-2162-4e73-a180-e3ea76d2a1cf', '2016-01-13'),
('b7e9fd3d-10c3-4f19-a695-4fcf563e3388', 'user12', 'd781eaae8248db6ce1a7b82e58e60435', 'user12@email.com', 2, 'db12a23e-b4f3-441b-95f4-9f9c1952ef6e', '2016-01-13'),
('cda92de8-2162-4e73-a180-e3ea76d2a1cf', 'administrator', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', 0, '-', '2016-01-13'),
('db12a23e-b4f3-441b-95f4-9f9c1952ef6e', 'admin1', 'e00cf25ad42683b3df678c61f42c6bda', 'admin1@email.com', 1, 'cda92de8-2162-4e73-a180-e3ea76d2a1cf', '2016-01-13');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
