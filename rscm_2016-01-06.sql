-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2016 at 04:15 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rscm`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_pasien`
--

CREATE TABLE IF NOT EXISTS `m_pasien` (
  `id_pasien` varchar(36) NOT NULL,
  `nama_pasien` varchar(100) NOT NULL,
  `namaOrangTua` varchar(100) NOT NULL,
  `usia` varchar(100) NOT NULL,
  `pendidikan` varchar(100) NOT NULL,
  `tempatLahir` varchar(100) NOT NULL,
  `tglLahir` date NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `suku` varchar(100) NOT NULL,
  `keturunan` varchar(100) NOT NULL,
  `asing` varchar(100) NOT NULL,
  `noRekamMedis` varchar(100) NOT NULL,
  `agama` varchar(100) NOT NULL,
  `daerah` varchar(100) NOT NULL,
  `jJalan` varchar(200) NOT NULL,
  `jRt` varchar(10) NOT NULL,
  `jRw` varchar(10) NOT NULL,
  `jNo` varchar(10) NOT NULL,
  `jTelp` varchar(10) NOT NULL,
  `jKelurahan` varchar(100) NOT NULL,
  `jKecamatan` varchar(100) NOT NULL,
  `aJalan` varchar(100) NOT NULL,
  `aRt` varchar(10) NOT NULL,
  `aRw` varchar(10) NOT NULL,
  `aNo` varchar(100) NOT NULL,
  `aTelp` varchar(100) NOT NULL,
  `aKelurahan` varchar(100) NOT NULL,
  `aKecamatan` varchar(100) NOT NULL,
  `aKabupaten` varchar(100) NOT NULL,
  `aProvinsi` varchar(100) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_pasien`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pasien`
--

INSERT INTO `m_pasien` (`id_pasien`, `nama_pasien`, `namaOrangTua`, `usia`, `pendidikan`, `tempatLahir`, `tglLahir`, `pekerjaan`, `suku`, `keturunan`, `asing`, `noRekamMedis`, `agama`, `daerah`, `jJalan`, `jRt`, `jRw`, `jNo`, `jTelp`, `jKelurahan`, `jKecamatan`, `aJalan`, `aRt`, `aRw`, `aNo`, `aTelp`, `aKelurahan`, `aKecamatan`, `aKabupaten`, `aProvinsi`, `user_id`, `modified`) VALUES
('5b36fc78-fb79-470f-a899-e6092107f6f4', 'Diar', '12', '12', '12', '12', '2015-12-23', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2015-12-23');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE IF NOT EXISTS `m_user` (
  `id_user` varchar(36) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `hak_akses` int(1) NOT NULL,
  `parent` int(10) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`id_user`, `nama_user`, `password`, `email`, `hak_akses`, `parent`, `modified`) VALUES
('3f0c0758-fcf8-43e1-ac82-813cc18fddcc', 'Super Administrator', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', 0, 0, '0000-00-00'),
('3f0c0758-fcf8-45e1-ac82-803cc18fddcc', 'qwe', '76d80224611fc919a5d54f0ff9fba446', 'qwe@qwe.qwe', 0, 1, '2015-12-20'),
('9e8d0756-f94d-460f-8a24-83e26fc0fbb4', 'jalaludin', '', 'asepjalaludin@gmail.com', 0, 1, '2015-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `p_anamnesis`
--

CREATE TABLE IF NOT EXISTS `p_anamnesis` (
  `id_anamnesis` varchar(36) NOT NULL,
  `id_pasien` varchar(36) NOT NULL,
  `kunjungan` varchar(10) NOT NULL,
  `tgl_periksa` date NOT NULL,
  `Aa1` tinyint(1) NOT NULL,
  `Aa2` tinyint(1) NOT NULL,
  `Aa3` tinyint(1) NOT NULL,
  `Aa4` tinyint(1) NOT NULL,
  `Aa5` tinyint(1) NOT NULL,
  `Aa6` tinyint(1) NOT NULL,
  `Aa7` tinyint(1) NOT NULL,
  `Aa8` tinyint(1) NOT NULL,
  `Ab1` tinyint(1) NOT NULL,
  `Ab2` tinyint(1) NOT NULL,
  `Ab3` tinyint(1) NOT NULL,
  `Ab4` tinyint(1) NOT NULL,
  `Ab5` tinyint(1) NOT NULL,
  `Ab6` tinyint(1) NOT NULL,
  `Ab7` tinyint(1) NOT NULL,
  `Ab8` tinyint(1) NOT NULL,
  `Ac1` tinyint(1) NOT NULL,
  `Ac2` tinyint(1) NOT NULL,
  `Ac3` tinyint(1) NOT NULL,
  `Ac4` tinyint(1) NOT NULL,
  `Ac5` tinyint(1) NOT NULL,
  `Ac6` tinyint(1) NOT NULL,
  `Ac7` tinyint(1) NOT NULL,
  `Ac8` tinyint(1) NOT NULL,
  `Ad1` tinyint(1) NOT NULL,
  `Ad2` tinyint(1) NOT NULL,
  `Ad3` tinyint(1) NOT NULL,
  `Ad4` tinyint(1) NOT NULL,
  `Ad5` tinyint(1) NOT NULL,
  `Ad6` tinyint(1) NOT NULL,
  `Ad7` tinyint(1) NOT NULL,
  `Ad8` tinyint(1) NOT NULL,
  `Ae1` tinyint(1) NOT NULL,
  `Ae2` tinyint(1) NOT NULL,
  `Ae3` tinyint(1) NOT NULL,
  `Ae4` tinyint(1) NOT NULL,
  `Ae5` tinyint(1) NOT NULL,
  `Ae6` tinyint(1) NOT NULL,
  `Ae7` tinyint(1) NOT NULL,
  `Ae8` tinyint(1) NOT NULL,
  `Af1` tinyint(1) NOT NULL,
  `Af2` tinyint(1) NOT NULL,
  `Af3` tinyint(1) NOT NULL,
  `Af4` tinyint(1) NOT NULL,
  `Af5` tinyint(1) NOT NULL,
  `Af6` tinyint(1) NOT NULL,
  `Af7` tinyint(1) NOT NULL,
  `Af8` tinyint(1) NOT NULL,
  `Ag1` tinyint(1) NOT NULL,
  `Ag2` tinyint(1) NOT NULL,
  `Ag3` tinyint(1) NOT NULL,
  `Ag4` tinyint(1) NOT NULL,
  `Ag5` tinyint(1) NOT NULL,
  `Ag6` tinyint(1) NOT NULL,
  `Ag7` tinyint(1) NOT NULL,
  `Ag8` tinyint(1) NOT NULL,
  `Ah1` tinyint(1) NOT NULL,
  `Ah2` tinyint(1) NOT NULL,
  `Ah3` tinyint(1) NOT NULL,
  `Ah4` tinyint(1) NOT NULL,
  `Ah5` tinyint(1) NOT NULL,
  `Ah6` tinyint(1) NOT NULL,
  `Ah7` tinyint(1) NOT NULL,
  `Ah8` tinyint(1) NOT NULL,
  `Ai1` tinyint(1) NOT NULL,
  `Ai2` tinyint(1) NOT NULL,
  `Ai3` tinyint(1) NOT NULL,
  `Ai4` tinyint(1) NOT NULL,
  `Ai5` tinyint(1) NOT NULL,
  `Ai6` tinyint(1) NOT NULL,
  `Ai7` tinyint(1) NOT NULL,
  `Ai8` tinyint(1) NOT NULL,
  `Aj1` tinyint(1) NOT NULL,
  `Aj2` tinyint(1) NOT NULL,
  `Aj3` tinyint(1) NOT NULL,
  `Aj4` tinyint(1) NOT NULL,
  `Aj5` tinyint(1) NOT NULL,
  `Aj6` tinyint(1) NOT NULL,
  `Aj7` tinyint(1) NOT NULL,
  `Aj8` tinyint(1) NOT NULL,
  `Ak1` tinyint(1) NOT NULL,
  `Ak2` tinyint(1) NOT NULL,
  `Ak3` tinyint(1) NOT NULL,
  `Ak4` tinyint(1) NOT NULL,
  `Ak5` tinyint(1) NOT NULL,
  `Ak6` tinyint(1) NOT NULL,
  `Ak7` tinyint(1) NOT NULL,
  `Ak8` tinyint(1) NOT NULL,
  `Al1` tinyint(1) NOT NULL,
  `Al2` tinyint(1) NOT NULL,
  `Al3` tinyint(1) NOT NULL,
  `Al4` tinyint(1) NOT NULL,
  `Al5` tinyint(1) NOT NULL,
  `Al6` tinyint(1) NOT NULL,
  `Al7` tinyint(1) NOT NULL,
  `Al8` tinyint(1) NOT NULL,
  `Am1` tinyint(1) NOT NULL,
  `Am2` tinyint(1) NOT NULL,
  `Am3` tinyint(1) NOT NULL,
  `Am4` tinyint(1) NOT NULL,
  `Am5` tinyint(1) NOT NULL,
  `Am6` tinyint(1) NOT NULL,
  `Am7` tinyint(1) NOT NULL,
  `Am8` tinyint(1) NOT NULL,
  `Ba1` tinyint(1) NOT NULL,
  `Ba2` tinyint(1) NOT NULL,
  `Ba3` tinyint(1) NOT NULL,
  `Ba4` tinyint(1) NOT NULL,
  `Ba5` tinyint(1) NOT NULL,
  `Ba6` tinyint(1) NOT NULL,
  `Ba7` tinyint(1) NOT NULL,
  `Ba8` tinyint(1) NOT NULL,
  `Bb1` tinyint(1) NOT NULL,
  `Bb2` tinyint(1) NOT NULL,
  `Bb3` tinyint(1) NOT NULL,
  `Bb4` tinyint(1) NOT NULL,
  `Bb5` tinyint(1) NOT NULL,
  `Bb6` tinyint(1) NOT NULL,
  `Bb7` tinyint(1) NOT NULL,
  `Bb8` tinyint(1) NOT NULL,
  `Bc1` tinyint(1) NOT NULL,
  `Bc2` tinyint(1) NOT NULL,
  `Bc3` tinyint(1) NOT NULL,
  `Bc4` tinyint(1) NOT NULL,
  `Bc5` tinyint(1) NOT NULL,
  `Bc6` tinyint(1) NOT NULL,
  `Bc7` tinyint(1) NOT NULL,
  `Bc8` tinyint(1) NOT NULL,
  `Bd1` tinyint(1) NOT NULL,
  `Bd2` tinyint(1) NOT NULL,
  `Bd3` tinyint(1) NOT NULL,
  `Bd4` tinyint(1) NOT NULL,
  `Bd5` tinyint(1) NOT NULL,
  `Bd6` tinyint(1) NOT NULL,
  `Bd7` tinyint(1) NOT NULL,
  `Bd8` tinyint(1) NOT NULL,
  `Be1` tinyint(1) NOT NULL,
  `Be2` tinyint(1) NOT NULL,
  `Be3` tinyint(1) NOT NULL,
  `Be4` tinyint(1) NOT NULL,
  `Be5` tinyint(1) NOT NULL,
  `Be6` tinyint(1) NOT NULL,
  `Be7` tinyint(1) NOT NULL,
  `Be8` tinyint(1) NOT NULL,
  `Bf1` tinyint(1) NOT NULL,
  `Bf2` tinyint(1) NOT NULL,
  `Bf3` tinyint(1) NOT NULL,
  `Bf4` tinyint(1) NOT NULL,
  `Bf5` tinyint(1) NOT NULL,
  `Bf6` tinyint(1) NOT NULL,
  `Bf7` tinyint(1) NOT NULL,
  `Bf8` tinyint(1) NOT NULL,
  `Bg1` tinyint(1) NOT NULL,
  `Bg2` tinyint(1) NOT NULL,
  `Bg3` tinyint(1) NOT NULL,
  `Bg4` tinyint(1) NOT NULL,
  `Bg5` tinyint(1) NOT NULL,
  `Bg6` tinyint(1) NOT NULL,
  `Bg7` tinyint(1) NOT NULL,
  `Bg8` tinyint(1) NOT NULL,
  `Bh1` tinyint(1) NOT NULL,
  `Bh2` tinyint(1) NOT NULL,
  `Bh3` tinyint(1) NOT NULL,
  `Bh4` tinyint(1) NOT NULL,
  `Bh5` tinyint(1) NOT NULL,
  `Bh6` tinyint(1) NOT NULL,
  `Bh7` tinyint(1) NOT NULL,
  `Bh8` tinyint(1) NOT NULL,
  `Bi1` tinyint(1) NOT NULL,
  `Bi2` tinyint(1) NOT NULL,
  `Bi3` tinyint(1) NOT NULL,
  `Bi4` tinyint(1) NOT NULL,
  `Bi5` tinyint(1) NOT NULL,
  `Bi6` tinyint(1) NOT NULL,
  `Bi7` tinyint(1) NOT NULL,
  `Bi8` tinyint(1) NOT NULL,
  `Bj1` tinyint(1) NOT NULL,
  `Bj2` tinyint(1) NOT NULL,
  `Bj3` tinyint(1) NOT NULL,
  `Bj4` tinyint(1) NOT NULL,
  `Bj5` tinyint(1) NOT NULL,
  `Bj6` tinyint(1) NOT NULL,
  `Bj7` tinyint(1) NOT NULL,
  `Bj8` tinyint(1) NOT NULL,
  `Ca1` tinyint(1) NOT NULL,
  `Ca2` tinyint(1) NOT NULL,
  `Ca3` tinyint(1) NOT NULL,
  `Ca4` tinyint(1) NOT NULL,
  `Ca5` tinyint(1) NOT NULL,
  `Ca6` tinyint(1) NOT NULL,
  `Ca7` tinyint(1) NOT NULL,
  `Ca8` tinyint(1) NOT NULL,
  `Cb1` tinyint(1) NOT NULL,
  `Cb2` tinyint(1) NOT NULL,
  `Cb3` tinyint(1) NOT NULL,
  `Cb4` tinyint(1) NOT NULL,
  `Cb5` tinyint(1) NOT NULL,
  `Cb6` tinyint(1) NOT NULL,
  `Cb7` tinyint(1) NOT NULL,
  `Cb8` tinyint(1) NOT NULL,
  `Cc1` tinyint(1) NOT NULL,
  `Cc2` tinyint(1) NOT NULL,
  `Cc3` tinyint(1) NOT NULL,
  `Cc4` tinyint(1) NOT NULL,
  `Cc5` tinyint(1) NOT NULL,
  `Cc6` tinyint(1) NOT NULL,
  `Cc7` tinyint(1) NOT NULL,
  `Cc8` tinyint(1) NOT NULL,
  `Cd1` tinyint(1) NOT NULL,
  `Cd2` tinyint(1) NOT NULL,
  `Cd3` tinyint(1) NOT NULL,
  `Cd4` tinyint(1) NOT NULL,
  `Cd5` tinyint(1) NOT NULL,
  `Cd6` tinyint(1) NOT NULL,
  `Cd7` tinyint(1) NOT NULL,
  `Cd8` tinyint(1) NOT NULL,
  `Ce1` tinyint(1) NOT NULL,
  `Ce2` tinyint(1) NOT NULL,
  `Ce3` tinyint(1) NOT NULL,
  `Ce4` tinyint(1) NOT NULL,
  `Ce5` tinyint(1) NOT NULL,
  `Ce6` tinyint(1) NOT NULL,
  `Ce7` tinyint(1) NOT NULL,
  `Ce8` tinyint(1) NOT NULL,
  `Cf1` tinyint(1) NOT NULL,
  `Cf2` tinyint(1) NOT NULL,
  `Cf3` tinyint(1) NOT NULL,
  `Cf4` tinyint(1) NOT NULL,
  `Cf5` tinyint(1) NOT NULL,
  `Cf6` tinyint(1) NOT NULL,
  `Cf7` tinyint(1) NOT NULL,
  `Cf8` tinyint(1) NOT NULL,
  `Da1` tinyint(1) NOT NULL,
  `Da2` tinyint(1) NOT NULL,
  `Da3` tinyint(1) NOT NULL,
  `Da4` tinyint(1) NOT NULL,
  `Da5` tinyint(1) NOT NULL,
  `Da6` tinyint(1) NOT NULL,
  `Da7` tinyint(1) NOT NULL,
  `Da8` tinyint(1) NOT NULL,
  `Db1` tinyint(1) NOT NULL,
  `Db2` tinyint(1) NOT NULL,
  `Db3` tinyint(1) NOT NULL,
  `Db4` tinyint(1) NOT NULL,
  `Db5` tinyint(1) NOT NULL,
  `Db6` tinyint(1) NOT NULL,
  `Db7` tinyint(1) NOT NULL,
  `Db8` tinyint(1) NOT NULL,
  `Dc1` tinyint(1) NOT NULL,
  `Dc2` tinyint(1) NOT NULL,
  `Dc3` tinyint(1) NOT NULL,
  `Dc4` tinyint(1) NOT NULL,
  `Dc5` tinyint(1) NOT NULL,
  `Dc6` tinyint(1) NOT NULL,
  `Dc7` tinyint(1) NOT NULL,
  `Dc8` tinyint(1) NOT NULL,
  `Dd1` tinyint(1) NOT NULL,
  `Dd2` tinyint(1) NOT NULL,
  `Dd3` tinyint(1) NOT NULL,
  `Dd4` tinyint(1) NOT NULL,
  `Dd5` tinyint(1) NOT NULL,
  `Dd6` tinyint(1) NOT NULL,
  `Dd7` tinyint(1) NOT NULL,
  `Dd8` tinyint(1) NOT NULL,
  `De1` tinyint(1) NOT NULL,
  `De2` tinyint(1) NOT NULL,
  `De3` tinyint(1) NOT NULL,
  `De4` tinyint(1) NOT NULL,
  `De5` tinyint(1) NOT NULL,
  `De6` tinyint(1) NOT NULL,
  `De7` tinyint(1) NOT NULL,
  `De8` tinyint(1) NOT NULL,
  `Df1` tinyint(1) NOT NULL,
  `Df2` tinyint(1) NOT NULL,
  `Df3` tinyint(1) NOT NULL,
  `Df4` tinyint(1) NOT NULL,
  `Df5` tinyint(1) NOT NULL,
  `Df6` tinyint(1) NOT NULL,
  `Df7` tinyint(1) NOT NULL,
  `Df8` tinyint(1) NOT NULL,
  `Ea1` tinyint(1) NOT NULL,
  `Ea2` tinyint(1) NOT NULL,
  `Ea3` tinyint(1) NOT NULL,
  `Ea4` tinyint(1) NOT NULL,
  `Ea5` tinyint(1) NOT NULL,
  `Ea6` tinyint(1) NOT NULL,
  `Ea7` tinyint(1) NOT NULL,
  `Ea8` tinyint(1) NOT NULL,
  `Eb1` tinyint(1) NOT NULL,
  `Eb2` tinyint(1) NOT NULL,
  `Eb3` tinyint(1) NOT NULL,
  `Eb4` tinyint(1) NOT NULL,
  `Eb5` tinyint(1) NOT NULL,
  `Eb6` tinyint(1) NOT NULL,
  `Eb7` tinyint(1) NOT NULL,
  `Eb8` tinyint(1) NOT NULL,
  `Ec1` tinyint(1) NOT NULL,
  `Ec2` tinyint(1) NOT NULL,
  `Ec3` tinyint(1) NOT NULL,
  `Ec4` tinyint(1) NOT NULL,
  `Ec5` tinyint(1) NOT NULL,
  `Ec6` tinyint(1) NOT NULL,
  `Ec7` tinyint(1) NOT NULL,
  `Ec8` tinyint(1) NOT NULL,
  `Ed1` tinyint(1) NOT NULL,
  `Ed2` tinyint(1) NOT NULL,
  `Ed3` tinyint(1) NOT NULL,
  `Ed4` tinyint(1) NOT NULL,
  `Ed5` tinyint(1) NOT NULL,
  `Ed6` tinyint(1) NOT NULL,
  `Ed7` tinyint(1) NOT NULL,
  `Ed8` tinyint(1) NOT NULL,
  `Ee1` tinyint(1) NOT NULL,
  `Ee2` tinyint(1) NOT NULL,
  `Ee3` tinyint(1) NOT NULL,
  `Ee4` tinyint(1) NOT NULL,
  `Ee5` tinyint(1) NOT NULL,
  `Ee6` tinyint(1) NOT NULL,
  `Ee7` tinyint(1) NOT NULL,
  `Ee8` tinyint(1) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL,
  PRIMARY KEY (`id_anamnesis`),
  KEY `id_pasien` (`id_pasien`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_anamnesis`
--

INSERT INTO `p_anamnesis` (`id_anamnesis`, `id_pasien`, `kunjungan`, `tgl_periksa`, `Aa1`, `Aa2`, `Aa3`, `Aa4`, `Aa5`, `Aa6`, `Aa7`, `Aa8`, `Ab1`, `Ab2`, `Ab3`, `Ab4`, `Ab5`, `Ab6`, `Ab7`, `Ab8`, `Ac1`, `Ac2`, `Ac3`, `Ac4`, `Ac5`, `Ac6`, `Ac7`, `Ac8`, `Ad1`, `Ad2`, `Ad3`, `Ad4`, `Ad5`, `Ad6`, `Ad7`, `Ad8`, `Ae1`, `Ae2`, `Ae3`, `Ae4`, `Ae5`, `Ae6`, `Ae7`, `Ae8`, `Af1`, `Af2`, `Af3`, `Af4`, `Af5`, `Af6`, `Af7`, `Af8`, `Ag1`, `Ag2`, `Ag3`, `Ag4`, `Ag5`, `Ag6`, `Ag7`, `Ag8`, `Ah1`, `Ah2`, `Ah3`, `Ah4`, `Ah5`, `Ah6`, `Ah7`, `Ah8`, `Ai1`, `Ai2`, `Ai3`, `Ai4`, `Ai5`, `Ai6`, `Ai7`, `Ai8`, `Aj1`, `Aj2`, `Aj3`, `Aj4`, `Aj5`, `Aj6`, `Aj7`, `Aj8`, `Ak1`, `Ak2`, `Ak3`, `Ak4`, `Ak5`, `Ak6`, `Ak7`, `Ak8`, `Al1`, `Al2`, `Al3`, `Al4`, `Al5`, `Al6`, `Al7`, `Al8`, `Am1`, `Am2`, `Am3`, `Am4`, `Am5`, `Am6`, `Am7`, `Am8`, `Ba1`, `Ba2`, `Ba3`, `Ba4`, `Ba5`, `Ba6`, `Ba7`, `Ba8`, `Bb1`, `Bb2`, `Bb3`, `Bb4`, `Bb5`, `Bb6`, `Bb7`, `Bb8`, `Bc1`, `Bc2`, `Bc3`, `Bc4`, `Bc5`, `Bc6`, `Bc7`, `Bc8`, `Bd1`, `Bd2`, `Bd3`, `Bd4`, `Bd5`, `Bd6`, `Bd7`, `Bd8`, `Be1`, `Be2`, `Be3`, `Be4`, `Be5`, `Be6`, `Be7`, `Be8`, `Bf1`, `Bf2`, `Bf3`, `Bf4`, `Bf5`, `Bf6`, `Bf7`, `Bf8`, `Bg1`, `Bg2`, `Bg3`, `Bg4`, `Bg5`, `Bg6`, `Bg7`, `Bg8`, `Bh1`, `Bh2`, `Bh3`, `Bh4`, `Bh5`, `Bh6`, `Bh7`, `Bh8`, `Bi1`, `Bi2`, `Bi3`, `Bi4`, `Bi5`, `Bi6`, `Bi7`, `Bi8`, `Bj1`, `Bj2`, `Bj3`, `Bj4`, `Bj5`, `Bj6`, `Bj7`, `Bj8`, `Ca1`, `Ca2`, `Ca3`, `Ca4`, `Ca5`, `Ca6`, `Ca7`, `Ca8`, `Cb1`, `Cb2`, `Cb3`, `Cb4`, `Cb5`, `Cb6`, `Cb7`, `Cb8`, `Cc1`, `Cc2`, `Cc3`, `Cc4`, `Cc5`, `Cc6`, `Cc7`, `Cc8`, `Cd1`, `Cd2`, `Cd3`, `Cd4`, `Cd5`, `Cd6`, `Cd7`, `Cd8`, `Ce1`, `Ce2`, `Ce3`, `Ce4`, `Ce5`, `Ce6`, `Ce7`, `Ce8`, `Cf1`, `Cf2`, `Cf3`, `Cf4`, `Cf5`, `Cf6`, `Cf7`, `Cf8`, `Da1`, `Da2`, `Da3`, `Da4`, `Da5`, `Da6`, `Da7`, `Da8`, `Db1`, `Db2`, `Db3`, `Db4`, `Db5`, `Db6`, `Db7`, `Db8`, `Dc1`, `Dc2`, `Dc3`, `Dc4`, `Dc5`, `Dc6`, `Dc7`, `Dc8`, `Dd1`, `Dd2`, `Dd3`, `Dd4`, `Dd5`, `Dd6`, `Dd7`, `Dd8`, `De1`, `De2`, `De3`, `De4`, `De5`, `De6`, `De7`, `De8`, `Df1`, `Df2`, `Df3`, `Df4`, `Df5`, `Df6`, `Df7`, `Df8`, `Ea1`, `Ea2`, `Ea3`, `Ea4`, `Ea5`, `Ea6`, `Ea7`, `Ea8`, `Eb1`, `Eb2`, `Eb3`, `Eb4`, `Eb5`, `Eb6`, `Eb7`, `Eb8`, `Ec1`, `Ec2`, `Ec3`, `Ec4`, `Ec5`, `Ec6`, `Ec7`, `Ec8`, `Ed1`, `Ed2`, `Ed3`, `Ed4`, `Ed5`, `Ed6`, `Ed7`, `Ed8`, `Ee1`, `Ee2`, `Ee3`, `Ee4`, `Ee5`, `Ee6`, `Ee7`, `Ee8`, `user_id`, `modified`) VALUES
('afc01e5e-5ec7-47c2-a2f0-bce2a83e7231', '5b36fc78-fb79-470f-a899-e6092107f6f4', '143', '2016-01-12', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2016-01-06'),
('cc4cca53-3e35-4d74-895c-5eeec3bd0cf2', '5b36fc78-fb79-470f-a899-e6092107f6f4', '1', '2016-01-06', 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2016-01-06');

-- --------------------------------------------------------

--
-- Table structure for table `p_pemeriksaan`
--

CREATE TABLE IF NOT EXISTS `p_pemeriksaan` (
  `id_pemeriksaan` varchar(36) NOT NULL,
  `id_pasien` varchar(36) NOT NULL,
  `kunjungan` varchar(100) NOT NULL,
  `tgl_Periksa` date NOT NULL,
  `dokter_umum` varchar(100) NOT NULL,
  `dokter_tht` varchar(100) NOT NULL,
  `A1` tinyint(1) NOT NULL,
  `A2` tinyint(1) NOT NULL,
  `A3` tinyint(1) NOT NULL,
  `A4` tinyint(1) NOT NULL,
  `A5` tinyint(1) NOT NULL,
  `A6` tinyint(1) NOT NULL,
  `ALainnya` varchar(100) NOT NULL,
  `B1` tinyint(1) NOT NULL,
  `B2` tinyint(1) NOT NULL,
  `Ba1` tinyint(1) NOT NULL,
  `Ba2` tinyint(1) NOT NULL,
  `Ba3` tinyint(1) NOT NULL,
  `Ba4` tinyint(1) NOT NULL,
  `Ba5` tinyint(1) NOT NULL,
  `Ba6` tinyint(1) NOT NULL,
  `Ba7` tinyint(1) NOT NULL,
  `BaLainnya` varchar(100) NOT NULL,
  `Bb1` tinyint(1) NOT NULL,
  `Bb2` tinyint(1) NOT NULL,
  `Bb3` tinyint(1) NOT NULL,
  `Bb4` tinyint(1) NOT NULL,
  `Bb5` double NOT NULL,
  `Bb6` tinyint(1) NOT NULL,
  `Bb7` tinyint(1) NOT NULL,
  `Bb8` tinyint(1) NOT NULL,
  `BbLainnya` varchar(100) NOT NULL,
  `Ca1` tinyint(1) NOT NULL,
  `Ca2` tinyint(1) NOT NULL,
  `Ca3` tinyint(1) NOT NULL,
  `Ca4` tinyint(1) NOT NULL,
  `Ca5` tinyint(1) NOT NULL,
  `Ca6` tinyint(1) NOT NULL,
  `Ca7` tinyint(1) NOT NULL,
  `Ca8` tinyint(1) NOT NULL,
  `Ca9` tinyint(1) NOT NULL,
  `Ca10` tinyint(1) NOT NULL,
  `Cb1` tinyint(1) NOT NULL,
  `Cb2` tinyint(1) NOT NULL,
  `Cb3` tinyint(1) NOT NULL,
  `Cb4` tinyint(1) NOT NULL,
  `Cb5` tinyint(1) NOT NULL,
  `Cb6` tinyint(1) NOT NULL,
  `Cb7` tinyint(1) NOT NULL,
  `Cb8` tinyint(1) NOT NULL,
  `Cc1` tinyint(1) NOT NULL,
  `Cc2` tinyint(1) NOT NULL,
  `Cc3` tinyint(1) NOT NULL,
  `Cc4` tinyint(1) NOT NULL,
  `Cc5` tinyint(1) NOT NULL,
  `Cc6` tinyint(1) NOT NULL,
  `Cc7` tinyint(1) NOT NULL,
  `Cc8` tinyint(1) NOT NULL,
  `Cc9` tinyint(1) NOT NULL,
  `Cc10` tinyint(1) NOT NULL,
  `Cd1` tinyint(1) NOT NULL,
  `Cd2` tinyint(1) NOT NULL,
  `Cd3` tinyint(1) NOT NULL,
  `Cd4` tinyint(1) NOT NULL,
  `Cd5` tinyint(1) NOT NULL,
  `Cd6` tinyint(1) NOT NULL,
  `Cd7` tinyint(1) NOT NULL,
  `Cd8` tinyint(1) NOT NULL,
  `Ce1` tinyint(1) NOT NULL,
  `Ce2` tinyint(1) NOT NULL,
  `Ce3` tinyint(1) NOT NULL,
  `Ce4` tinyint(1) NOT NULL,
  `Ce5` tinyint(1) NOT NULL,
  `Ce6` tinyint(1) NOT NULL,
  `Ce7` tinyint(1) NOT NULL,
  `Ce8` tinyint(1) NOT NULL,
  `Ce9` tinyint(1) NOT NULL,
  `Cf1` tinyint(1) NOT NULL,
  `Cf2` tinyint(1) NOT NULL,
  `Cf3` tinyint(1) NOT NULL,
  `Cf4` tinyint(1) NOT NULL,
  `Cf5` tinyint(1) NOT NULL,
  `Cf6` tinyint(1) NOT NULL,
  `Cf7` tinyint(1) NOT NULL,
  `Cf8` tinyint(1) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_pemeriksaan`
--

INSERT INTO `p_pemeriksaan` (`id_pemeriksaan`, `id_pasien`, `kunjungan`, `tgl_Periksa`, `dokter_umum`, `dokter_tht`, `A1`, `A2`, `A3`, `A4`, `A5`, `A6`, `ALainnya`, `B1`, `B2`, `Ba1`, `Ba2`, `Ba3`, `Ba4`, `Ba5`, `Ba6`, `Ba7`, `BaLainnya`, `Bb1`, `Bb2`, `Bb3`, `Bb4`, `Bb5`, `Bb6`, `Bb7`, `Bb8`, `BbLainnya`, `Ca1`, `Ca2`, `Ca3`, `Ca4`, `Ca5`, `Ca6`, `Ca7`, `Ca8`, `Ca9`, `Ca10`, `Cb1`, `Cb2`, `Cb3`, `Cb4`, `Cb5`, `Cb6`, `Cb7`, `Cb8`, `Cc1`, `Cc2`, `Cc3`, `Cc4`, `Cc5`, `Cc6`, `Cc7`, `Cc8`, `Cc9`, `Cc10`, `Cd1`, `Cd2`, `Cd3`, `Cd4`, `Cd5`, `Cd6`, `Cd7`, `Cd8`, `Ce1`, `Ce2`, `Ce3`, `Ce4`, `Ce5`, `Ce6`, `Ce7`, `Ce8`, `Ce9`, `Cf1`, `Cf2`, `Cf3`, `Cf4`, `Cf5`, `Cf6`, `Cf7`, `Cf8`, `user_id`, `modified`) VALUES
('f2e1bc3f-8c13-4362-a2ef-8b418d8c6503', '5b36fc78-fb79-470f-a899-e6092107f6f4', '1', '2016-01-06', 'gf', 'dfg', 1, 0, 0, 1, 1, 1, '123', 0, 0, 1, 1, 1, 1, 1, 1, 1, '123', 1, 1, 1, 1, 1, 1, 1, 1, '123', 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2016-01-06'),
('fd44f7cb-f101-4e1a-b5c3-bd87744a8d93', '5b36fc78-fb79-470f-a899-e6092107f6f4', '67', '2016-01-14', 'rty', 'rty', 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2016-01-06');

-- --------------------------------------------------------

--
-- Table structure for table `p_pemeriksaan_fisik`
--

CREATE TABLE IF NOT EXISTS `p_pemeriksaan_fisik` (
  `id_pemeriksaan_fisik` varchar(36) NOT NULL,
  `id_pasien` varchar(36) NOT NULL,
  `kunjungan` varchar(10) NOT NULL,
  `tgl_periksa` date NOT NULL,
  `td` varchar(100) NOT NULL,
  `nadi` varchar(100) NOT NULL,
  `tb` varchar(100) NOT NULL,
  `bb` varchar(100) NOT NULL,
  `ks1` tinyint(1) NOT NULL,
  `ks2` tinyint(1) NOT NULL,
  `ks3` tinyint(1) NOT NULL,
  `ks4` tinyint(1) NOT NULL,
  `ks5` tinyint(1) NOT NULL,
  `ks6` tinyint(1) NOT NULL,
  `ks7` tinyint(1) NOT NULL,
  `ks8` tinyint(1) NOT NULL,
  `ks9` tinyint(1) NOT NULL,
  `ks10` tinyint(1) NOT NULL,
  `ks11` tinyint(1) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `modified` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_pemeriksaan_fisik`
--

INSERT INTO `p_pemeriksaan_fisik` (`id_pemeriksaan_fisik`, `id_pasien`, `kunjungan`, `tgl_periksa`, `td`, `nadi`, `tb`, `bb`, `ks1`, `ks2`, `ks3`, `ks4`, `ks5`, `ks6`, `ks7`, `ks8`, `ks9`, `ks10`, `ks11`, `user_id`, `modified`) VALUES
('d5bd793c-1473-4880-a2cf-82aba2460382', '5b36fc78-fb79-470f-a899-e6092107f6f4', '1', '2016-01-06', '1', '3', '2', '4', 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2016-01-07'),
('71e0e03c-6eb5-4c64-8d21-93e9650a27cb', '5b36fc78-fb79-470f-a899-e6092107f6f4', '54', '2016-01-06', '2', '2', '2', '2', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '3f0c0758-fcf8-43e1-ac82-813cc18fddcc', '2016-01-07');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `m_pasien`
--
ALTER TABLE `m_pasien`
  ADD CONSTRAINT `m_pasien_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `m_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `p_anamnesis`
--
ALTER TABLE `p_anamnesis`
  ADD CONSTRAINT `p_anamnesis_ibfk_5` FOREIGN KEY (`id_pasien`) REFERENCES `m_pasien` (`id_pasien`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `p_anamnesis_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `m_user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
