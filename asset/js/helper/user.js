function combobox_user(element, base_url) {
    $(element).combobox({
        url: base_url + 'index.php/master/user/get',
        method: 'post',
        mode: 'remote',
        valueField: 'id_user',
        textField: 'nama_user',
        //editable: false,
        onShowPanel: function () {
            $(this).combobox('reload');
        }
    });
}
