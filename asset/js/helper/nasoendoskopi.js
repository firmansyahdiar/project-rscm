function combobox_nasoendoskopi3(element) {
    $(element).combobox({
        valueField: 'value',
        textField: 'label',
        panelHeight:140,
        data:[{
           'label':'-',
           'value':'0'
        },{
           'label':'Normal',
           'value':'1'
        },{
           'label':'Tumor Eksofitik',
           'value':'2'
        },{
           'label':'Tumor Ulseratif',
           'value':'3'
        }]
    });
}
function combobox_nasoendoskopi4(element) {
    $(element).combobox({
        valueField: 'value',
        textField: 'label',
        panelHeight:140,
        data:[{
           'label':'-',
           'value':'0'
        },{
           'label':'Normal',
           'value':'1'
        },{
           'label':'Tumor Eksofitik',
           'value':'2'
        },{
           'label':'Tumor Ulseratif',
           'value':'3'
        },{
           'label':'Terfiksasi',
           'value':'4'
        }]
    });
}/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


