function combobox_pasien(element, base_url) {
    $(element).combobox({
        url: base_url + 'index.php/master/pasien/get',
        method: 'post',
        mode: 'remote',
        valueField: 'id_pasien',
        textField: 'nama_pasien',
        //editable: false,
        onShowPanel: function () {
            $(this).combobox('reload');
        }
    });
}

function combogrid_pasien(element, base_url) {
    $(element).combogrid({
        delay: 500,
        method: 'post',
        mode: 'remote',
        panelWidth: 500,
        url: base_url+'index.php/master/pasien/get',
        idField: 'id_pasien',
        textField: 'nama_pasien',
        fitColumns: true,
        pagination: true,
        columns: [[
                {field: 'id_pasien', title: 'ID', width: 10, hidden: false},
                {field: 'nama_pasien', title: 'Nama Pasien', align: 'left', width: '70%'},
                {field: 'tglLahir', title: 'Tanggal Lahir', align: 'left', width: '30%'}
            ]],
        onShowPanel: function() {
            $(this).combogrid('grid').datagrid('reload');
        }
    });
}