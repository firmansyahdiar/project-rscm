function combobox_tonsil(element) {
    $(element).combobox({
        valueField: 'value',
        textField: 'label',
        panelHeight:140,
        data:[{
           'label':'-',
           'value':'0'
        },{
           'label':'T1',
           'value':'1'
        },{
           'label':'T2',
           'value':'2'
        },{
           'label':'T3',
           'value':'3'
        },{
           'label':'T4',
           'value':'4'
        }]
    });
}

function combobox_molle(element) {
    $(element).combobox({
        valueField: 'value',
        textField: 'label',
        panelHeight:140,
        data:[{
           'label':'-',
           'value':'0'
        },{
           'label':'Normal',
           'value':'1'
        },{
           'label':'Destruksi',
           'value':'2'
        }]
    });
}

function combobox_durum(element) {
    $(element).combobox({
        valueField: 'value',
        textField: 'label',
        panelHeight:140,
        data:[{
           'label':'-',
           'value':'0'
        },{
           'label':'Normal',
           'value':'1'
        },{
           'label':'Goyah',
           'value':'2'
        },{
           'label':'Infiltrasi Tumor',
           'value':'3'
        }]
    });
}

function combobox_lidah(element) {
    $(element).combobox({
        valueField: 'value',
        textField: 'label',
        panelHeight:140,
        data:[{
           'label':'-',
           'value':'0'
        },{
           'label':'Normal',
           'value':'1'
        },{
           'label':'Leukoplakia',
           'value':'2'
        },{
           'label':'Ulkus',
           'value':'3'
        },{
           'label':'Aksofitik',
           'value':'4'
        }]
    });
}

function combobox_mulut(element) {
    $(element).combobox({
        valueField: 'value',
        textField: 'label',
        panelHeight:140,
        data:[{
           'label':'-',
           'value':'0'
        },{
           'label':'Normal',
           'value':'1'
        },{
           'label':'Tumor',
           'value':'2'
        }]
    });
}

function combobox_geligi(element) {
    $(element).combobox({
        valueField: 'value',
        textField: 'label',
        panelHeight:140,
        data:[{
           'label':'-',
           'value':'0'
        },{
           'label':'Normal',
           'value':'1'
        },{
           'label':'Goyah',
           'value':'2'
        },{
           'label':'Infiltrasi Tumor',
           'value':'3'
        }]
    });
}


