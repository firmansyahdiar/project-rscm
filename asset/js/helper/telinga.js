function combobox_telinga(element) {
    $(element).combobox({
        valueField: 'value',
        textField: 'label',
        panelHeight:140,
        data:[{
           'label':'-',
           'value':'0'
        },{
           'label':'Normal',
           'value':'1'
        },{
           'label':'Konduktif',
           'value':'2'
        },{
           'label':'Sensorineural',
           'value':'3'
        },{
           'label':'Campur',
           'value':'4'
        }]
    });
}